--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE accion (
    "idAccion" integer NOT NULL,
    "nroAccion" character varying(20),
    "fechaAccion" date,
    "estadoAccion" character varying(1),
    "idClub" integer NOT NULL,
    "idPostulacion" integer NOT NULL
);


ALTER TABLE accion OWNER TO postgres;

--
-- Name: accion_idAccion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "accion_idAccion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "accion_idAccion_seq" OWNER TO postgres;

--
-- Name: accion_idAccion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "accion_idAccion_seq" OWNED BY accion."idAccion";


--
-- Name: acompanante; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE acompanante (
    "idAcompanante" integer NOT NULL,
    "cedulaAcompanante" character varying(10),
    "nombreAcompanante" character varying(50),
    "apellidoAcompanante" character varying(50),
    "estadoAcompanante" character varying(1)
);


ALTER TABLE acompanante OWNER TO postgres;

--
-- Name: acompanante_idAcompanante_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "acompanante_idAcompanante_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "acompanante_idAcompanante_seq" OWNER TO postgres;

--
-- Name: acompanante_idAcompanante_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "acompanante_idAcompanante_seq" OWNED BY acompanante."idAcompanante";


--
-- Name: actividad; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE actividad (
    "idActividad" integer NOT NULL,
    "nombreActividad" character varying(50),
    "descActividad" character varying(255),
    "estadoActividad" character varying(1)
);


ALTER TABLE actividad OWNER TO postgres;

--
-- Name: actividad_idActividad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "actividad_idActividad_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "actividad_idActividad_seq" OWNER TO postgres;

--
-- Name: actividad_idActividad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "actividad_idActividad_seq" OWNED BY actividad."idActividad";


--
-- Name: apelacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE apelacion (
    "idApelacion" integer NOT NULL,
    "descApelacion" text,
    "fechaApelacion" date,
    "estadoApelacion" character varying(1),
    "idSancion" integer NOT NULL
);


ALTER TABLE apelacion OWNER TO postgres;

--
-- Name: apelacion_idApelacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "apelacion_idApelacion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "apelacion_idApelacion_seq" OWNER TO postgres;

--
-- Name: apelacion_idApelacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "apelacion_idApelacion_seq" OWNED BY apelacion."idApelacion";


--
-- Name: area; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE area (
    "idArea" integer NOT NULL,
    "nombreArea" character varying(50),
    "descArea" character varying(255),
    "capacidadPersonaArea" integer,
    "dimensionArea" character varying(50),
    "imagenArea" character varying(100),
    "estadoArea" character varying(1),
    "idClub" integer NOT NULL,
    "idTipoArea" integer NOT NULL
);


ALTER TABLE area OWNER TO postgres;

--
-- Name: area_Recurso; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "area_Recurso" (
    "idArea_recurso" integer NOT NULL,
    "cantArea_recu" integer,
    "estadoArea_recu" character varying(1),
    "idArea" integer NOT NULL,
    "idRecurso" integer NOT NULL
);


ALTER TABLE "area_Recurso" OWNER TO postgres;

--
-- Name: area_Recurso_idArea_recurso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "area_Recurso_idArea_recurso_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "area_Recurso_idArea_recurso_seq" OWNER TO postgres;

--
-- Name: area_Recurso_idArea_recurso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "area_Recurso_idArea_recurso_seq" OWNED BY "area_Recurso"."idArea_recurso";


--
-- Name: area_idArea_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "area_idArea_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "area_idArea_seq" OWNER TO postgres;

--
-- Name: area_idArea_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "area_idArea_seq" OWNED BY area."idArea";


--
-- Name: arrendamiento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE arrendamiento (
    "idArrendamiento" integer NOT NULL,
    "fechaEmisionArrend" date,
    "fechaArrend" date,
    "descArrend" text,
    "pagoArrendamiento" integer,
    "estadoArrend" character varying(1),
    "idArea" integer NOT NULL,
    "idCalendario" integer NOT NULL,
    "idPersona" integer NOT NULL
);


ALTER TABLE arrendamiento OWNER TO postgres;

--
-- Name: arrendamiento_Indicador; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "arrendamiento_Indicador" (
    "idArrendamiento_indi" integer NOT NULL,
    "valorEsperadoArrend" integer,
    "estadoArrend_ind" character varying(1),
    "idArrendamiento" integer NOT NULL,
    "idIndicador" integer NOT NULL
);


ALTER TABLE "arrendamiento_Indicador" OWNER TO postgres;

--
-- Name: arrendamiento_Indicador_idArrendamiento_indi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "arrendamiento_Indicador_idArrendamiento_indi_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "arrendamiento_Indicador_idArrendamiento_indi_seq" OWNER TO postgres;

--
-- Name: arrendamiento_Indicador_idArrendamiento_indi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "arrendamiento_Indicador_idArrendamiento_indi_seq" OWNED BY "arrendamiento_Indicador"."idArrendamiento_indi";


--
-- Name: arrendamiento_idArrendamiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "arrendamiento_idArrendamiento_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "arrendamiento_idArrendamiento_seq" OWNER TO postgres;

--
-- Name: arrendamiento_idArrendamiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "arrendamiento_idArrendamiento_seq" OWNED BY arrendamiento."idArrendamiento";


--
-- Name: asistencia_Evento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "asistencia_Evento" (
    "idAsistencia_Evento" integer NOT NULL,
    "estadoAsist_even" character varying(1),
    "idEvento" integer NOT NULL,
    "idPersona" integer NOT NULL
);


ALTER TABLE "asistencia_Evento" OWNER TO postgres;

--
-- Name: asistencia_Evento_idAsistencia_Evento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "asistencia_Evento_idAsistencia_Evento_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "asistencia_Evento_idAsistencia_Evento_seq" OWNER TO postgres;

--
-- Name: asistencia_Evento_idAsistencia_Evento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "asistencia_Evento_idAsistencia_Evento_seq" OWNED BY "asistencia_Evento"."idAsistencia_Evento";


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: calendario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE calendario (
    "idCalendario" integer NOT NULL,
    "fechaIncio" date,
    "fechaFin" date,
    "estadoCalendario" character varying(1)
);


ALTER TABLE calendario OWNER TO postgres;

--
-- Name: calendario_idCalendario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "calendario_idCalendario_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "calendario_idCalendario_seq" OWNER TO postgres;

--
-- Name: calendario_idCalendario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "calendario_idCalendario_seq" OWNED BY calendario."idCalendario";


--
-- Name: cargo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cargo (
    "idCargo" integer NOT NULL,
    "nombreCargo" character varying(50),
    "descCargo" character varying(255),
    "estadoCargo" character varying(1)
);


ALTER TABLE cargo OWNER TO postgres;

--
-- Name: cargo_idCargo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "cargo_idCargo_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "cargo_idCargo_seq" OWNER TO postgres;

--
-- Name: cargo_idCargo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "cargo_idCargo_seq" OWNED BY cargo."idCargo";


--
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categoria (
    "idCategoria" integer NOT NULL,
    "nombreCategoria" character varying(50),
    "descCategoria" character varying(255),
    "estadoCategoria" character varying(1)
);


ALTER TABLE categoria OWNER TO postgres;

--
-- Name: categoria_idCategoria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "categoria_idCategoria_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "categoria_idCategoria_seq" OWNER TO postgres;

--
-- Name: categoria_idCategoria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "categoria_idCategoria_seq" OWNED BY categoria."idCategoria";


--
-- Name: club; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE club (
    "idClub" integer NOT NULL,
    "nombreClub" character varying(50),
    "descClub" character varying(255),
    direccion character varying(255),
    "telefonoClub" character varying(20),
    "idDocumento" integer NOT NULL,
    "idGaleria" integer NOT NULL
);


ALTER TABLE club OWNER TO postgres;

--
-- Name: club_idClub_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "club_idClub_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "club_idClub_seq" OWNER TO postgres;

--
-- Name: club_idClub_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "club_idClub_seq" OWNED BY club."idClub";


--
-- Name: comision; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comision (
    "idComision" integer NOT NULL,
    "nombreComision" character varying(50),
    "descComision" text,
    "estadoComision" text,
    "idActividad" integer NOT NULL
);


ALTER TABLE comision OWNER TO postgres;

--
-- Name: comision_idComision_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "comision_idComision_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "comision_idComision_seq" OWNER TO postgres;

--
-- Name: comision_idComision_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "comision_idComision_seq" OWNED BY comision."idComision";


--
-- Name: corsheaders_corsmodel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE corsheaders_corsmodel (
    id integer NOT NULL,
    cors character varying(255) NOT NULL
);


ALTER TABLE corsheaders_corsmodel OWNER TO postgres;

--
-- Name: corsheaders_corsmodel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE corsheaders_corsmodel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE corsheaders_corsmodel_id_seq OWNER TO postgres;

--
-- Name: corsheaders_corsmodel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE corsheaders_corsmodel_id_seq OWNED BY corsheaders_corsmodel.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO postgres;

--
-- Name: documento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE documento (
    "idDocumento" integer NOT NULL,
    "nombreDocumento" character varying(50),
    "archivoDocumento" character varying(255),
    "estadoDocumento" character varying(1)
);


ALTER TABLE documento OWNER TO postgres;

--
-- Name: documento_idDocumento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "documento_idDocumento_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "documento_idDocumento_seq" OWNER TO postgres;

--
-- Name: documento_idDocumento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "documento_idDocumento_seq" OWNED BY documento."idDocumento";


--
-- Name: encuesta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE encuesta (
    "idEncuesta" integer NOT NULL,
    "nombreEncuesta" character varying(50),
    "descEncuesta" text,
    "fechaEncuesta" date,
    "estadoEncuesta" text
);


ALTER TABLE encuesta OWNER TO postgres;

--
-- Name: encuesta_idEncuesta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "encuesta_idEncuesta_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "encuesta_idEncuesta_seq" OWNER TO postgres;

--
-- Name: encuesta_idEncuesta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "encuesta_idEncuesta_seq" OWNED BY encuesta."idEncuesta";


--
-- Name: evento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE evento (
    "idEvento" integer NOT NULL,
    "nombreEvento" character varying(50),
    "decEvento" character varying(255),
    "fechaEvento" date,
    "estadoEvento" character varying(50),
    "idCalendario" integer NOT NULL,
    "idPersona" integer NOT NULL,
    "idTipoEvento" integer NOT NULL,
    "idTipoPublico" integer NOT NULL
);


ALTER TABLE evento OWNER TO postgres;

--
-- Name: evento_Comision; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "evento_Comision" (
    "idEvento_comision" integer NOT NULL,
    responsable character varying(50),
    "estadoEvento_comi" character varying(1),
    "idComision" integer NOT NULL,
    "idEvento" integer NOT NULL
);


ALTER TABLE "evento_Comision" OWNER TO postgres;

--
-- Name: evento_Comision_idEvento_comision_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "evento_Comision_idEvento_comision_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "evento_Comision_idEvento_comision_seq" OWNER TO postgres;

--
-- Name: evento_Comision_idEvento_comision_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "evento_Comision_idEvento_comision_seq" OWNED BY "evento_Comision"."idEvento_comision";


--
-- Name: evento_Indicador; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "evento_Indicador" (
    "idEvento_indicador" integer NOT NULL,
    "valorEsperadoEve" integer,
    "idEvento" integer NOT NULL,
    "idIndicador" integer NOT NULL
);


ALTER TABLE "evento_Indicador" OWNER TO postgres;

--
-- Name: evento_Indicador_idEvento_indicador_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "evento_Indicador_idEvento_indicador_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "evento_Indicador_idEvento_indicador_seq" OWNER TO postgres;

--
-- Name: evento_Indicador_idEvento_indicador_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "evento_Indicador_idEvento_indicador_seq" OWNED BY "evento_Indicador"."idEvento_indicador";


--
-- Name: evento_Recurso; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "evento_Recurso" (
    "idEvento_recurso" integer NOT NULL,
    cantidad integer,
    "estadoEvento_recu" character varying(1),
    "idEvento" integer NOT NULL,
    "idRecurso" integer NOT NULL
);


ALTER TABLE "evento_Recurso" OWNER TO postgres;

--
-- Name: evento_Recurso_idEvento_recurso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "evento_Recurso_idEvento_recurso_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "evento_Recurso_idEvento_recurso_seq" OWNER TO postgres;

--
-- Name: evento_Recurso_idEvento_recurso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "evento_Recurso_idEvento_recurso_seq" OWNED BY "evento_Recurso"."idEvento_recurso";


--
-- Name: evento_idEvento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "evento_idEvento_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "evento_idEvento_seq" OWNER TO postgres;

--
-- Name: evento_idEvento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "evento_idEvento_seq" OWNED BY evento."idEvento";


--
-- Name: funcion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE funcion (
    id integer NOT NULL,
    id_padre integer,
    iconuri character varying(255),
    pagina character varying(255),
    extra character varying(255),
    nombre character varying(255)
);


ALTER TABLE funcion OWNER TO postgres;

--
-- Name: funcion_Grupo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "funcion_Grupo" (
    id integer NOT NULL,
    "estadoFuncion_grupo" character varying(1),
    "idFuncion" integer NOT NULL,
    "idGrupo" integer NOT NULL
);


ALTER TABLE "funcion_Grupo" OWNER TO postgres;

--
-- Name: funcion_Grupo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "funcion_Grupo_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "funcion_Grupo_id_seq" OWNER TO postgres;

--
-- Name: funcion_Grupo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "funcion_Grupo_id_seq" OWNED BY "funcion_Grupo".id;


--
-- Name: funcion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE funcion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE funcion_id_seq OWNER TO postgres;

--
-- Name: funcion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE funcion_id_seq OWNED BY funcion.id;


--
-- Name: galeria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE galeria (
    "idGaleria" integer NOT NULL,
    "nombreGaleria" character varying(50),
    "imagenGaleria" character varying(100),
    "estadoGaleria" character varying(1)
);


ALTER TABLE galeria OWNER TO postgres;

--
-- Name: galeria_idGaleria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "galeria_idGaleria_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "galeria_idGaleria_seq" OWNER TO postgres;

--
-- Name: galeria_idGaleria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "galeria_idGaleria_seq" OWNED BY galeria."idGaleria";


--
-- Name: genero; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE genero (
    "idGenero" integer NOT NULL,
    "nombreGenero" character varying(50),
    "descGenero" character varying(255),
    "estadoGenero" character varying(1)
);


ALTER TABLE genero OWNER TO postgres;

--
-- Name: genero_idGenero_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "genero_idGenero_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "genero_idGenero_seq" OWNER TO postgres;

--
-- Name: genero_idGenero_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "genero_idGenero_seq" OWNED BY genero."idGenero";


--
-- Name: grupo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE grupo (
    "idGrupo" integer NOT NULL,
    "nombreGrupo" character varying(40),
    "descGrupo" character varying(255),
    "estadoGrupo" character varying(1)
);


ALTER TABLE grupo OWNER TO postgres;

--
-- Name: grupo_idGrupo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "grupo_idGrupo_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "grupo_idGrupo_seq" OWNER TO postgres;

--
-- Name: grupo_idGrupo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "grupo_idGrupo_seq" OWNED BY grupo."idGrupo";


--
-- Name: incidencia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE incidencia (
    "idIncidencia" integer NOT NULL,
    "descIncidencia" text,
    "fechaIncidencia" date,
    "horaIncidencia" date,
    "estadoIncidencia" character varying(1),
    "idArrendamiento" integer,
    "idEvento" integer,
    "idPersona" integer NOT NULL,
    "idTipoIncid" integer NOT NULL,
    "idVisita" integer
);


ALTER TABLE incidencia OWNER TO postgres;

--
-- Name: incidencia_idIncidencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "incidencia_idIncidencia_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "incidencia_idIncidencia_seq" OWNER TO postgres;

--
-- Name: incidencia_idIncidencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "incidencia_idIncidencia_seq" OWNED BY incidencia."idIncidencia";


--
-- Name: indicador; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE indicador (
    "idIndicador" integer NOT NULL,
    "nombreIndicador" character varying(50),
    "descIndicador" text,
    "estadoIndicador" character varying(1),
    "idTipoindicador" integer NOT NULL,
    "idUnidadMedida" integer NOT NULL
);


ALTER TABLE indicador OWNER TO postgres;

--
-- Name: indicador_idIndicador_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "indicador_idIndicador_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "indicador_idIndicador_seq" OWNER TO postgres;

--
-- Name: indicador_idIndicador_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "indicador_idIndicador_seq" OWNED BY indicador."idIndicador";


--
-- Name: morosidad; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE morosidad (
    "idMorosidad" integer NOT NULL,
    "fechaMorosidad" date,
    "nroMesPendiente" integer,
    "estadoMorosidad" character varying(1),
    "idPersona" integer NOT NULL
);


ALTER TABLE morosidad OWNER TO postgres;

--
-- Name: morosidad_idMorosidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "morosidad_idMorosidad_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "morosidad_idMorosidad_seq" OWNER TO postgres;

--
-- Name: morosidad_idMorosidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "morosidad_idMorosidad_seq" OWNED BY morosidad."idMorosidad";


--
-- Name: noticia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE noticia (
    "idNoticia" integer NOT NULL,
    "tituloNoticia" character varying(50),
    "imagenNoticia" character varying(100),
    "dufusionNoticia" character varying(20),
    "descNoticia" text,
    "fechaIniNoticia" date,
    "fechaExpNoticia" date,
    "estadoNoticia" text,
    "idClub" integer NOT NULL,
    "idTipoNoticia" integer NOT NULL
);


ALTER TABLE noticia OWNER TO postgres;

--
-- Name: noticia_idNoticia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "noticia_idNoticia_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "noticia_idNoticia_seq" OWNER TO postgres;

--
-- Name: noticia_idNoticia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "noticia_idNoticia_seq" OWNED BY noticia."idNoticia";


--
-- Name: notificacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE notificacion (
    "idNotificacion" integer NOT NULL,
    "asuntoNotificacion" character varying(50),
    "descNotificacion" character varying(255),
    "estadoNotificacion" character varying(1),
    "idPersona" integer NOT NULL,
    "idTipoNotificacion" integer NOT NULL
);


ALTER TABLE notificacion OWNER TO postgres;

--
-- Name: notificacion_idNotificacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "notificacion_idNotificacion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "notificacion_idNotificacion_seq" OWNER TO postgres;

--
-- Name: notificacion_idNotificacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "notificacion_idNotificacion_seq" OWNED BY notificacion."idNotificacion";


--
-- Name: opcionPregunta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "opcionPregunta" (
    "idOpcionPregunta" integer NOT NULL,
    "nombreOpcion" character varying(30),
    "estadoOpcionPreg" character varying(1)
);


ALTER TABLE "opcionPregunta" OWNER TO postgres;

--
-- Name: opcionPregunta_idOpcionPregunta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "opcionPregunta_idOpcionPregunta_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "opcionPregunta_idOpcionPregunta_seq" OWNER TO postgres;

--
-- Name: opcionPregunta_idOpcionPregunta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "opcionPregunta_idOpcionPregunta_seq" OWNED BY "opcionPregunta"."idOpcionPregunta";


--
-- Name: opinion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE opinion (
    "idOpinion" integer NOT NULL,
    "asuntoOpinion" text,
    "descOpinion" text,
    "estadoOpinion" character varying(1),
    "idTipoOpinion" integer NOT NULL,
    "idUsuario" integer NOT NULL
);


ALTER TABLE opinion OWNER TO postgres;

--
-- Name: opinion_idOpinion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "opinion_idOpinion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "opinion_idOpinion_seq" OWNER TO postgres;

--
-- Name: opinion_idOpinion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "opinion_idOpinion_seq" OWNED BY opinion."idOpinion";


--
-- Name: parentesco; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE parentesco (
    "idParentesco" integer NOT NULL,
    "nombreParentesco" character varying(50),
    "descParentesco" character varying(255),
    "estadoParentesco" character varying(1)
);


ALTER TABLE parentesco OWNER TO postgres;

--
-- Name: parentesco_idParentesco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "parentesco_idParentesco_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "parentesco_idParentesco_seq" OWNER TO postgres;

--
-- Name: parentesco_idParentesco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "parentesco_idParentesco_seq" OWNED BY parentesco."idParentesco";


--
-- Name: persona; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE persona (
    "idPersona" integer NOT NULL,
    nombre character varying(50),
    apellido character varying(50),
    cedula character varying(10),
    direccion text,
    "correoElectronico" character varying(80),
    "fechaNacimiento" date,
    sexo character varying(1),
    "lugarNacimiento" character varying(50),
    nacionalidad character varying(50),
    profesion character varying(50),
    "cargoTrabajo" character varying(50),
    "direccionTrabajo" character varying(255),
    sueldo integer,
    hobby character varying(50),
    "estadoCivil" character varying(50),
    "tlfCasa" character varying(20),
    "tlfCel" character varying(20),
    "tlfTrabajo" character varying(20),
    "idPadre" integer,
    "imagenPersona" character varying(150),
    "idCargo" integer,
    "idParentesco" integer,
    "idTipoPersona" integer
);


ALTER TABLE persona OWNER TO postgres;

--
-- Name: persona_idPersona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "persona_idPersona_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "persona_idPersona_seq" OWNER TO postgres;

--
-- Name: persona_idPersona_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "persona_idPersona_seq" OWNED BY persona."idPersona";


--
-- Name: persona_preferencia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE persona_preferencia (
    "idPersona_pref" integer NOT NULL,
    "estadoPersona_pref" character varying(1),
    "idPersona" integer NOT NULL,
    "idPreferencia" integer NOT NULL
);


ALTER TABLE persona_preferencia OWNER TO postgres;

--
-- Name: persona_preferencia_idPersona_pref_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "persona_preferencia_idPersona_pref_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "persona_preferencia_idPersona_pref_seq" OWNER TO postgres;

--
-- Name: persona_preferencia_idPersona_pref_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "persona_preferencia_idPersona_pref_seq" OWNED BY persona_preferencia."idPersona_pref";


--
-- Name: postulacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE postulacion (
    "idPostulacion" integer NOT NULL,
    "fechaPostulacion" date,
    "estadoPostulacion" character varying(1),
    "idPersona" integer NOT NULL,
    "idReferencia" integer NOT NULL
);


ALTER TABLE postulacion OWNER TO postgres;

--
-- Name: postulacion_idPostulacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "postulacion_idPostulacion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "postulacion_idPostulacion_seq" OWNER TO postgres;

--
-- Name: postulacion_idPostulacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "postulacion_idPostulacion_seq" OWNED BY postulacion."idPostulacion";


--
-- Name: preferencia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE preferencia (
    "idPreferencia" integer NOT NULL,
    "nombrePreferencia" character varying(50),
    "descPreferencia" character varying(255),
    "idCategoria" integer NOT NULL
);


ALTER TABLE preferencia OWNER TO postgres;

--
-- Name: preferencia_idPreferencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "preferencia_idPreferencia_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "preferencia_idPreferencia_seq" OWNER TO postgres;

--
-- Name: preferencia_idPreferencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "preferencia_idPreferencia_seq" OWNED BY preferencia."idPreferencia";


--
-- Name: pregunta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pregunta (
    "idPregunta" integer NOT NULL,
    "nombrePregunta" character varying(50),
    "estadoPregunta" character varying(1),
    "idCategoria" integer NOT NULL,
    "idEncuesta" integer NOT NULL,
    "idOpcionPregunta" integer NOT NULL
);


ALTER TABLE pregunta OWNER TO postgres;

--
-- Name: pregunta_idPregunta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "pregunta_idPregunta_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "pregunta_idPregunta_seq" OWNER TO postgres;

--
-- Name: pregunta_idPregunta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "pregunta_idPregunta_seq" OWNED BY pregunta."idPregunta";


--
-- Name: rechazo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE rechazo (
    "idRechazo" integer NOT NULL,
    "descRechazo" text,
    "estadoRechazo" character varying(20),
    "idApelacion" integer,
    "idArrendamiento" integer,
    "idEvento" integer,
    "idPostulacion" integer,
    "idTipoRechazo" integer NOT NULL
);


ALTER TABLE rechazo OWNER TO postgres;

--
-- Name: rechazo_idRechazo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "rechazo_idRechazo_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "rechazo_idRechazo_seq" OWNER TO postgres;

--
-- Name: rechazo_idRechazo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "rechazo_idRechazo_seq" OWNED BY rechazo."idRechazo";


--
-- Name: recurso; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE recurso (
    "idRecurso" integer NOT NULL,
    "nombreRecurso" character varying(50) NOT NULL,
    "cantidadRecurso" integer,
    "descRecurso" character varying(255),
    "estadoRecurso" character varying(1)
);


ALTER TABLE recurso OWNER TO postgres;

--
-- Name: recurso_idRecurso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "recurso_idRecurso_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "recurso_idRecurso_seq" OWNER TO postgres;

--
-- Name: recurso_idRecurso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "recurso_idRecurso_seq" OWNED BY recurso."idRecurso";


--
-- Name: referencia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE referencia (
    "idReferencia" integer NOT NULL,
    "nombreReferencia" character varying(50),
    "apellidoReferencia" character varying(50),
    "tlfReferencia" character varying(20),
    "estadoReferencia" character varying(1)
);


ALTER TABLE referencia OWNER TO postgres;

--
-- Name: referencia_idReferencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "referencia_idReferencia_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "referencia_idReferencia_seq" OWNER TO postgres;

--
-- Name: referencia_idReferencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "referencia_idReferencia_seq" OWNED BY referencia."idReferencia";


--
-- Name: respuestaEncuesta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "respuestaEncuesta" (
    "idRespuestaEncuesta" integer NOT NULL,
    "fechaRespuesta" date,
    "estadoRespuestaEnc" character varying(1),
    "idPregunta" integer NOT NULL,
    "idUsuario" integer NOT NULL
);


ALTER TABLE "respuestaEncuesta" OWNER TO postgres;

--
-- Name: respuestaEncuesta_idRespuestaEncuesta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "respuestaEncuesta_idRespuestaEncuesta_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "respuestaEncuesta_idRespuestaEncuesta_seq" OWNER TO postgres;

--
-- Name: respuestaEncuesta_idRespuestaEncuesta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "respuestaEncuesta_idRespuestaEncuesta_seq" OWNED BY "respuestaEncuesta"."idRespuestaEncuesta";


--
-- Name: resultado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE resultado (
    "idResultado" integer NOT NULL,
    "valorRealResultado" integer,
    "calculoResultado" integer,
    "descResultado" character varying(255),
    "estadoResultado" character varying(1),
    "idArrendamiento" integer,
    "idEvento" integer
);


ALTER TABLE resultado OWNER TO postgres;

--
-- Name: resultado_idResultado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "resultado_idResultado_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "resultado_idResultado_seq" OWNER TO postgres;

--
-- Name: resultado_idResultado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "resultado_idResultado_seq" OWNED BY resultado."idResultado";


--
-- Name: sancion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sancion (
    "idSancion" integer NOT NULL,
    "descSancion" text,
    "fechaSancion" date,
    "estadoSancion" character varying(1),
    "idIncidencia" integer NOT NULL,
    "idTipoSancion" integer NOT NULL
);


ALTER TABLE sancion OWNER TO postgres;

--
-- Name: sancion_idSancion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "sancion_idSancion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "sancion_idSancion_seq" OWNER TO postgres;

--
-- Name: sancion_idSancion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "sancion_idSancion_seq" OWNED BY sancion."idSancion";


--
-- Name: tipoArea; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoArea" (
    "idTipoArea" integer NOT NULL,
    "nombreTipoArea" character varying(50),
    "descTipoArea" character varying(255),
    "estadoTipoArea" character varying(1)
);


ALTER TABLE "tipoArea" OWNER TO postgres;

--
-- Name: tipoArea_idTipoArea_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoArea_idTipoArea_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "tipoArea_idTipoArea_seq" OWNER TO postgres;

--
-- Name: tipoArea_idTipoArea_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoArea_idTipoArea_seq" OWNED BY "tipoArea"."idTipoArea";


--
-- Name: tipoEvento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoEvento" (
    "idTipoEvento" integer NOT NULL,
    "nombreTipoEvento" character varying(50),
    "descTipoEvento" text,
    "estadoTipoEvento" character varying(1)
);


ALTER TABLE "tipoEvento" OWNER TO postgres;

--
-- Name: tipoEvento_idTipoEvento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoEvento_idTipoEvento_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "tipoEvento_idTipoEvento_seq" OWNER TO postgres;

--
-- Name: tipoEvento_idTipoEvento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoEvento_idTipoEvento_seq" OWNED BY "tipoEvento"."idTipoEvento";


--
-- Name: tipoIncidencia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoIncidencia" (
    "idTipoIncid" integer NOT NULL,
    "nombreTipoIncid" character varying(50),
    "descTipoIncid" text,
    "estadoTipoIncid" text
);


ALTER TABLE "tipoIncidencia" OWNER TO postgres;

--
-- Name: tipoIncidencia_idTipoIncid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoIncidencia_idTipoIncid_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "tipoIncidencia_idTipoIncid_seq" OWNER TO postgres;

--
-- Name: tipoIncidencia_idTipoIncid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoIncidencia_idTipoIncid_seq" OWNED BY "tipoIncidencia"."idTipoIncid";


--
-- Name: tipoIndicador; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoIndicador" (
    "idTipoindicador" integer NOT NULL,
    "nombreTipoindicador" character varying(50),
    "descTipoIndicador" text,
    "estadoTipoIndicador" character varying(1)
);


ALTER TABLE "tipoIndicador" OWNER TO postgres;

--
-- Name: tipoIndicador_idTipoindicador_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoIndicador_idTipoindicador_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "tipoIndicador_idTipoindicador_seq" OWNER TO postgres;

--
-- Name: tipoIndicador_idTipoindicador_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoIndicador_idTipoindicador_seq" OWNED BY "tipoIndicador"."idTipoindicador";


--
-- Name: tipoNoticia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoNoticia" (
    "idTipoNoticia" integer NOT NULL,
    "nombreTiponoticia" character varying(50),
    "descTipoNoticia" text,
    "estadoTipoNoticia" character varying(1)
);


ALTER TABLE "tipoNoticia" OWNER TO postgres;

--
-- Name: tipoNoticia_idTipoNoticia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoNoticia_idTipoNoticia_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "tipoNoticia_idTipoNoticia_seq" OWNER TO postgres;

--
-- Name: tipoNoticia_idTipoNoticia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoNoticia_idTipoNoticia_seq" OWNED BY "tipoNoticia"."idTipoNoticia";


--
-- Name: tipoNotificacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoNotificacion" (
    "idTipoNotificacion" integer NOT NULL,
    "nombreTipoNotif" character varying(50),
    "descTipoNotif" character varying(255),
    "estadoTipoNotif" character varying(1)
);


ALTER TABLE "tipoNotificacion" OWNER TO postgres;

--
-- Name: tipoNotificacion_idTipoNotificacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoNotificacion_idTipoNotificacion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "tipoNotificacion_idTipoNotificacion_seq" OWNER TO postgres;

--
-- Name: tipoNotificacion_idTipoNotificacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoNotificacion_idTipoNotificacion_seq" OWNED BY "tipoNotificacion"."idTipoNotificacion";


--
-- Name: tipoOpinion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoOpinion" (
    "idTipoOpinion" integer NOT NULL,
    "nombreTipoOpinion" character varying(50),
    "descTipoOpinion" text
);


ALTER TABLE "tipoOpinion" OWNER TO postgres;

--
-- Name: tipoOpinion_idTipoOpinion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoOpinion_idTipoOpinion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "tipoOpinion_idTipoOpinion_seq" OWNER TO postgres;

--
-- Name: tipoOpinion_idTipoOpinion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoOpinion_idTipoOpinion_seq" OWNED BY "tipoOpinion"."idTipoOpinion";


--
-- Name: tipoPersona; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoPersona" (
    "idTipoPersona" integer NOT NULL,
    "nombreTipoPersona" character varying(50),
    "descTipoPersona" character varying(255),
    "estadoTipoPersona" text
);


ALTER TABLE "tipoPersona" OWNER TO postgres;

--
-- Name: tipoPersona_idTipoPersona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoPersona_idTipoPersona_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "tipoPersona_idTipoPersona_seq" OWNER TO postgres;

--
-- Name: tipoPersona_idTipoPersona_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoPersona_idTipoPersona_seq" OWNED BY "tipoPersona"."idTipoPersona";


--
-- Name: tipoPublico; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoPublico" (
    "idTipoPublico" integer NOT NULL,
    "nombreTipoPublico" character varying(50),
    "descTipoPublico" text,
    "estadoTipoPublico" character varying(1)
);


ALTER TABLE "tipoPublico" OWNER TO postgres;

--
-- Name: tipoPublico_idTipoPublico_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoPublico_idTipoPublico_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "tipoPublico_idTipoPublico_seq" OWNER TO postgres;

--
-- Name: tipoPublico_idTipoPublico_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoPublico_idTipoPublico_seq" OWNED BY "tipoPublico"."idTipoPublico";


--
-- Name: tipoRechazo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoRechazo" (
    "idTipoRechazo" integer NOT NULL,
    "nombreTipoRechazo" character varying(50),
    "descTipoRechazo" text,
    "estadoTipoRechazo" text
);


ALTER TABLE "tipoRechazo" OWNER TO postgres;

--
-- Name: tipoRechazo_idTipoRechazo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoRechazo_idTipoRechazo_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "tipoRechazo_idTipoRechazo_seq" OWNER TO postgres;

--
-- Name: tipoRechazo_idTipoRechazo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoRechazo_idTipoRechazo_seq" OWNED BY "tipoRechazo"."idTipoRechazo";


--
-- Name: tipoSancion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoSancion" (
    "idTipoSancion" integer NOT NULL,
    "nombreTipoSancion" character varying(50),
    "descTipoSancion" text,
    "estadoTipoSancion" text
);


ALTER TABLE "tipoSancion" OWNER TO postgres;

--
-- Name: tipoSancion_idTipoSancion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoSancion_idTipoSancion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "tipoSancion_idTipoSancion_seq" OWNER TO postgres;

--
-- Name: tipoSancion_idTipoSancion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoSancion_idTipoSancion_seq" OWNED BY "tipoSancion"."idTipoSancion";


--
-- Name: tipoUnidadMedida; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoUnidadMedida" (
    "idTipoUnidadMedida" integer NOT NULL,
    "nombreTipoUnidad" character varying(50),
    "descTipoUnidadMedida" text
);


ALTER TABLE "tipoUnidadMedida" OWNER TO postgres;

--
-- Name: tipoUnidadMedida_idTipoUnidadMedida_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoUnidadMedida_idTipoUnidadMedida_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "tipoUnidadMedida_idTipoUnidadMedida_seq" OWNER TO postgres;

--
-- Name: tipoUnidadMedida_idTipoUnidadMedida_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoUnidadMedida_idTipoUnidadMedida_seq" OWNED BY "tipoUnidadMedida"."idTipoUnidadMedida";


--
-- Name: unidadMedida; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "unidadMedida" (
    "idUnidadMedida" integer NOT NULL,
    "nombreUnidadMedida" character varying(50),
    "descUnidadMedida" text,
    "estadoUnidadMedida" character varying(1)
);


ALTER TABLE "unidadMedida" OWNER TO postgres;

--
-- Name: unidadMedida_idUnidadMedida_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "unidadMedida_idUnidadMedida_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "unidadMedida_idUnidadMedida_seq" OWNER TO postgres;

--
-- Name: unidadMedida_idUnidadMedida_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "unidadMedida_idUnidadMedida_seq" OWNED BY "unidadMedida"."idUnidadMedida";


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    "idUsuario" integer NOT NULL,
    "nombreUsuario" character varying(20),
    contrasena character varying(50),
    "preguntaSereta" character varying(100),
    "respuestaSecreta" character varying(100),
    "fechaCreacionUsuario" date,
    "estadoUsuario" character varying(1),
    "idGrupo" integer NOT NULL,
    "idPersona" integer NOT NULL
);


ALTER TABLE usuario OWNER TO postgres;

--
-- Name: usuario_idUsuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "usuario_idUsuario_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "usuario_idUsuario_seq" OWNER TO postgres;

--
-- Name: usuario_idUsuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "usuario_idUsuario_seq" OWNED BY usuario."idUsuario";


--
-- Name: visita; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE visita (
    "idVisita" integer NOT NULL,
    "fechaVisita" date,
    "horaEntrada" timestamp with time zone,
    "estadoVisita" character varying(1),
    "idAcompanante" integer NOT NULL,
    "idPersona" integer NOT NULL
);


ALTER TABLE visita OWNER TO postgres;

--
-- Name: visita_idVisita_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "visita_idVisita_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "visita_idVisita_seq" OWNER TO postgres;

--
-- Name: visita_idVisita_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "visita_idVisita_seq" OWNED BY visita."idVisita";


--
-- Name: idAccion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accion ALTER COLUMN "idAccion" SET DEFAULT nextval('"accion_idAccion_seq"'::regclass);


--
-- Name: idAcompanante; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY acompanante ALTER COLUMN "idAcompanante" SET DEFAULT nextval('"acompanante_idAcompanante_seq"'::regclass);


--
-- Name: idActividad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY actividad ALTER COLUMN "idActividad" SET DEFAULT nextval('"actividad_idActividad_seq"'::regclass);


--
-- Name: idApelacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY apelacion ALTER COLUMN "idApelacion" SET DEFAULT nextval('"apelacion_idApelacion_seq"'::regclass);


--
-- Name: idArea; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area ALTER COLUMN "idArea" SET DEFAULT nextval('"area_idArea_seq"'::regclass);


--
-- Name: idArea_recurso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "area_Recurso" ALTER COLUMN "idArea_recurso" SET DEFAULT nextval('"area_Recurso_idArea_recurso_seq"'::regclass);


--
-- Name: idArrendamiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY arrendamiento ALTER COLUMN "idArrendamiento" SET DEFAULT nextval('"arrendamiento_idArrendamiento_seq"'::regclass);


--
-- Name: idArrendamiento_indi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "arrendamiento_Indicador" ALTER COLUMN "idArrendamiento_indi" SET DEFAULT nextval('"arrendamiento_Indicador_idArrendamiento_indi_seq"'::regclass);


--
-- Name: idAsistencia_Evento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "asistencia_Evento" ALTER COLUMN "idAsistencia_Evento" SET DEFAULT nextval('"asistencia_Evento_idAsistencia_Evento_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: idCalendario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY calendario ALTER COLUMN "idCalendario" SET DEFAULT nextval('"calendario_idCalendario_seq"'::regclass);


--
-- Name: idCargo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cargo ALTER COLUMN "idCargo" SET DEFAULT nextval('"cargo_idCargo_seq"'::regclass);


--
-- Name: idCategoria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria ALTER COLUMN "idCategoria" SET DEFAULT nextval('"categoria_idCategoria_seq"'::regclass);


--
-- Name: idClub; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY club ALTER COLUMN "idClub" SET DEFAULT nextval('"club_idClub_seq"'::regclass);


--
-- Name: idComision; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comision ALTER COLUMN "idComision" SET DEFAULT nextval('"comision_idComision_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY corsheaders_corsmodel ALTER COLUMN id SET DEFAULT nextval('corsheaders_corsmodel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: idDocumento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documento ALTER COLUMN "idDocumento" SET DEFAULT nextval('"documento_idDocumento_seq"'::regclass);


--
-- Name: idEncuesta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY encuesta ALTER COLUMN "idEncuesta" SET DEFAULT nextval('"encuesta_idEncuesta_seq"'::regclass);


--
-- Name: idEvento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento ALTER COLUMN "idEvento" SET DEFAULT nextval('"evento_idEvento_seq"'::regclass);


--
-- Name: idEvento_comision; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "evento_Comision" ALTER COLUMN "idEvento_comision" SET DEFAULT nextval('"evento_Comision_idEvento_comision_seq"'::regclass);


--
-- Name: idEvento_indicador; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "evento_Indicador" ALTER COLUMN "idEvento_indicador" SET DEFAULT nextval('"evento_Indicador_idEvento_indicador_seq"'::regclass);


--
-- Name: idEvento_recurso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "evento_Recurso" ALTER COLUMN "idEvento_recurso" SET DEFAULT nextval('"evento_Recurso_idEvento_recurso_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY funcion ALTER COLUMN id SET DEFAULT nextval('funcion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "funcion_Grupo" ALTER COLUMN id SET DEFAULT nextval('"funcion_Grupo_id_seq"'::regclass);


--
-- Name: idGaleria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY galeria ALTER COLUMN "idGaleria" SET DEFAULT nextval('"galeria_idGaleria_seq"'::regclass);


--
-- Name: idGenero; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY genero ALTER COLUMN "idGenero" SET DEFAULT nextval('"genero_idGenero_seq"'::regclass);


--
-- Name: idGrupo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY grupo ALTER COLUMN "idGrupo" SET DEFAULT nextval('"grupo_idGrupo_seq"'::regclass);


--
-- Name: idIncidencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY incidencia ALTER COLUMN "idIncidencia" SET DEFAULT nextval('"incidencia_idIncidencia_seq"'::regclass);


--
-- Name: idIndicador; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicador ALTER COLUMN "idIndicador" SET DEFAULT nextval('"indicador_idIndicador_seq"'::regclass);


--
-- Name: idMorosidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY morosidad ALTER COLUMN "idMorosidad" SET DEFAULT nextval('"morosidad_idMorosidad_seq"'::regclass);


--
-- Name: idNoticia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noticia ALTER COLUMN "idNoticia" SET DEFAULT nextval('"noticia_idNoticia_seq"'::regclass);


--
-- Name: idNotificacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notificacion ALTER COLUMN "idNotificacion" SET DEFAULT nextval('"notificacion_idNotificacion_seq"'::regclass);


--
-- Name: idOpcionPregunta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "opcionPregunta" ALTER COLUMN "idOpcionPregunta" SET DEFAULT nextval('"opcionPregunta_idOpcionPregunta_seq"'::regclass);


--
-- Name: idOpinion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY opinion ALTER COLUMN "idOpinion" SET DEFAULT nextval('"opinion_idOpinion_seq"'::regclass);


--
-- Name: idParentesco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parentesco ALTER COLUMN "idParentesco" SET DEFAULT nextval('"parentesco_idParentesco_seq"'::regclass);


--
-- Name: idPersona; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona ALTER COLUMN "idPersona" SET DEFAULT nextval('"persona_idPersona_seq"'::regclass);


--
-- Name: idPersona_pref; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona_preferencia ALTER COLUMN "idPersona_pref" SET DEFAULT nextval('"persona_preferencia_idPersona_pref_seq"'::regclass);


--
-- Name: idPostulacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY postulacion ALTER COLUMN "idPostulacion" SET DEFAULT nextval('"postulacion_idPostulacion_seq"'::regclass);


--
-- Name: idPreferencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY preferencia ALTER COLUMN "idPreferencia" SET DEFAULT nextval('"preferencia_idPreferencia_seq"'::regclass);


--
-- Name: idPregunta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pregunta ALTER COLUMN "idPregunta" SET DEFAULT nextval('"pregunta_idPregunta_seq"'::regclass);


--
-- Name: idRechazo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rechazo ALTER COLUMN "idRechazo" SET DEFAULT nextval('"rechazo_idRechazo_seq"'::regclass);


--
-- Name: idRecurso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recurso ALTER COLUMN "idRecurso" SET DEFAULT nextval('"recurso_idRecurso_seq"'::regclass);


--
-- Name: idReferencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY referencia ALTER COLUMN "idReferencia" SET DEFAULT nextval('"referencia_idReferencia_seq"'::regclass);


--
-- Name: idRespuestaEncuesta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "respuestaEncuesta" ALTER COLUMN "idRespuestaEncuesta" SET DEFAULT nextval('"respuestaEncuesta_idRespuestaEncuesta_seq"'::regclass);


--
-- Name: idResultado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultado ALTER COLUMN "idResultado" SET DEFAULT nextval('"resultado_idResultado_seq"'::regclass);


--
-- Name: idSancion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sancion ALTER COLUMN "idSancion" SET DEFAULT nextval('"sancion_idSancion_seq"'::regclass);


--
-- Name: idTipoArea; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoArea" ALTER COLUMN "idTipoArea" SET DEFAULT nextval('"tipoArea_idTipoArea_seq"'::regclass);


--
-- Name: idTipoEvento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoEvento" ALTER COLUMN "idTipoEvento" SET DEFAULT nextval('"tipoEvento_idTipoEvento_seq"'::regclass);


--
-- Name: idTipoIncid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoIncidencia" ALTER COLUMN "idTipoIncid" SET DEFAULT nextval('"tipoIncidencia_idTipoIncid_seq"'::regclass);


--
-- Name: idTipoindicador; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoIndicador" ALTER COLUMN "idTipoindicador" SET DEFAULT nextval('"tipoIndicador_idTipoindicador_seq"'::regclass);


--
-- Name: idTipoNoticia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoNoticia" ALTER COLUMN "idTipoNoticia" SET DEFAULT nextval('"tipoNoticia_idTipoNoticia_seq"'::regclass);


--
-- Name: idTipoNotificacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoNotificacion" ALTER COLUMN "idTipoNotificacion" SET DEFAULT nextval('"tipoNotificacion_idTipoNotificacion_seq"'::regclass);


--
-- Name: idTipoOpinion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoOpinion" ALTER COLUMN "idTipoOpinion" SET DEFAULT nextval('"tipoOpinion_idTipoOpinion_seq"'::regclass);


--
-- Name: idTipoPersona; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoPersona" ALTER COLUMN "idTipoPersona" SET DEFAULT nextval('"tipoPersona_idTipoPersona_seq"'::regclass);


--
-- Name: idTipoPublico; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoPublico" ALTER COLUMN "idTipoPublico" SET DEFAULT nextval('"tipoPublico_idTipoPublico_seq"'::regclass);


--
-- Name: idTipoRechazo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoRechazo" ALTER COLUMN "idTipoRechazo" SET DEFAULT nextval('"tipoRechazo_idTipoRechazo_seq"'::regclass);


--
-- Name: idTipoSancion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoSancion" ALTER COLUMN "idTipoSancion" SET DEFAULT nextval('"tipoSancion_idTipoSancion_seq"'::regclass);


--
-- Name: idTipoUnidadMedida; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoUnidadMedida" ALTER COLUMN "idTipoUnidadMedida" SET DEFAULT nextval('"tipoUnidadMedida_idTipoUnidadMedida_seq"'::regclass);


--
-- Name: idUnidadMedida; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "unidadMedida" ALTER COLUMN "idUnidadMedida" SET DEFAULT nextval('"unidadMedida_idUnidadMedida_seq"'::regclass);


--
-- Name: idUsuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN "idUsuario" SET DEFAULT nextval('"usuario_idUsuario_seq"'::regclass);


--
-- Name: idVisita; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visita ALTER COLUMN "idVisita" SET DEFAULT nextval('"visita_idVisita_seq"'::regclass);


--
-- Name: accion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accion
    ADD CONSTRAINT accion_pkey PRIMARY KEY ("idAccion");


--
-- Name: acompanante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY acompanante
    ADD CONSTRAINT acompanante_pkey PRIMARY KEY ("idAcompanante");


--
-- Name: actividad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY actividad
    ADD CONSTRAINT actividad_pkey PRIMARY KEY ("idActividad");


--
-- Name: apelacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY apelacion
    ADD CONSTRAINT apelacion_pkey PRIMARY KEY ("idApelacion");


--
-- Name: area_Recurso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "area_Recurso"
    ADD CONSTRAINT "area_Recurso_pkey" PRIMARY KEY ("idArea_recurso");


--
-- Name: area_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY area
    ADD CONSTRAINT area_pkey PRIMARY KEY ("idArea");


--
-- Name: arrendamiento_Indicador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "arrendamiento_Indicador"
    ADD CONSTRAINT "arrendamiento_Indicador_pkey" PRIMARY KEY ("idArrendamiento_indi");


--
-- Name: arrendamiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY arrendamiento
    ADD CONSTRAINT arrendamiento_pkey PRIMARY KEY ("idArrendamiento");


--
-- Name: asistencia_Evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "asistencia_Evento"
    ADD CONSTRAINT "asistencia_Evento_pkey" PRIMARY KEY ("idAsistencia_Evento");


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: calendario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY calendario
    ADD CONSTRAINT calendario_pkey PRIMARY KEY ("idCalendario");


--
-- Name: cargo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cargo
    ADD CONSTRAINT cargo_pkey PRIMARY KEY ("idCargo");


--
-- Name: categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pkey PRIMARY KEY ("idCategoria");


--
-- Name: club_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY club
    ADD CONSTRAINT club_pkey PRIMARY KEY ("idClub");


--
-- Name: comision_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY comision
    ADD CONSTRAINT comision_pkey PRIMARY KEY ("idComision");


--
-- Name: corsheaders_corsmodel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY corsheaders_corsmodel
    ADD CONSTRAINT corsheaders_corsmodel_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_45f3b1d93ec8c61c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_45f3b1d93ec8c61c_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: documento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY documento
    ADD CONSTRAINT documento_pkey PRIMARY KEY ("idDocumento");


--
-- Name: encuesta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY encuesta
    ADD CONSTRAINT encuesta_pkey PRIMARY KEY ("idEncuesta");


--
-- Name: evento_Comision_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "evento_Comision"
    ADD CONSTRAINT "evento_Comision_pkey" PRIMARY KEY ("idEvento_comision");


--
-- Name: evento_Indicador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "evento_Indicador"
    ADD CONSTRAINT "evento_Indicador_pkey" PRIMARY KEY ("idEvento_indicador");


--
-- Name: evento_Recurso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "evento_Recurso"
    ADD CONSTRAINT "evento_Recurso_pkey" PRIMARY KEY ("idEvento_recurso");


--
-- Name: evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT evento_pkey PRIMARY KEY ("idEvento");


--
-- Name: funcion_Grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "funcion_Grupo"
    ADD CONSTRAINT "funcion_Grupo_pkey" PRIMARY KEY (id);


--
-- Name: funcion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY funcion
    ADD CONSTRAINT funcion_pkey PRIMARY KEY (id);


--
-- Name: galeria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY galeria
    ADD CONSTRAINT galeria_pkey PRIMARY KEY ("idGaleria");


--
-- Name: genero_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY genero
    ADD CONSTRAINT genero_pkey PRIMARY KEY ("idGenero");


--
-- Name: grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY grupo
    ADD CONSTRAINT grupo_pkey PRIMARY KEY ("idGrupo");


--
-- Name: incidencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY incidencia
    ADD CONSTRAINT incidencia_pkey PRIMARY KEY ("idIncidencia");


--
-- Name: indicador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY indicador
    ADD CONSTRAINT indicador_pkey PRIMARY KEY ("idIndicador");


--
-- Name: morosidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY morosidad
    ADD CONSTRAINT morosidad_pkey PRIMARY KEY ("idMorosidad");


--
-- Name: noticia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY noticia
    ADD CONSTRAINT noticia_pkey PRIMARY KEY ("idNoticia");


--
-- Name: notificacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY notificacion
    ADD CONSTRAINT notificacion_pkey PRIMARY KEY ("idNotificacion");


--
-- Name: opcionPregunta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "opcionPregunta"
    ADD CONSTRAINT "opcionPregunta_pkey" PRIMARY KEY ("idOpcionPregunta");


--
-- Name: opinion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY opinion
    ADD CONSTRAINT opinion_pkey PRIMARY KEY ("idOpinion");


--
-- Name: parentesco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY parentesco
    ADD CONSTRAINT parentesco_pkey PRIMARY KEY ("idParentesco");


--
-- Name: persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT persona_pkey PRIMARY KEY ("idPersona");


--
-- Name: persona_preferencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY persona_preferencia
    ADD CONSTRAINT persona_preferencia_pkey PRIMARY KEY ("idPersona_pref");


--
-- Name: postulacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY postulacion
    ADD CONSTRAINT postulacion_pkey PRIMARY KEY ("idPostulacion");


--
-- Name: preferencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY preferencia
    ADD CONSTRAINT preferencia_pkey PRIMARY KEY ("idPreferencia");


--
-- Name: pregunta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pregunta
    ADD CONSTRAINT pregunta_pkey PRIMARY KEY ("idPregunta");


--
-- Name: rechazo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rechazo
    ADD CONSTRAINT rechazo_pkey PRIMARY KEY ("idRechazo");


--
-- Name: recurso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY recurso
    ADD CONSTRAINT recurso_pkey PRIMARY KEY ("idRecurso");


--
-- Name: referencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY referencia
    ADD CONSTRAINT referencia_pkey PRIMARY KEY ("idReferencia");


--
-- Name: respuestaEncuesta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "respuestaEncuesta"
    ADD CONSTRAINT "respuestaEncuesta_pkey" PRIMARY KEY ("idRespuestaEncuesta");


--
-- Name: resultado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY resultado
    ADD CONSTRAINT resultado_pkey PRIMARY KEY ("idResultado");


--
-- Name: sancion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sancion
    ADD CONSTRAINT sancion_pkey PRIMARY KEY ("idSancion");


--
-- Name: tipoArea_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoArea"
    ADD CONSTRAINT "tipoArea_pkey" PRIMARY KEY ("idTipoArea");


--
-- Name: tipoEvento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoEvento"
    ADD CONSTRAINT "tipoEvento_pkey" PRIMARY KEY ("idTipoEvento");


--
-- Name: tipoIncidencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoIncidencia"
    ADD CONSTRAINT "tipoIncidencia_pkey" PRIMARY KEY ("idTipoIncid");


--
-- Name: tipoIndicador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoIndicador"
    ADD CONSTRAINT "tipoIndicador_pkey" PRIMARY KEY ("idTipoindicador");


--
-- Name: tipoNoticia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoNoticia"
    ADD CONSTRAINT "tipoNoticia_pkey" PRIMARY KEY ("idTipoNoticia");


--
-- Name: tipoNotificacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoNotificacion"
    ADD CONSTRAINT "tipoNotificacion_pkey" PRIMARY KEY ("idTipoNotificacion");


--
-- Name: tipoOpinion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoOpinion"
    ADD CONSTRAINT "tipoOpinion_pkey" PRIMARY KEY ("idTipoOpinion");


--
-- Name: tipoPersona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoPersona"
    ADD CONSTRAINT "tipoPersona_pkey" PRIMARY KEY ("idTipoPersona");


--
-- Name: tipoPublico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoPublico"
    ADD CONSTRAINT "tipoPublico_pkey" PRIMARY KEY ("idTipoPublico");


--
-- Name: tipoRechazo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoRechazo"
    ADD CONSTRAINT "tipoRechazo_pkey" PRIMARY KEY ("idTipoRechazo");


--
-- Name: tipoSancion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoSancion"
    ADD CONSTRAINT "tipoSancion_pkey" PRIMARY KEY ("idTipoSancion");


--
-- Name: tipoUnidadMedida_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoUnidadMedida"
    ADD CONSTRAINT "tipoUnidadMedida_pkey" PRIMARY KEY ("idTipoUnidadMedida");


--
-- Name: unidadMedida_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "unidadMedida"
    ADD CONSTRAINT "unidadMedida_pkey" PRIMARY KEY ("idUnidadMedida");


--
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY ("idUsuario");


--
-- Name: visita_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT visita_pkey PRIMARY KEY ("idVisita");


--
-- Name: accion_759e719e; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX accion_759e719e ON accion USING btree ("idPostulacion");


--
-- Name: accion_806825b0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX accion_806825b0 ON accion USING btree ("idClub");


--
-- Name: apelacion_67646d8f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX apelacion_67646d8f ON apelacion USING btree ("idSancion");


--
-- Name: area_806825b0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX area_806825b0 ON area USING btree ("idClub");


--
-- Name: area_Recurso_296b207b; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "area_Recurso_296b207b" ON "area_Recurso" USING btree ("idRecurso");


--
-- Name: area_Recurso_88e3c0d7; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "area_Recurso_88e3c0d7" ON "area_Recurso" USING btree ("idArea");


--
-- Name: area_c9666bed; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX area_c9666bed ON area USING btree ("idTipoArea");


--
-- Name: arrendamiento_88e3c0d7; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX arrendamiento_88e3c0d7 ON arrendamiento USING btree ("idArea");


--
-- Name: arrendamiento_9a82e1d5; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX arrendamiento_9a82e1d5 ON arrendamiento USING btree ("idCalendario");


--
-- Name: arrendamiento_Indicador_3c086f6c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "arrendamiento_Indicador_3c086f6c" ON "arrendamiento_Indicador" USING btree ("idArrendamiento");


--
-- Name: arrendamiento_Indicador_c78f0797; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "arrendamiento_Indicador_c78f0797" ON "arrendamiento_Indicador" USING btree ("idIndicador");


--
-- Name: arrendamiento_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX arrendamiento_f0681536 ON arrendamiento USING btree ("idPersona");


--
-- Name: asistencia_Evento_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "asistencia_Evento_1ed26592" ON "asistencia_Evento" USING btree ("idEvento");


--
-- Name: asistencia_Evento_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "asistencia_Evento_f0681536" ON "asistencia_Evento" USING btree ("idPersona");


--
-- Name: auth_group_name_253ae2a6331666e8_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_name_253ae2a6331666e8_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_51b3b110094b8aae_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_username_51b3b110094b8aae_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: club_10874abf; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX club_10874abf ON club USING btree ("idGaleria");


--
-- Name: club_cf5965c5; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX club_cf5965c5 ON club USING btree ("idDocumento");


--
-- Name: comision_ac0770eb; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX comision_ac0770eb ON comision USING btree ("idActividad");


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_461cfeaa630ca218_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_session_key_461cfeaa630ca218_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: evento_248d39f2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX evento_248d39f2 ON evento USING btree ("idTipoPublico");


--
-- Name: evento_2981c086; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX evento_2981c086 ON evento USING btree ("idTipoEvento");


--
-- Name: evento_9a82e1d5; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX evento_9a82e1d5 ON evento USING btree ("idCalendario");


--
-- Name: evento_Comision_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "evento_Comision_1ed26592" ON "evento_Comision" USING btree ("idEvento");


--
-- Name: evento_Comision_e66dab75; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "evento_Comision_e66dab75" ON "evento_Comision" USING btree ("idComision");


--
-- Name: evento_Indicador_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "evento_Indicador_1ed26592" ON "evento_Indicador" USING btree ("idEvento");


--
-- Name: evento_Indicador_c78f0797; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "evento_Indicador_c78f0797" ON "evento_Indicador" USING btree ("idIndicador");


--
-- Name: evento_Recurso_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "evento_Recurso_1ed26592" ON "evento_Recurso" USING btree ("idEvento");


--
-- Name: evento_Recurso_296b207b; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "evento_Recurso_296b207b" ON "evento_Recurso" USING btree ("idRecurso");


--
-- Name: evento_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX evento_f0681536 ON evento USING btree ("idPersona");


--
-- Name: funcion_Grupo_a3c64682; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "funcion_Grupo_a3c64682" ON "funcion_Grupo" USING btree ("idFuncion");


--
-- Name: funcion_Grupo_d5f7a198; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "funcion_Grupo_d5f7a198" ON "funcion_Grupo" USING btree ("idGrupo");


--
-- Name: incidencia_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX incidencia_1ed26592 ON incidencia USING btree ("idEvento");


--
-- Name: incidencia_3c086f6c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX incidencia_3c086f6c ON incidencia USING btree ("idArrendamiento");


--
-- Name: incidencia_7d823e3c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX incidencia_7d823e3c ON incidencia USING btree ("idTipoIncid");


--
-- Name: incidencia_bfde36d4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX incidencia_bfde36d4 ON incidencia USING btree ("idVisita");


--
-- Name: incidencia_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX incidencia_f0681536 ON incidencia USING btree ("idPersona");


--
-- Name: indicador_09dd2faf; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX indicador_09dd2faf ON indicador USING btree ("idTipoindicador");


--
-- Name: indicador_81933ff2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX indicador_81933ff2 ON indicador USING btree ("idUnidadMedida");


--
-- Name: morosidad_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX morosidad_f0681536 ON morosidad USING btree ("idPersona");


--
-- Name: noticia_806825b0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX noticia_806825b0 ON noticia USING btree ("idClub");


--
-- Name: noticia_ae86fe01; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX noticia_ae86fe01 ON noticia USING btree ("idTipoNoticia");


--
-- Name: notificacion_99da90db; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX notificacion_99da90db ON notificacion USING btree ("idTipoNotificacion");


--
-- Name: notificacion_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX notificacion_f0681536 ON notificacion USING btree ("idPersona");


--
-- Name: opinion_3cfd71b3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX opinion_3cfd71b3 ON opinion USING btree ("idUsuario");


--
-- Name: opinion_826503b5; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX opinion_826503b5 ON opinion USING btree ("idTipoOpinion");


--
-- Name: persona_01ec234f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX persona_01ec234f ON persona USING btree ("idCargo");


--
-- Name: persona_975dec5d; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX persona_975dec5d ON persona USING btree ("idParentesco");


--
-- Name: persona_c4ace542; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX persona_c4ace542 ON persona USING btree ("idTipoPersona");


--
-- Name: persona_preferencia_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX persona_preferencia_f0681536 ON persona_preferencia USING btree ("idPersona");


--
-- Name: persona_preferencia_f60cac55; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX persona_preferencia_f60cac55 ON persona_preferencia USING btree ("idPreferencia");


--
-- Name: postulacion_4cc153c1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX postulacion_4cc153c1 ON postulacion USING btree ("idReferencia");


--
-- Name: postulacion_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX postulacion_f0681536 ON postulacion USING btree ("idPersona");


--
-- Name: preferencia_5ea0e3e4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX preferencia_5ea0e3e4 ON preferencia USING btree ("idCategoria");


--
-- Name: pregunta_5ea0e3e4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX pregunta_5ea0e3e4 ON pregunta USING btree ("idCategoria");


--
-- Name: pregunta_bf5d0416; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX pregunta_bf5d0416 ON pregunta USING btree ("idOpcionPregunta");


--
-- Name: pregunta_f4db176b; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX pregunta_f4db176b ON pregunta USING btree ("idEncuesta");


--
-- Name: rechazo_18a8e5bd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rechazo_18a8e5bd ON rechazo USING btree ("idApelacion");


--
-- Name: rechazo_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rechazo_1ed26592 ON rechazo USING btree ("idEvento");


--
-- Name: rechazo_3c086f6c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rechazo_3c086f6c ON rechazo USING btree ("idArrendamiento");


--
-- Name: rechazo_759e719e; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rechazo_759e719e ON rechazo USING btree ("idPostulacion");


--
-- Name: rechazo_c74696e7; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rechazo_c74696e7 ON rechazo USING btree ("idTipoRechazo");


--
-- Name: respuestaEncuesta_3a6bebd1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "respuestaEncuesta_3a6bebd1" ON "respuestaEncuesta" USING btree ("idPregunta");


--
-- Name: respuestaEncuesta_3cfd71b3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "respuestaEncuesta_3cfd71b3" ON "respuestaEncuesta" USING btree ("idUsuario");


--
-- Name: resultado_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX resultado_1ed26592 ON resultado USING btree ("idEvento");


--
-- Name: resultado_3c086f6c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX resultado_3c086f6c ON resultado USING btree ("idArrendamiento");


--
-- Name: sancion_07dcffe6; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sancion_07dcffe6 ON sancion USING btree ("idTipoSancion");


--
-- Name: sancion_f195edd1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sancion_f195edd1 ON sancion USING btree ("idIncidencia");


--
-- Name: usuario_d5f7a198; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX usuario_d5f7a198 ON usuario USING btree ("idGrupo");


--
-- Name: usuario_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX usuario_f0681536 ON usuario USING btree ("idPersona");


--
-- Name: visita_4dd07f93; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visita_4dd07f93 ON visita USING btree ("idAcompanante");


--
-- Name: visita_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visita_f0681536 ON visita USING btree ("idPersona");


--
-- Name: D0d1f8f111512d8743be8d8edcbca4e3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "arrendamiento_Indicador"
    ADD CONSTRAINT "D0d1f8f111512d8743be8d8edcbca4e3" FOREIGN KEY ("idArrendamiento") REFERENCES arrendamiento("idArrendamiento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D2b0de6621b39c21b011cfdc500100db; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rechazo
    ADD CONSTRAINT "D2b0de6621b39c21b011cfdc500100db" FOREIGN KEY ("idArrendamiento") REFERENCES arrendamiento("idArrendamiento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D7b347cace2014a372309c1a24a904e6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pregunta
    ADD CONSTRAINT "D7b347cace2014a372309c1a24a904e6" FOREIGN KEY ("idOpcionPregunta") REFERENCES "opcionPregunta"("idOpcionPregunta") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D7e9b9bbbee9d4ce52fb929e70096128; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notificacion
    ADD CONSTRAINT "D7e9b9bbbee9d4ce52fb929e70096128" FOREIGN KEY ("idTipoNotificacion") REFERENCES "tipoNotificacion"("idTipoNotificacion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D8cc468f7c0c85289ae67c33968228ea; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicador
    ADD CONSTRAINT "D8cc468f7c0c85289ae67c33968228ea" FOREIGN KEY ("idTipoindicador") REFERENCES "tipoIndicador"("idTipoindicador") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: acc_idPostulacion_211209bff2d1beef_fk_postulacion_idPostulacion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accion
    ADD CONSTRAINT "acc_idPostulacion_211209bff2d1beef_fk_postulacion_idPostulacion" FOREIGN KEY ("idPostulacion") REFERENCES postulacion("idPostulacion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: accion_idClub_338f68c8c7cdf8a7_fk_club_idClub; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accion
    ADD CONSTRAINT "accion_idClub_338f68c8c7cdf8a7_fk_club_idClub" FOREIGN KEY ("idClub") REFERENCES club("idClub") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: apelacion_idSancion_667245386b53f651_fk_sancion_idSancion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY apelacion
    ADD CONSTRAINT "apelacion_idSancion_667245386b53f651_fk_sancion_idSancion" FOREIGN KEY ("idSancion") REFERENCES sancion("idSancion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: area_Recurso_idArea_25233498f799f022_fk_area_idArea; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "area_Recurso"
    ADD CONSTRAINT "area_Recurso_idArea_25233498f799f022_fk_area_idArea" FOREIGN KEY ("idArea") REFERENCES area("idArea") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: area_Recurso_idRecurso_23d7576655c2b3a9_fk_recurso_idRecurso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "area_Recurso"
    ADD CONSTRAINT "area_Recurso_idRecurso_23d7576655c2b3a9_fk_recurso_idRecurso" FOREIGN KEY ("idRecurso") REFERENCES recurso("idRecurso") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: area_idClub_7e8a13e9d26d8c39_fk_club_idClub; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area
    ADD CONSTRAINT "area_idClub_7e8a13e9d26d8c39_fk_club_idClub" FOREIGN KEY ("idClub") REFERENCES club("idClub") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: area_idTipoArea_5e61371363e62720_fk_tipoArea_idTipoArea; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area
    ADD CONSTRAINT "area_idTipoArea_5e61371363e62720_fk_tipoArea_idTipoArea" FOREIGN KEY ("idTipoArea") REFERENCES "tipoArea"("idTipoArea") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: arrend_idCalendario_1e1f82ea86e3fbcc_fk_calendario_idCalendario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY arrendamiento
    ADD CONSTRAINT "arrend_idCalendario_1e1f82ea86e3fbcc_fk_calendario_idCalendario" FOREIGN KEY ("idCalendario") REFERENCES calendario("idCalendario") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: arrendami_idIndicador_36eb0210b913f910_fk_indicador_idIndicador; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "arrendamiento_Indicador"
    ADD CONSTRAINT "arrendami_idIndicador_36eb0210b913f910_fk_indicador_idIndicador" FOREIGN KEY ("idIndicador") REFERENCES indicador("idIndicador") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: arrendamiento_idArea_6b49d5d74d4d5961_fk_area_idArea; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY arrendamiento
    ADD CONSTRAINT "arrendamiento_idArea_6b49d5d74d4d5961_fk_area_idArea" FOREIGN KEY ("idArea") REFERENCES area("idArea") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: arrendamiento_idPersona_d0f08821b0d07d7_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY arrendamiento
    ADD CONSTRAINT "arrendamiento_idPersona_d0f08821b0d07d7_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: asistencia_Even_idPersona_1c627fcd1ef82938_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "asistencia_Evento"
    ADD CONSTRAINT "asistencia_Even_idPersona_1c627fcd1ef82938_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: asistencia_Evento_idEvento_400d50869991f38_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "asistencia_Evento"
    ADD CONSTRAINT "asistencia_Evento_idEvento_400d50869991f38_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_content_type_id_508cf46651277a81_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_content_type_id_508cf46651277a81_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: club_idDocumento_17af4cba13cc7873_fk_documento_idDocumento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY club
    ADD CONSTRAINT "club_idDocumento_17af4cba13cc7873_fk_documento_idDocumento" FOREIGN KEY ("idDocumento") REFERENCES documento("idDocumento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: club_idGaleria_1a611064fbec634e_fk_galeria_idGaleria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY club
    ADD CONSTRAINT "club_idGaleria_1a611064fbec634e_fk_galeria_idGaleria" FOREIGN KEY ("idGaleria") REFERENCES galeria("idGaleria") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comision_idActividad_25c5d5e05d19d699_fk_actividad_idActividad; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comision
    ADD CONSTRAINT "comision_idActividad_25c5d5e05d19d699_fk_actividad_idActividad" FOREIGN KEY ("idActividad") REFERENCES actividad("idActividad") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djan_content_type_id_697914295151027a_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT djan_content_type_id_697914295151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: eve_idTipoPublico_3e5ce99c0f61198a_fk_tipoPublico_idTipoPublico; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT "eve_idTipoPublico_3e5ce99c0f61198a_fk_tipoPublico_idTipoPublico" FOREIGN KEY ("idTipoPublico") REFERENCES "tipoPublico"("idTipoPublico") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_Comisi_idComision_142d26a497b9323_fk_comision_idComision; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "evento_Comision"
    ADD CONSTRAINT "evento_Comisi_idComision_142d26a497b9323_fk_comision_idComision" FOREIGN KEY ("idComision") REFERENCES comision("idComision") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_Comision_idEvento_7a032c67b4b25141_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "evento_Comision"
    ADD CONSTRAINT "evento_Comision_idEvento_7a032c67b4b25141_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_In_idIndicador_215e444647239fed_fk_indicador_idIndicador; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "evento_Indicador"
    ADD CONSTRAINT "evento_In_idIndicador_215e444647239fed_fk_indicador_idIndicador" FOREIGN KEY ("idIndicador") REFERENCES indicador("idIndicador") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_Indicador_idEvento_7c8059ffbd6e39e2_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "evento_Indicador"
    ADD CONSTRAINT "evento_Indicador_idEvento_7c8059ffbd6e39e2_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_Recurso_idEvento_146f592be8a62dce_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "evento_Recurso"
    ADD CONSTRAINT "evento_Recurso_idEvento_146f592be8a62dce_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_Recurso_idRecurso_626f17866b5f349b_fk_recurso_idRecurso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "evento_Recurso"
    ADD CONSTRAINT "evento_Recurso_idRecurso_626f17866b5f349b_fk_recurso_idRecurso" FOREIGN KEY ("idRecurso") REFERENCES recurso("idRecurso") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_idCalendario_30f7eb70c601fe25_fk_calendario_idCalendario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT "evento_idCalendario_30f7eb70c601fe25_fk_calendario_idCalendario" FOREIGN KEY ("idCalendario") REFERENCES calendario("idCalendario") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_idPersona_672838d45cb6d5ea_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT "evento_idPersona_672838d45cb6d5ea_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evento_idTipoEvento_7ea9897695f9dfb4_fk_tipoEvento_idTipoEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT "evento_idTipoEvento_7ea9897695f9dfb4_fk_tipoEvento_idTipoEvento" FOREIGN KEY ("idTipoEvento") REFERENCES "tipoEvento"("idTipoEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: f28d575cee6498efc99262b6b00c26a3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY incidencia
    ADD CONSTRAINT f28d575cee6498efc99262b6b00c26a3 FOREIGN KEY ("idArrendamiento") REFERENCES arrendamiento("idArrendamiento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: f83171e428cde8b8c8122bcfa9706716; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultado
    ADD CONSTRAINT f83171e428cde8b8c8122bcfa9706716 FOREIGN KEY ("idArrendamiento") REFERENCES arrendamiento("idArrendamiento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: funcion_Grupo_idFuncion_248a362d73768668_fk_funcion_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "funcion_Grupo"
    ADD CONSTRAINT "funcion_Grupo_idFuncion_248a362d73768668_fk_funcion_id" FOREIGN KEY ("idFuncion") REFERENCES funcion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: funcion_Grupo_idGrupo_3e21335c8211e7a5_fk_grupo_idGrupo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "funcion_Grupo"
    ADD CONSTRAINT "funcion_Grupo_idGrupo_3e21335c8211e7a5_fk_grupo_idGrupo" FOREIGN KEY ("idGrupo") REFERENCES grupo("idGrupo") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: idUnidadMedida_5a929e86b8f3bd45_fk_unidadMedida_idUnidadMedida; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicador
    ADD CONSTRAINT "idUnidadMedida_5a929e86b8f3bd45_fk_unidadMedida_idUnidadMedida" FOREIGN KEY ("idUnidadMedida") REFERENCES "unidadMedida"("idUnidadMedida") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: inci_idTipoIncid_5093fcd5201b76ed_fk_tipoIncidencia_idTipoIncid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY incidencia
    ADD CONSTRAINT "inci_idTipoIncid_5093fcd5201b76ed_fk_tipoIncidencia_idTipoIncid" FOREIGN KEY ("idTipoIncid") REFERENCES "tipoIncidencia"("idTipoIncid") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: incidencia_idEvento_4819872e4ed09386_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY incidencia
    ADD CONSTRAINT "incidencia_idEvento_4819872e4ed09386_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: incidencia_idPersona_6e97f13e3306776_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY incidencia
    ADD CONSTRAINT "incidencia_idPersona_6e97f13e3306776_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: incidencia_idVisita_17d9cc040294a4f7_fk_visita_idVisita; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY incidencia
    ADD CONSTRAINT "incidencia_idVisita_17d9cc040294a4f7_fk_visita_idVisita" FOREIGN KEY ("idVisita") REFERENCES visita("idVisita") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: morosidad_idPersona_310bd577d74a613e_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY morosidad
    ADD CONSTRAINT "morosidad_idPersona_310bd577d74a613e_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: not_idTipoNoticia_5f3aae226fa62a74_fk_tipoNoticia_idTipoNoticia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noticia
    ADD CONSTRAINT "not_idTipoNoticia_5f3aae226fa62a74_fk_tipoNoticia_idTipoNoticia" FOREIGN KEY ("idTipoNoticia") REFERENCES "tipoNoticia"("idTipoNoticia") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: noticia_idClub_5f632c806483bdc2_fk_club_idClub; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noticia
    ADD CONSTRAINT "noticia_idClub_5f632c806483bdc2_fk_club_idClub" FOREIGN KEY ("idClub") REFERENCES club("idClub") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: notificacion_idPersona_1ad6c51ebe68d3d3_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notificacion
    ADD CONSTRAINT "notificacion_idPersona_1ad6c51ebe68d3d3_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: opi_idTipoOpinion_4c56eff427ed4192_fk_tipoOpinion_idTipoOpinion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY opinion
    ADD CONSTRAINT "opi_idTipoOpinion_4c56eff427ed4192_fk_tipoOpinion_idTipoOpinion" FOREIGN KEY ("idTipoOpinion") REFERENCES "tipoOpinion"("idTipoOpinion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: opinion_idUsuario_ecf8b73bd01efea_fk_usuario_idUsuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY opinion
    ADD CONSTRAINT "opinion_idUsuario_ecf8b73bd01efea_fk_usuario_idUsuario" FOREIGN KEY ("idUsuario") REFERENCES usuario("idUsuario") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: per_idPreferencia_3d37c0ce2738a919_fk_preferencia_idPreferencia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona_preferencia
    ADD CONSTRAINT "per_idPreferencia_3d37c0ce2738a919_fk_preferencia_idPreferencia" FOREIGN KEY ("idPreferencia") REFERENCES preferencia("idPreferencia") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: per_idTipoPersona_631c7fb05c2ab3ba_fk_tipoPersona_idTipoPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT "per_idTipoPersona_631c7fb05c2ab3ba_fk_tipoPersona_idTipoPersona" FOREIGN KEY ("idTipoPersona") REFERENCES "tipoPersona"("idTipoPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: person_idParentesco_27c3a28dd7a89667_fk_parentesco_idParentesco; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT "person_idParentesco_27c3a28dd7a89667_fk_parentesco_idParentesco" FOREIGN KEY ("idParentesco") REFERENCES parentesco("idParentesco") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: persona_idCargo_23f16a104c0d239e_fk_cargo_idCargo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT "persona_idCargo_23f16a104c0d239e_fk_cargo_idCargo" FOREIGN KEY ("idCargo") REFERENCES cargo("idCargo") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: persona_prefere_idPersona_12b3c6c43741f673_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona_preferencia
    ADD CONSTRAINT "persona_prefere_idPersona_12b3c6c43741f673_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: postul_idReferencia_5b02b5e89134b734_fk_referencia_idReferencia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY postulacion
    ADD CONSTRAINT "postul_idReferencia_5b02b5e89134b734_fk_referencia_idReferencia" FOREIGN KEY ("idReferencia") REFERENCES referencia("idReferencia") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: postulacion_idPersona_7dd2c56b0289aed1_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY postulacion
    ADD CONSTRAINT "postulacion_idPersona_7dd2c56b0289aed1_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: preferenc_idCategoria_49898af412ee6165_fk_categoria_idCategoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY preferencia
    ADD CONSTRAINT "preferenc_idCategoria_49898af412ee6165_fk_categoria_idCategoria" FOREIGN KEY ("idCategoria") REFERENCES categoria("idCategoria") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pregunta_idCategoria_7cf896c4a859c7e8_fk_categoria_idCategoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pregunta
    ADD CONSTRAINT "pregunta_idCategoria_7cf896c4a859c7e8_fk_categoria_idCategoria" FOREIGN KEY ("idCategoria") REFERENCES categoria("idCategoria") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pregunta_idEncuesta_4c717c21683fdd66_fk_encuesta_idEncuesta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pregunta
    ADD CONSTRAINT "pregunta_idEncuesta_4c717c21683fdd66_fk_encuesta_idEncuesta" FOREIGN KEY ("idEncuesta") REFERENCES encuesta("idEncuesta") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rec_idPostulacion_635088921ec28de5_fk_postulacion_idPostulacion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rechazo
    ADD CONSTRAINT "rec_idPostulacion_635088921ec28de5_fk_postulacion_idPostulacion" FOREIGN KEY ("idPostulacion") REFERENCES postulacion("idPostulacion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rec_idTipoRechazo_235dc57ee6f78f52_fk_tipoRechazo_idTipoRechazo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rechazo
    ADD CONSTRAINT "rec_idTipoRechazo_235dc57ee6f78f52_fk_tipoRechazo_idTipoRechazo" FOREIGN KEY ("idTipoRechazo") REFERENCES "tipoRechazo"("idTipoRechazo") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rechazo_idApelacion_3ba192eb965ed1fa_fk_apelacion_idApelacion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rechazo
    ADD CONSTRAINT "rechazo_idApelacion_3ba192eb965ed1fa_fk_apelacion_idApelacion" FOREIGN KEY ("idApelacion") REFERENCES apelacion("idApelacion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rechazo_idEvento_51fdef1ed318fac6_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rechazo
    ADD CONSTRAINT "rechazo_idEvento_51fdef1ed318fac6_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: respuestaEnc_idPregunta_5a5f3b723978812d_fk_pregunta_idPregunta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "respuestaEncuesta"
    ADD CONSTRAINT "respuestaEnc_idPregunta_5a5f3b723978812d_fk_pregunta_idPregunta" FOREIGN KEY ("idPregunta") REFERENCES pregunta("idPregunta") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: respuestaEncues_idUsuario_1fd52516650094cc_fk_usuario_idUsuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "respuestaEncuesta"
    ADD CONSTRAINT "respuestaEncues_idUsuario_1fd52516650094cc_fk_usuario_idUsuario" FOREIGN KEY ("idUsuario") REFERENCES usuario("idUsuario") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: resultado_idEvento_4b0a73c52661334f_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultado
    ADD CONSTRAINT "resultado_idEvento_4b0a73c52661334f_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: san_idTipoSancion_1e8b92967bad2588_fk_tipoSancion_idTipoSancion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sancion
    ADD CONSTRAINT "san_idTipoSancion_1e8b92967bad2588_fk_tipoSancion_idTipoSancion" FOREIGN KEY ("idTipoSancion") REFERENCES "tipoSancion"("idTipoSancion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sancio_idIncidencia_5b006785a4f891a7_fk_incidencia_idIncidencia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sancion
    ADD CONSTRAINT "sancio_idIncidencia_5b006785a4f891a7_fk_incidencia_idIncidencia" FOREIGN KEY ("idIncidencia") REFERENCES incidencia("idIncidencia") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuario_idGrupo_1723a06efb568e77_fk_grupo_idGrupo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT "usuario_idGrupo_1723a06efb568e77_fk_grupo_idGrupo" FOREIGN KEY ("idGrupo") REFERENCES grupo("idGrupo") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuario_idPersona_24d60ce8e14c02ec_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT "usuario_idPersona_24d60ce8e14c02ec_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: vis_idAcompanante_3e879786dd6367bc_fk_acompanante_idAcompanante; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT "vis_idAcompanante_3e879786dd6367bc_fk_acompanante_idAcompanante" FOREIGN KEY ("idAcompanante") REFERENCES acompanante("idAcompanante") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: visita_idPersona_67be1c087d4870e7_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT "visita_idPersona_67be1c087d4870e7_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

