Canario Web Services
===
##### Para instalar la version de Django

`pip install django==1.8`

##### Para consumir los servicios instalar el siguiente:

`pip install django-cors-headers`

##### Configurado para base de datos postgresql (en caso de error instalar python-dev: sudo apt-get install python-dev):

`pip install psycopg2`

##### Para la ruta de la imagen:

`pip install Pillow`

##### Para configurar la BD:

    nombre de la BD: 	canario
    usuario de postgresql:	postgres
    contrasena de postgresql:	postgres

##### Para sincronizar con BD ejecutar los siguientes comandos:
`
python manage.py syncdb
python manage.py makemigrations
python manage.py migrate`

Nota: al pedir crear superusuario

    user:   admin
    email:  admin@admin.com
    pass:   admin

