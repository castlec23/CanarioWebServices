# -*- coding: utf-8 -*-
from django.shortcuts import render,render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from WebServices.models import *
from django.core import serializers
from django.conf import settings
from django.core.mail import EmailMessage
import json, datetime
from pprint import pprint


def prueba(request):
	data = serializers.serialize("json", Persona.objects.all().order_by('pk'))
	return HttpResponse(data, content_type='application/json')

def email(request):
	email = EmailMessage('Asunto', 'Mensaje', to=['castlec23@gmail.com']) 
	email.send()
	data = ['email']
	return HttpResponse(data, content_type='application/json')

def contacto(request):
	nombre = str(request.GET.get("name"))
	correo = str(request.GET.get("email"))
	telefono = str(request.GET.get("phone"))
	mensaje = str(request.GET.get("mensaje"))
	msj = " El Sr(a): %s \n\n Informacion de Contacto \n\n Email: %s \n\n Telefono: %s \n\n Mensaje:\n %s \n" % (nombre,correo,telefono,mensaje)
	try:
		email = EmailMessage('Contacto de Portal', msj, to=['hogarclbarquisimeto@gmail.com']) 
		email.send()
		data = ['{"exito":true,"msj":"Gracias por su colaboracion"}']
	except Exception as e:
		data = ['{exito:false,msj:"Ha ocurrido un error inesperado, intentelo mas tarde"}']
	return HttpResponse(data, content_type='application/json')


def solicitarArrendamiento(request):
	if request.POST:
		pprint (request.POST)
		if request.POST.get("areas[]", False):
			areasa = request.POST.getlist('areas[]')
			per =  Persona.objects.filter(cedpersona=request.POST.get("cedula"))
			
			if per:
				per = per[0]
				print per.nombrepersona + " tipo " + per.tipopersonaid.nombretipopersona
			else:
				tipoper = Tipopersona.objects.get(idtipopersona=4)
				per = Persona(apellidopersona = request.POST.get("apellido"), cedpersona = request.POST.get("cedula"), correopersona = request.POST.get("correo"), editingstatus = False, estadopersona = 'A', nombrepersona = request.POST.get("nombre"), tlfcasapersona = request.POST.get("telefono-casa"), tlfcelpersona = request.POST.get("telefono-cel"), tipopersonaid = tipoper)
				per.save()

			tipoarr = Tipoarrendamiento.objects.get(idtipoarrend=request.POST.get("tipo-arrendamiento"))
			print tipoarr.descarrend
			fechaarr = datetime.datetime.strptime(request.POST.get("fecha-arrendamiento"), "%Y-%m-%d").date()
			print fechaarr
			horaarr = datetime.datetime.strptime(request.POST.get("hora-inicio"),"%H:%M").time()
			print horaarr

			arrendamiento = Arrendamiento(descarrend = request.POST.get("descripcion"), editingstatus = False, estadoarrend = 'P', fechaarrend = fechaarr, fechaemisionarrend = datetime.date.today(), horaarrend = horaarr, personaid = per, tipoarrendamientoid = tipoarr)
			arrendamiento.save()
			for s in areasa:
				print s
				elarea = Area.objects.get(idarea = s)
				print elarea.nombrearea
				areaArrend = AreaArrend(areaid=elarea, arrendamientoid = arrendamiento)
				areaArrend.save()
			return HttpResponseRedirect('http://localhost:8080/CanarioWeb/portal/Admin/estadoSolicitud.html')
	else:
		print 'PETICION GET'
		return HttpResponseRedirect('http://localhost:8080/CanarioWeb/portal/Admin/formArrendamiento.html')

def solicitarMembresia(request):
	if request.POST:
		pprint (request.POST)
		
		solicitante =  Persona.objects.filter(cedpersona=request.POST.get("cedula"))

		#¿COMO SE VA A VALIDAR AL SOLICITANTE (SI YA EXISTE PUEDE QUE SEA CLIENTE, o BENEFICIARIO)?
		if not solicitante:
			tipoper = Tipopersona.objects.get(idtipopersona=8)
			fechaNacimiento = datetime.datetime.strptime(request.POST.get("fecha-nacimiento"), "%Y-%m-%d").date()
			solicitante = Persona(apellidopersona = request.POST.get("apellido"), cedpersona = request.POST.get("cedula"), correopersona = request.POST.get("correo"), direcpersona = request.POST.get("direccion"), directrabpersona = request.POST.get("direccion-empresa"), estadocivilpersona = request.POST.get("estado-civil"), fechanacpersona = fechaNacimiento, lugarnacpersona = request.POST.get("lugar-nacimiento"), nombrepersona = request.POST.get("nombre"), empresatrabpersona = request.POST.get("nombre-empresa"), profesionpersona = request.POST.get("profesion"), sexopersona = request.POST.get("sexo"), sueldopersona = request.POST.get("sueldo"), tlfcasapersona = request.POST.get("telefono-casa"), tlfcelpersona = request.POST.get("telefono-cel"), tlftrabpersona = request.POST.get("telefono-empresa"), editingstatus = False, estadopersona = 'A', tipopersonaid = tipoper)
			

			#CREACION DE CONYUGUE
			if request.POST.get("cedula-cy",False):
				solicitanteCY = Persona.objects.filter(cedpersona=request.POST.get("cedula-cy"))
				if not solicitanteCY:
					existeCY = True
				else:
					existeCY = False
					
			else:
				existeCY = True

			if (existeCY and  (request.POST.get("cedula-cy") != request.POST.get("cedula"))):
				solicitante.save()
				if request.POST.get("cedula-cy").strip()!="":
					tipopercy = Tipopersona.objects.get(idtipopersona=5)
					parentescocy = Parentesco.objects.get(idparentesco=1)
					fechaNacimientoCY = datetime.datetime.strptime(request.POST.get("fecha-nacimiento-cy"), "%Y-%m-%d").date()
					solicitanteCY = Persona(apellidopersona = request.POST.get("apellido-cy"), cedpersona = request.POST.get("cedula-cy"), correopersona = request.POST.get("correo-cy"), estadocivilpersona = request.POST.get("estado-civil-cy"), fechanacpersona = fechaNacimientoCY, lugarnacpersona = request.POST.get("lugar-nacimiento-cy"), nombrepersona = request.POST.get("nombre-cy"), profesionpersona = request.POST.get("profesion-cy"), sexopersona = request.POST.get("sexo-cy"), sueldopersona = request.POST.get("sueldo-cy"), tlfcasapersona = request.POST.get("telefono-casa-cy"), tlfcelpersona = request.POST.get("telefono-cel-cy"), editingstatus = False, estadopersona = 'A', tipopersonaid = tipopercy, idpadrepersona = solicitante, parentescoid = parentescocy)
					solicitanteCY.save()

				if request.POST.get("preferenciasSeleccionadas[]", False):
					preferenciasSeleccionadas = request.POST.getlist('preferenciasSeleccionadas[]')
					for s in preferenciasSeleccionadas:
						#CREACION DE PREFERENCIAS
						pref = Preferencia.objects.get(idpreferencia=s)
						pprint (s + pref.nombrepreferencia)
						prefPersona = PreferenciaPersona(personaid=solicitante, preferenciaid=pref)
						prefPersona.save()

				postulacion = Postulacion(editingstatus = False, estadopostulacion = 'S', fechapostulacion = datetime.date.today(), personaid = solicitante)
				postulacion.save()

				#SE DEBE ASIGNAR LAS REFERENCIAS DE LA POSTULACION

				perref1= Persona.objects.get(idpersona=request.POST.get('ref-1'))
				perref2= Persona.objects.get(idpersona=request.POST.get('ref-2'))	
				ref1= Referencia(postulacionid = postulacion,personaid = perref1,estadoreferencia = 'A',editingstatus = False)
				ref1.save()
				ref2= Referencia(postulacionid = postulacion,personaid = perref2,estadoreferencia = 'A',editingstatus = False)
				ref2.save()

				return HttpResponseRedirect('http://localhost:8080/CanarioWeb/portal/CaraWeb/index.html')

			else:
				#ERROR EL CONYUGUE YA EXISTE
				datos = []
				data = {}
				data['exito']=False
				data['msj']='Ya existe el conyugue: ' + request.POST.get("cedula-cy") 
				datos.append(data)
				return HttpResponse(json.dumps(datos), content_type='application/json')

		else:
			#ERROR LA PERSONA YA EXISTE, ES DECIR YA ES MIEMBRO O ES CLIENTE O TIENE UNA POSTULACION PENDIENTE
			solicitante = solicitante[0]
			print solicitante.nombrepersona + " tipo " + solicitante.tipopersonaid.nombretipopersona
			datos = []
			data = {}
			data['exito']=False
			data['msj']='Ya existe este miembro: ' + solicitante.cedpersona
			datos.append(data)
			return HttpResponse(json.dumps(datos), content_type='application/json')

	else:
		print 'PETICION GET'
		return HttpResponseRedirect('http://localhost:8080/CanarioWeb/portal/Admin/formMembresia.html')

def validarExistente(solicitante):
	if solicitante.tipopersonaid.pk == 8:
		postulaciones =  Postulacion.objects.filter(personaid=solicitante.idpersona).order_by('-fechapostulacion')
		if str(postulaciones)!='[]':
			if postulaciones[0].estadopostulacion=='R':
				return True
		else:
			return True
	return False


def buscarCliente(request):
	datos = []
	data = {}
	print request.POST
	per =  Persona.objects.filter(cedpersona=request.POST.get("user"))
	if per:
		print per
		per=per[0]
		if per.estadopersona == "A":
			data['exito']=True
			data['msj']='Exito'
			data['id']=per.pk
			data['cedula']=per.cedpersona
			data['nombre']=per.nombrepersona
			data['apellido']=per.apellidopersona
			data['telCel']=per.tlfcelpersona
			data['telCasa']=per.tlfcasapersona
			data['correo']=per.correopersona
			datos.append(data)
			
		else:
			data['exito']=False
			data['msj']='El usuario no puede realizar un arrendamiento'
			datos.append(data)
	else:
		data['exito']=False
		data['msj']='No se encontro su usuario, por favor verifique e intente de nuevo'
		datos.append(data)
	return HttpResponse(json.dumps(datos), content_type='application/json')


def buscarReferencia(request):
	datos = []
	data = {}
	per =  Persona.objects.filter(cedpersona=request.POST.get("user"))
	if per:
		per=per[0]
		if per.tipopersonaid.pk == 1 and per.estadopersona == "A":
			data['exito']=True
			data['msj']='Exito'
			data['id']=per.pk
			data['cedula']=per.cedpersona
			data['nombre']=per.nombrepersona
			data['apellido']=per.apellidopersona
			data['telefono']=per.tlfcelpersona
			datos.append(data)
			
		else:
			data['exito']=False
			data['msj']='El usuario no puede ser referencia, por favor verifique e intente de nuevo'
			datos.append(data)
	else:
		data['exito']=False
		data['msj']='No se encontro su usuario, por favor verifique e intente de nuevo'
		datos.append(data)
	return HttpResponse(json.dumps(datos), content_type='application/json')


def eventos(request):
	datos = []
	for i in Evento.objects.filter(estadoevento='P'):
		data = {}
		data['title']= i.nombreevento
		data['start']= str(i.fechaevento)
		data['hora']= str(i.horaevento)
		data['description']= i.descevento
		data['tipo']= i.tipoeventoid.nombretipoevento
		datos.append(data)
	return HttpResponse(json.dumps(datos), content_type='application/json')

def adiponibles(request):
	datos = []
	for i in AreaArrend.objects.filter(arrendamientoid__estadoarrend='A'):
		data = {}
		data['title']= i.areaid.nombrearea
		data['start']= str(i.arrendamientoid.fechaarrend)
		data['color']= '#58c9f3 !important'
		if i.areaid.pk==1:
			data['color']= '#F0750C !important'
		if i.areaid.pk==2:
			data['color']= '#F73838 !important'
		if i.areaid.pk==3:
			data['color']= '#C738D7 !important'
		if i.areaid.pk==4:
			data['color']= '#3667E2 !important'
		if i.areaid.pk==5:
			data['color']= '#B600B0 !important'
		if i.areaid.pk==6:
			data['color']= '#E2DA36 !important'
		if i.areaid.pk==7:
			data['color']= '#0AC000 !important'
		if i.areaid.pk==8:
			data['color']= '#DB0000 !important'
		if i.areaid.pk==9:
			data['color']= '#36E253 !important'
		datos.append(data)
	for i in AreaEvento.objects.filter(eventoid__estadoevento='P'):
		data = {}
		data['title']= i.areaid.nombrearea
		data['start']= str(i.eventoid.fechaevento)
		data['color']= '#58c9f3 !important'
		if i.areaid.pk==1:
			data['color']= '#F0750C !important'
		if i.areaid.pk==2:
			data['color']= '#F73838 !important'
		if i.areaid.pk==3:
			data['color']= '#C738D7 !important'
		if i.areaid.pk==4:
			data['color']= '#3667E2 !important'
		if i.areaid.pk==5:
			data['color']= '#B600B0 !important'
		if i.areaid.pk==6:
			data['color']= '#E2DA36 !important'
		if i.areaid.pk==7:
			data['color']= '#0AC000 !important'
		if i.areaid.pk==8:
			data['color']= '#DB0000 !important'
		if i.areaid.pk==9:
			data['color']= '#36E253 !important'
		datos.append(data)
	return HttpResponse(json.dumps(datos), content_type='application/json')

def profesiones(request):
	data = serializers.serialize("json", Profesion.objects.all().order_by('nombreprofesion'))
	return HttpResponse(data, content_type='application/json')

def paises(request):
	data = serializers.serialize("json", Pais.objects.all().order_by('nombrepais'))
	return HttpResponse(data, content_type='application/json')

def estados(request):
	data = serializers.serialize("json", Estado.objects.all().order_by('nombreestado'))
	return HttpResponse(data, content_type='application/json')

def estadocivil(request):
	data = serializers.serialize("json", Estadocivil.objects.all().order_by('nombreestadocivil'))
	return HttpResponse(data, content_type='application/json')

def preferencias(request):
	data = serializers.serialize("json", Preferencia.objects.all().order_by('nombrepreferencia'))
	return HttpResponse(data, content_type='application/json')

def categorias(request):
	data = serializers.serialize("json", Categoria.objects.all().order_by('nombrecategoria'))
	return HttpResponse(data, content_type='application/json')

def areas(request):
	data = serializers.serialize("json", Area.objects.all().order_by('nombrearea'))
	return HttpResponse(data, content_type='application/json')

def aDisponiblesFecha(request):
	areasTodas =  Area.objects.order_by('nombrearea').filter(estadoarea='A')
	fecha = datetime.datetime.strptime(request.GET.get("fecha"), "%Y-%m-%d").date()

	areaArrendadas = AreaArrend.objects.filter(arrendamientoid__fechaarrend=fecha,arrendamientoid__estadoarrend='A')
	areaEventos = AreaEvento.objects.filter(eventoid__fechaevento=fecha,eventoid__estadoevento__in=['R','P','E'])
	arr = []
	for a in areasTodas:
		insert = True
		for aa in areaArrendadas:
			if aa.areaid == a:
				insert = False
		for ae in areaEventos:
			if ae.areaid == a:
				insert = False
		if insert:
			print a.nombrearea
			ar ={}
			ar['pk']=a.pk
			ar['nombre']=a.nombrearea
			arr.append(ar)

	return HttpResponse(json.dumps(arr), content_type='application/json')

def tiposArrendamiento(request):
	data = serializers.serialize("json", Tipoarrendamiento.objects.all().order_by('nombrearrend'))
	return HttpResponse(data, content_type='application/json')

def solicitud2(request):
	datos = []
	data = {}
	per =  Persona.objects.filter(cedpersona=request.GET.get("user"))
	if str(per)!='[]':
		data['exito']=True
		arrenda = Arrendamiento.objects.filter(personaid=per)
		if str(arrenda)!='[]':
			data['exito']=True
			data['arrendamientos']=serializers.serialize("json",arrenda)
			datos.append(data)
		else:
			data['exito']=False
			data['msj']='No se encontraron arrendamientos a su nombre'
			datos.append(data)
	else:
		data['exito']=False
		data['msj']='No se encontraron su usuario, por favor verifique e intente de nuevo'
		datos.append(data)
	return HttpResponse(json.dumps(datos), content_type='application/json')

def edosolicitud(request):
	datos = []
	data = {}
	per =  Persona.objects.filter(cedpersona=request.GET.get("user"))
	if str(per)!='[]':
		data['exito']=True
		arrenda = Arrendamiento.objects.filter(personaid=per).exclude(estadoarrend='I').exclude(estadoarrend='C')
		if str(arrenda)!='[]':
			arr =[]
			for a in arrenda:
				ar ={}
				ar['fecha']=str(a.fechaarrend)
				ar['hora']=str(a.horaarrend)
				are = AreaArrend.objects.filter(arrendamientoid__personaid=per).values("areaid")
				ar['area']= [v for v in Area.objects.filter(pk__in=are).values("nombrearea")]
				ar['tipoarren']=a.tipoarrendamientoid.nombrearrend
				ar['estado']='Pendiente'
				if (a.estadoarrend=='A'):
					ar['estado']='Aprobado'
				if (a.estadoarrend=='R'):
					ar['estado']='Rechazado'
				if (a.estadoarrend=='P'):
					ar['estado']='Pendiente'

				arr.append(ar)
			data['exito']=True
			data['arrendamientos']=arr
			datos.append(data)
		else:
			data['exito']=False
			data['msj']='No se encontraron arrendamientos a su nombre'
			datos.append(data)
	else:
		data['exito']=False
		data['msj']='No se encontro su usuario, por favor verifique e intente de nuevo'
		datos.append(data)
	return HttpResponse(json.dumps(datos), content_type='application/json')


def noticias(request):
	data = serializers.serialize("json", Noticia.objects.all().order_by('-pk').filter(estadonoticia='A',difusionnoticia='Publica',fechaexpnoticia__gte=datetime.date.today(),fechaininoticia__lte=datetime.date.today())[:10])
	return HttpResponse(data, content_type='application/json')

def galeria(request):
	data = serializers.serialize("json", Galeria.objects.all().order_by('nombregaleria').filter(estadogaleria='A'))
	return HttpResponse(data, content_type='application/json')