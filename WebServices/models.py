from _ast import mod
from django.db import models
# Create your models here.

class Accion(models.Model):
    idaccion = models.AutoField(primary_key=True)
    editingstatus = models.BooleanField()
    estadoaccion = models.CharField(max_length=1)
    fechaaccion = models.DateField()
    personaiid = models.ForeignKey('Persona', db_column='personaiid')

    class Meta:
        managed = True
        db_table = 'accion'


class Acompanante(models.Model):
    idacompanante = models.AutoField(primary_key=True)
    apellidoacompanante = models.CharField(max_length=30)
    cedacompanante = models.CharField(max_length=10)
    editingstatus = models.BooleanField()
    estadoacompanante = models.CharField(max_length=1)
    nombreacompanante = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'acompanante'


class ActComision(models.Model):
    idact_comision = models.AutoField(primary_key=True)
    actividadid = models.ForeignKey('Actividad', db_column='actividadid')
    comisionid = models.ForeignKey('Comision', db_column='comisionid')

    class Meta:
        managed = True
        db_table = 'act_comision'


class Actividad(models.Model):
    idactividad = models.AutoField(primary_key=True)
    descactividad = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadoactividad = models.CharField(max_length=1)
    nombreactividad = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'actividad'


class Area(models.Model):
    idarea = models.AutoField(primary_key=True)
    capacidadarea = models.BigIntegerField()
    descarea = models.CharField(max_length=160, blank=True, null=True)
    dimensionarea = models.CharField(max_length=20)
    editingstatus = models.BooleanField()
    estadoarea = models.CharField(max_length=1)
    nombrearea = models.CharField(max_length=30)
    tipoareaid = models.ForeignKey('Tipoarea', db_column='tipoareaid')

    class Meta:
        managed = True
        db_table = 'area'


class AreaArrend(models.Model):
    idarea_arrend = models.AutoField(primary_key=True)
    areaid = models.ForeignKey(Area, db_column='areaid')
    arrendamientoid = models.ForeignKey('Arrendamiento', db_column='arrendamientoid')

    class Meta:
        managed = True
        db_table = 'area_arrend'


class AreaEvento(models.Model):
    idarea_evento = models.AutoField(primary_key=True)
    areaid = models.ForeignKey(Area, db_column='areaid')
    eventoid = models.ForeignKey('Evento', db_column='eventoid')

    class Meta:
        managed = True
        db_table = 'area_evento'


class AreaRecurso(models.Model):
    idarea_recurso = models.AutoField(primary_key=True)
    areaid = models.ForeignKey(Area, db_column='areaid')
    recursoid = models.ForeignKey('Recurso', db_column='recursoid')

    class Meta:
        managed = True
        db_table = 'area_recurso'


class Arrendamiento(models.Model):
    idarrendamiento = models.AutoField(primary_key=True)
    descarrend = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadoarrend = models.CharField(max_length=1)
    fechaarrend = models.DateField()
    fechaemisionarrend = models.DateField()
    horaarrend = models.TimeField()
    personaid = models.ForeignKey('Persona', db_column='personaid')
    tipoarrendamientoid = models.ForeignKey('Tipoarrendamiento', db_column='tipoarrendamientoid')

    class Meta:
        managed = True
        db_table = 'arrendamiento'


class AsistenciaEvento(models.Model):
    idasistencia_evento = models.AutoField(primary_key=True)
    eventoid = models.ForeignKey('Evento', db_column='eventoid')
    personaid = models.ForeignKey('Persona', db_column='personaid')

    class Meta:
        managed = True
        db_table = 'asistencia_evento'


class Beneficiario(models.Model):
    idbeneficiario = models.AutoField(primary_key=True)
    apellidobene = models.CharField(max_length=30)
    cedbene = models.CharField(max_length=10)
    correobene = models.CharField(max_length=40)
    editingstatus = models.BooleanField()
    estadobene = models.CharField(max_length=1, blank=True, null=True)
    fechanacbene = models.DateField()
    nombrebene = models.CharField(max_length=30)
    parentescoid = models.ForeignKey('Parentesco', db_column='parentescoid')
    personaid = models.ForeignKey('Persona', db_column='personaid')

    class Meta:
        managed = True
        db_table = 'beneficiario'


class Cargo(models.Model):
    idcargo = models.AutoField(primary_key=True)
    desccargo = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadocargo = models.CharField(max_length=1)
    nombrecargo = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'cargo'


class Categoria(models.Model):
    idcategoria = models.AutoField(primary_key=True)
    desccategoria = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadocategoria = models.CharField(max_length=1)
    nombrecategoria = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'categoria'

class Ciudad(models.Model):
    idciudad = models.AutoField(primary_key=True)
    nombreciudad = models.CharField(max_length=30)
    estadoid = models.ForeignKey('Estado', db_column='estadoid')

    class Meta:
        managed = False
        db_table = 'ciudad'


class Club(models.Model):
    idclub = models.AutoField(primary_key=True)
    correoclub = models.CharField(max_length=40)
    direccionclub = models.CharField(max_length=50)
    editingstatus = models.BooleanField()
    estadoclub = models.CharField(max_length=1)
    logoclub = models.BinaryField(blank=True, null=True)
    misionclub = models.CharField(max_length=255)
    nombreclub = models.CharField(max_length=30)
    rifclub = models.CharField(max_length=20)
    tlfclub = models.CharField(max_length=20)
    visionclub = models.CharField(max_length=255)
    limiteacciones = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'club'


class Comision(models.Model):
    idcomision = models.AutoField(primary_key=True)
    desccomision = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadocomision = models.CharField(max_length=1)
    nombrecomision = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'comision'


class Documento(models.Model):
    iddocumento = models.AutoField(primary_key=True)
    archivodocumento = models.CharField(max_length=100)
    editingstatus = models.BooleanField()
    estadodocumento = models.CharField(max_length=1, blank=True, null=True)
    nombredocumento = models.CharField(max_length=30)
    clubid = models.ForeignKey(Club, db_column='clubid')

    class Meta:
        managed = True
        db_table = 'documento'


class Encuesta(models.Model):
    idencuesta = models.AutoField(primary_key=True)
    descencuesta = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadoencuesta = models.CharField(max_length=1)
    fechaencuesta = models.DateField()
    nombreencuesta = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'encuesta'

class Estado(models.Model):
    idestado = models.AutoField(primary_key=True)
    nombreestado = models.CharField(max_length=30)
    paisid = models.ForeignKey('Pais', db_column='paisid')

    class Meta:
        managed = False
        db_table = 'estado'


class Estadocivil(models.Model):
    idestadocivil = models.AutoField(primary_key=True)
    nombreestadocivil = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'estadocivil'

class Evento(models.Model):
    idevento = models.AutoField(primary_key=True)
    descevento = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadoevento = models.CharField(max_length=1)
    fechaevento = models.DateField()
    nombreevento = models.CharField(max_length=30)
    horaevento = models.TimeField()
    tipoeventoid = models.ForeignKey('Tipoevento', db_column='tipoeventoid')
    tipopublicoid = models.ForeignKey('Tipopublico', db_column='tipopublicoid')

    class Meta:
        managed = True
        db_table = 'evento'


class EventoComision(models.Model):
    idevento_comision = models.AutoField(primary_key=True)
    responsable = models.CharField(max_length=40)
    comisionid = models.ForeignKey(Comision, db_column='comisionid')
    eventoid = models.ForeignKey(Evento, db_column='eventoid')

    class Meta:
        managed = True
        db_table = 'evento_comision'


class EventoIndicador(models.Model):
    idevento_ind = models.AutoField(primary_key=True)
    valorespevento_ind = models.FloatField()
    eventoid = models.ForeignKey(Evento, db_column='eventoid')
    indicadorid = models.ForeignKey('Indicador', db_column='indicadorid')
    resultado = models.FloatField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'evento_indicador'


class Funcion(models.Model):
    idfuncion = models.AutoField(primary_key=True)
    estadofuncion = models.CharField(max_length=1)
    extrafuncion = models.CharField(max_length=100)
    iconurifuncion = models.CharField(max_length=100)
    idpadrefuncion = models.IntegerField()
    nombrefuncion = models.CharField(max_length=30)
    paginafuncion = models.CharField(max_length=100)

    class Meta:
        managed = True
        db_table = 'funcion'


class FuncionGrupo(models.Model):
    idfuncion_grupo = models.AutoField(primary_key=True)
    editingstatus = models.BooleanField()
    estadofuncion_grup = models.CharField(max_length=1)
    funcionid = models.ForeignKey(Funcion, db_column='funcionid')
    grupoid = models.ForeignKey('Grupo', db_column='grupoid')

    class Meta:
        managed = True
        db_table = 'funcion_grupo'


class Galeria(models.Model):
    idgaleria = models.AutoField(primary_key=True)
    editingstatus = models.BooleanField()
    estadogaleria = models.CharField(max_length=1)
    imagengaleria = models.BinaryField()
    nombregaleria = models.CharField(max_length=30)
    clubid = models.ForeignKey(Club, db_column='clubid')

    class Meta:
        managed = True
        db_table = 'galeria'


class Grupo(models.Model):
    idgrupo = models.AutoField(primary_key=True)
    descgrupo = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadogrupo = models.CharField(max_length=1)
    nombregrupo = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'grupo'


class Incidencia(models.Model):
    idincidencia = models.AutoField(primary_key=True)
    descincidencia = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadoincidencia = models.CharField(max_length=1)
    fechaincidencia = models.DateField()
    horaincidencia = models.TimeField()
    arrendamientoid = models.ForeignKey(Arrendamiento, db_column='arrendamientoid')
    personaid = models.ForeignKey('Persona', db_column='personaid')
    tipoincidenciaid = models.ForeignKey('Tipoincidencia', db_column='tipoincidenciaid')

    class Meta:
        managed = True
        db_table = 'incidencia'


class Indicador(models.Model):
    idindicador = models.AutoField(primary_key=True)
    descindicador = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadoindicador = models.CharField(max_length=1)
    nombreindicador = models.CharField(max_length=30)
    tipoindicadorid = models.ForeignKey('Tipoindicador', db_column='tipoindicadorid')
    unidadmedidaid = models.ForeignKey('Unidadmedida', db_column='unidadmedidaid')

    class Meta:
        managed = True
        db_table = 'indicador'


class Noticia(models.Model):
    idnoticia = models.AutoField(primary_key=True)
    descnoticia = models.CharField(max_length=160, blank=True, null=True)
    difusionnoticia = models.CharField(max_length=30)
    editingstatus = models.BooleanField()
    estadonoticia = models.CharField(max_length=1)
    fechaexpnoticia = models.DateField()
    fechaininoticia = models.DateField()
    imagennoticia = models.BinaryField()
    titulonoticia = models.CharField(max_length=30)
    clubid = models.ForeignKey(Club, db_column='clubid')
    tiponoticiaid = models.ForeignKey('Tiponoticia', db_column='tiponoticiaid')

    class Meta:
        managed = True
        db_table = 'noticia'


class Notificacion(models.Model):
    idnotificacion = models.AutoField(primary_key=True)
    asuntonotificacion = models.CharField(max_length=160)
    editingstatus = models.BooleanField()
    estadonotificacion = models.CharField(max_length=1)
    personaid = models.ForeignKey('Persona', db_column='personaid')
    tiponotificacionid = models.ForeignKey('Tiponotificacion', db_column='tiponotificacionid')

    class Meta:
        managed = True
        db_table = 'notificacion'


class Opcionpregunta(models.Model):
    idopcionpregunta = models.AutoField(primary_key=True)
    editingstatus = models.BooleanField()
    estadoopcionpreg = models.CharField(max_length=1)
    nombreopcion = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'opcionpregunta'


class Opinion(models.Model):
    idopinion = models.AutoField(primary_key=True)
    asuntoopinion = models.CharField(max_length=20)
    descopinion = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadoopinion = models.CharField(max_length=1)
    tipoopinionid = models.ForeignKey('Tipoopinion', db_column='tipoopinionid')
    usuarioid = models.ForeignKey('Usuario', db_column='usuarioid')

    class Meta:
        managed = True
        db_table = 'opinion'

class Pais(models.Model):
    idpais = models.AutoField(primary_key=True)
    nombrepais = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'pais'


class Parentesco(models.Model):
    idparentesco = models.AutoField(primary_key=True)
    descparentesco = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadoparentesco = models.CharField(max_length=1)
    nombreparentesco = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'parentesco'


class Persona(models.Model):
    idpersona = models.AutoField(primary_key=True)
    apellidopersona = models.CharField(max_length=30)
    cargopersona = models.CharField(max_length=30, blank=True, null=True)
    cedpersona = models.CharField(max_length=10)
    correopersona = models.CharField(max_length=40, blank=True, null=True)
    direcpersona = models.CharField(max_length=50, blank=True, null=True)
    directrabpersona = models.CharField(max_length=50, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadocivilpersona = models.CharField(max_length=30, blank=True, null=True)
    estadopersona = models.CharField(max_length=1)
    fechanacpersona = models.DateField(blank=True, null=True)
    imagenpersona = models.BinaryField(blank=True, null=True)
    lugarnacpersona = models.CharField(max_length=20, blank=True, null=True)
    nombrepersona = models.CharField(max_length=30)
    profesionpersona = models.CharField(max_length=30, blank=True, null=True)
    sexopersona = models.CharField(max_length=1, blank=True, null=True)
    sueldopersona = models.FloatField(blank=True, null=True)
    tlfcasapersona = models.CharField(max_length=20, blank=True, null=True)
    tlfcelpersona = models.CharField(max_length=20, blank=True, null=True)
    tlftrabpersona = models.CharField(max_length=20, blank=True, null=True)
    parentescoid = models.ForeignKey(Parentesco, db_column='parentescoid', blank=True, null=True)
    idpadrepersona = models.ForeignKey('Persona', db_column='idpadrepersona')
    empresatrabpersona = models.CharField(max_length=30, blank=True, null=True)
    cargoid = models.ForeignKey(Cargo, db_column='cargoid', blank=True, null=True)
    empresatrabpersona = models.CharField(max_length=30, blank=True, null=True)
    tipopersonaid = models.ForeignKey('Tipopersona', db_column='tipopersonaid')

    class Meta:
        managed = True
        db_table = 'persona'


class Postulacion(models.Model):
    idpostulacion = models.AutoField(primary_key=True)
    editingstatus = models.BooleanField()
    estadopostulacion = models.CharField(max_length=1)
    fechapostulacion = models.DateField()
    personaid = models.ForeignKey(Persona, db_column='personaid')

    class Meta:
        managed = True
        db_table = 'postulacion'


class PostulacionRef(models.Model):
    idpost_ref = models.AutoField(primary_key=True)
    postulacionid = models.ForeignKey(Postulacion, db_column='postulacionid')
    referenciaid = models.ForeignKey('Referencia', db_column='referenciaid')

    class Meta:
        managed = True
        db_table = 'postulacion_ref'


class Preferencia(models.Model):
    idpreferencia = models.AutoField(primary_key=True)
    descpreferencia = models.CharField(max_length=160, blank=True, null=True)
    estadopreferencia = models.CharField(max_length=1)
    nombrepreferencia = models.CharField(max_length=30)
    categoriaid = models.ForeignKey(Categoria, db_column='categoriaid')

    class Meta:
        managed = True
        db_table = 'preferencia'


class PreferenciaEvento(models.Model):
    idpreferencia_eve = models.AutoField(primary_key=True)
    eventoid = models.ForeignKey(Evento, db_column='eventoid')
    preferenciaid = models.ForeignKey(Preferencia, db_column='preferenciaid')

    class Meta:
        managed = True
        db_table = 'preferencia_evento'


class PreferenciaPersona(models.Model):
    idpreferencia_per = models.AutoField(primary_key=True)
    personaid = models.ForeignKey(Persona, db_column='personaid')
    preferenciaid = models.ForeignKey(Preferencia, db_column='preferenciaid')

    class Meta:
        managed = True
        db_table = 'preferencia_persona'


class Pregunta(models.Model):
    idpregunta = models.AutoField(primary_key=True)
    editingstatus = models.BooleanField()
    estadopregunta = models.CharField(max_length=1)
    nombrepregunta = models.CharField(max_length=30)
    categoriaid = models.ForeignKey(Categoria, db_column='categoriaid')
    encuestaid = models.ForeignKey(Encuesta, db_column='encuestaid')
    opcionpreguntaid = models.ForeignKey(Opcionpregunta, db_column='opcionpreguntaid')

    class Meta:
        managed = True
        db_table = 'pregunta'

class Profesion(models.Model):
    idprofesion = models.AutoField(primary_key=True)
    nombreprofesion = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'profesion'

class Rechazo(models.Model):
    idrechazo = models.AutoField(primary_key=True)
    descrechazo = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadorechazo = models.CharField(max_length=1)
    arrendamientoid = models.ForeignKey(Arrendamiento, db_column='arrendamientoid', blank=True, null=True)
    postulacionid = models.ForeignKey(Postulacion, db_column='postulacionid', blank=True, null=True)
    tiporechazoid = models.ForeignKey('Tiporechazo', db_column='tiporechazoid')

    class Meta:
        managed = True
        db_table = 'rechazo'


class Recurso(models.Model):
    idrecurso = models.AutoField(primary_key=True)
    cantidadrecurso = models.BigIntegerField()
    descrecurso = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadorecurso = models.CharField(max_length=1)
    nombrerecurso = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'recurso'


class Referencia(models.Model):
    idreferencia = models.AutoField(primary_key=True)
    editingstatus = models.BooleanField()
    estadoreferencia = models.CharField(max_length=1)
    personaid = models.ForeignKey(Persona, db_column='personaid', blank=True, null=True)
    postulacionid = models.ForeignKey(Postulacion, db_column='postulacionid', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'referencia'


class Respuestaencuesta(models.Model):
    idrespuestaenc = models.AutoField(primary_key=True)
    editingstatus = models.BooleanField()
    estadorespuestaenc = models.CharField(max_length=1, blank=True, null=True)
    fecharespuestaenc = models.DateField()
    opcionpreguntaid = models.ForeignKey(Opcionpregunta, db_column='opcionpreguntaid')
    preguntaid = models.ForeignKey(Pregunta, db_column='preguntaid')
    usuarioid = models.ForeignKey('Usuario', db_column='usuarioid')

    class Meta:
        managed = True
        db_table = 'respuestaencuesta'


class Sancion(models.Model):
    idsancion = models.AutoField(primary_key=True)
    descsancion = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadosancion = models.CharField(max_length=1)
    fechasancion = models.DateField()
    personaid = models.ForeignKey(Persona, db_column='personaid')
    tiposancionid = models.ForeignKey('Tiposancion', db_column='tiposancionid')

    class Meta:
        managed = True
        db_table = 'sancion'


class Tipoarea(models.Model):
    idtipoarea = models.AutoField(primary_key=True)
    desctipoarea = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadotipoarea = models.CharField(max_length=1)
    nombretipoarea = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'tipoarea'


class Tipoarrendamiento(models.Model):
    idtipoarrend = models.AutoField(primary_key=True)
    descarrend = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadotipoarrend = models.CharField(max_length=1)
    nombrearrend = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'tipoarrendamiento'


class Tipoevento(models.Model):
    idtipoevento = models.AutoField(primary_key=True)
    desctipoevento = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadotipoevento = models.CharField(max_length=1)
    nombretipoevento = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'tipoevento'


class Tipoincidencia(models.Model):
    idtipoincid = models.AutoField(primary_key=True)
    desctipoincid = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadotipoincid = models.CharField(max_length=1)
    nombretipoincid = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'tipoincidencia'


class Tipoindicador(models.Model):
    idtipoindicador = models.AutoField(primary_key=True)
    desctipoind = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadotipoind = models.CharField(max_length=1)
    nombretipoind = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'tipoindicador'


class Tiponoticia(models.Model):
    idtiponoticia = models.AutoField(primary_key=True)
    desctiponoticia = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadotiponoticia = models.CharField(max_length=1)
    nombretiponoticia = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'tiponoticia'


class Tiponotificacion(models.Model):
    idtiponotif = models.AutoField(primary_key=True)
    desctiponotif = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadotiponotif = models.CharField(max_length=1)
    nombretiponotif = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'tiponotificacion'


class Tipoopinion(models.Model):
    idtipoopinion = models.AutoField(primary_key=True)
    desctipoopinion = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadotipoopinion = models.CharField(max_length=1)
    nombretipoopinion = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'tipoopinion'


class Tipopersona(models.Model):
    idtipopersona = models.AutoField(primary_key=True)
    desctipopersona = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadotipopersona = models.CharField(max_length=4)
    nombretipopersona = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'tipopersona'


class Tipopublico(models.Model):
    idtipopubico = models.AutoField(primary_key=True)
    desctipopublico = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadotipopublico = models.CharField(max_length=1)
    nombretipopublico = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'tipopublico'


class Tiporechazo(models.Model):
    idtiporechazo = models.AutoField(primary_key=True)
    desctiporechazo = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadotiporechazo = models.CharField(max_length=1)
    nombretiporechazo = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'tiporechazo'


class Tiposancion(models.Model):
    idtiposancion = models.AutoField(primary_key=True)
    desctiposancion = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadotiposancion = models.CharField(max_length=1)
    nombretiposancion = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'tiposancion'


class Unidadmedida(models.Model):
    idunidadmedida = models.AutoField(primary_key=True)
    descunidadmedida = models.CharField(max_length=160, blank=True, null=True)
    editingstatus = models.BooleanField()
    estadounidadmedida = models.CharField(max_length=1)
    nombreunidadmedida = models.CharField(max_length=30)

    class Meta:
        managed = True
        db_table = 'unidadmedida'


class Usuario(models.Model):
    idusuario = models.AutoField(primary_key=True)
    claveusuario = models.CharField(max_length=30)
    editingstatus = models.BooleanField()
    estadousuario = models.CharField(max_length=1)
    fechausuario = models.DateField()
    nombreusuario = models.CharField(max_length=30)
    grupoid = models.ForeignKey(Grupo, db_column='grupoid')
    personaid = models.ForeignKey(Persona, db_column='personaid')

    class Meta:
        managed = True
        db_table = 'usuario'


class Visita(models.Model):
    idvisita = models.AutoField(primary_key=True)
    editingstatus = models.BooleanField()
    fechavisita = models.DateField()
    horavisita = models.TimeField()
    personaid = models.ForeignKey(Persona, db_column='personaid')

    class Meta:
        managed = True
        db_table = 'visita'


class VisitaAcomp(models.Model):
    idvisita_acomp = models.AutoField(primary_key=True)
    acompananteid = models.ForeignKey(Acompanante, db_column='acompananteid')
    visitaid = models.ForeignKey(Visita, db_column='visitaid')

    class Meta:
        managed = True
        db_table = 'visita_acomp'

class Motivo(models.Model):
    idmotivo = models.AutoField(primary_key=True)
    nombremotivo = models.CharField(max_length=30, blank=True, null=True)
    descmotivo = models.CharField(max_length=160, blank=True, null=True)
    estadomotivo = models.CharField(max_length=1, blank=True, null=True)
    editingstatus = models.BooleanField()

    class Meta:
        managed = True
        db_table = 'motivo'

class Cancelacion(models.Model):
    idcancelacion = models.AutoField(primary_key=True)
    motivoid = models.ForeignKey(Motivo, db_column='idmotivo')
    arrendamientoid = models.ForeignKey(Arrendamiento, db_column='arrendamientoid')
    eventoid = models.ForeignKey(Evento, db_column='eventoid')
    desccancelacion = models.CharField(max_length=160, blank=True, null=True)
    estadocancelacion = models.CharField(max_length=1, blank=True, null=True)
    editingstatus = models.BooleanField()

    class Meta:
        managed = True
        db_table = 'cancelacion'
