from django.conf.urls import *
from WebServices.views import *

urlpatterns = [
    url(r'^prueba', prueba),
    url(r'^email', email),
    url(r'^contacto', contacto),
    url(r'^eventos', eventos),
    url(r'^areas', areas),
    url(r'^profesiones', profesiones),
    url(r'^paises', paises),
    url(r'^estados', estados),
    url(r'^estadocivil', estadocivil),
    url(r'^preferencias', preferencias),
    url(r'^categorias', categorias),
    url(r'^adiponibles', adiponibles),
    url(r'^estadoSolicitud', edosolicitud),
    url(r'^noticias', noticias),
    url(r'^galeria', galeria),
    url(r'^solicitud2', solicitud2),
    url(r'^tiposArrendamiento', tiposArrendamiento),
    url(r'^buscarReferencia', buscarReferencia),
    url(r'^buscarCliente', buscarCliente),
    url(r'^aDisponiblesFecha', aDisponiblesFecha),
    url(r'^solicitarMembresia',solicitarMembresia),
    url(r'^solicitarArrendamiento',solicitarArrendamiento)    
]