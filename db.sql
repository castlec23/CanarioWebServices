--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE accion (
    "nroAccion" integer NOT NULL,
    "fechaAsignacion" date,
    "estadoAccion" character varying(50),
    "idClub" integer NOT NULL,
    "idPersona" integer NOT NULL,
    "idSolicitudAccion" integer NOT NULL
);


ALTER TABLE public.accion OWNER TO postgres;

--
-- Name: accion_nroAccion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "accion_nroAccion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."accion_nroAccion_seq" OWNER TO postgres;

--
-- Name: accion_nroAccion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "accion_nroAccion_seq" OWNED BY accion."nroAccion";


--
-- Name: apelacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE apelacion (
    "idApelacion" integer NOT NULL,
    "descApelacion" text,
    "fechaApelacion" date,
    "estadoApelacion" character varying(50) NOT NULL,
    "idPersona" integer NOT NULL,
    "idSancion" integer NOT NULL
);


ALTER TABLE public.apelacion OWNER TO postgres;

--
-- Name: apelacion_idApelacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "apelacion_idApelacion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."apelacion_idApelacion_seq" OWNER TO postgres;

--
-- Name: apelacion_idApelacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "apelacion_idApelacion_seq" OWNED BY apelacion."idApelacion";


--
-- Name: area; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE area (
    "idArea" integer NOT NULL,
    "descArea" text,
    "capacidadPersona" integer,
    dimensiones character varying(50),
    "ubicacionArea" text,
    "estadoArea" character varying(50),
    "idClub" integer NOT NULL,
    "idTipoArea" integer NOT NULL
);


ALTER TABLE public.area OWNER TO postgres;

--
-- Name: area_idArea_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "area_idArea_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."area_idArea_seq" OWNER TO postgres;

--
-- Name: area_idArea_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "area_idArea_seq" OWNED BY area."idArea";


--
-- Name: arrendamiento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE arrendamiento (
    "idArrendamiento" integer NOT NULL,
    "descArrendamiento" text,
    "pagoArrendamiento" integer,
    "fechaPago" date,
    "idSolicitudArrendamiento" integer NOT NULL
);


ALTER TABLE public.arrendamiento OWNER TO postgres;

--
-- Name: arrendamiento_idArrendamiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "arrendamiento_idArrendamiento_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."arrendamiento_idArrendamiento_seq" OWNER TO postgres;

--
-- Name: arrendamiento_idArrendamiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "arrendamiento_idArrendamiento_seq" OWNED BY arrendamiento."idArrendamiento";


--
-- Name: asistencia_Evento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "asistencia_Evento" (
    id integer NOT NULL,
    "cantidadMiembros" integer,
    "idEvento" integer NOT NULL,
    "idPersona" integer NOT NULL
);


ALTER TABLE public."asistencia_Evento" OWNER TO postgres;

--
-- Name: asistencia_Evento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "asistencia_Evento_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."asistencia_Evento_id_seq" OWNER TO postgres;

--
-- Name: asistencia_Evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "asistencia_Evento_id_seq" OWNED BY "asistencia_Evento".id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: cargo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cargo (
    "idCargo" integer NOT NULL,
    "nombreCargo" character varying(50),
    "idTipoPersona" integer NOT NULL
);


ALTER TABLE public.cargo OWNER TO postgres;

--
-- Name: cargo_idCargo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "cargo_idCargo_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."cargo_idCargo_seq" OWNER TO postgres;

--
-- Name: cargo_idCargo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "cargo_idCargo_seq" OWNED BY cargo."idCargo";


--
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categoria (
    "idCategoria" integer NOT NULL,
    "nombreCategoria" character varying(50),
    "descCategoria" character varying(250)
);


ALTER TABLE public.categoria OWNER TO postgres;

--
-- Name: categoria_idCategoria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "categoria_idCategoria_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."categoria_idCategoria_seq" OWNER TO postgres;

--
-- Name: categoria_idCategoria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "categoria_idCategoria_seq" OWNED BY categoria."idCategoria";


--
-- Name: club; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE club (
    "idClub" integer NOT NULL,
    descripcion text,
    direccion text,
    "nroAcciones" integer,
    "idUsuario" integer NOT NULL
);


ALTER TABLE public.club OWNER TO postgres;

--
-- Name: club_idClub_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "club_idClub_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."club_idClub_seq" OWNER TO postgres;

--
-- Name: club_idClub_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "club_idClub_seq" OWNED BY club."idClub";


--
-- Name: comision; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comision (
    "idComision" integer NOT NULL,
    "nombreComision" character varying(50),
    "descComision" text,
    "estadoComision" character varying(50),
    "idEvento" integer NOT NULL,
    "idPersona" integer NOT NULL,
    "idTipoComision" integer NOT NULL
);


ALTER TABLE public.comision OWNER TO postgres;

--
-- Name: comision_idComision_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "comision_idComision_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."comision_idComision_seq" OWNER TO postgres;

--
-- Name: comision_idComision_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "comision_idComision_seq" OWNED BY comision."idComision";


--
-- Name: corsheaders_corsmodel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE corsheaders_corsmodel (
    id integer NOT NULL,
    cors character varying(255) NOT NULL
);


ALTER TABLE public.corsheaders_corsmodel OWNER TO postgres;

--
-- Name: corsheaders_corsmodel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE corsheaders_corsmodel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.corsheaders_corsmodel_id_seq OWNER TO postgres;

--
-- Name: corsheaders_corsmodel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE corsheaders_corsmodel_id_seq OWNED BY corsheaders_corsmodel.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: encuesta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE encuesta (
    "idEncuesta" integer NOT NULL,
    "nombreEncuesta" character varying(50),
    "descEncuesta" text,
    "fechaEncuesta" date
);


ALTER TABLE public.encuesta OWNER TO postgres;

--
-- Name: encuesta_Pregunta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "encuesta_Pregunta" (
    id integer NOT NULL,
    "idEncuesta" integer NOT NULL,
    "idPregunta" integer NOT NULL
);


ALTER TABLE public."encuesta_Pregunta" OWNER TO postgres;

--
-- Name: encuesta_Pregunta_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "encuesta_Pregunta_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."encuesta_Pregunta_id_seq" OWNER TO postgres;

--
-- Name: encuesta_Pregunta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "encuesta_Pregunta_id_seq" OWNED BY "encuesta_Pregunta".id;


--
-- Name: encuesta_idEncuesta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "encuesta_idEncuesta_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."encuesta_idEncuesta_seq" OWNER TO postgres;

--
-- Name: encuesta_idEncuesta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "encuesta_idEncuesta_seq" OWNED BY encuesta."idEncuesta";


--
-- Name: evento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE evento (
    "idEvento" integer NOT NULL,
    "capaciadadAsistencia" integer,
    "fechaCreacionEvento" date,
    "estadoEvento" character varying(50),
    "idSolicitudEvento" integer NOT NULL
);


ALTER TABLE public.evento OWNER TO postgres;

--
-- Name: evento_idEvento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "evento_idEvento_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."evento_idEvento_seq" OWNER TO postgres;

--
-- Name: evento_idEvento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "evento_idEvento_seq" OWNED BY evento."idEvento";


--
-- Name: funcion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE funcion (
    "idFuncion" integer NOT NULL,
    "nombreFuncion" character varying(50)
);


ALTER TABLE public.funcion OWNER TO postgres;

--
-- Name: funcionGrupo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "funcionGrupo" (
    id integer NOT NULL,
    "idFuncion" integer NOT NULL,
    "idGrupo" integer NOT NULL
);


ALTER TABLE public."funcionGrupo" OWNER TO postgres;

--
-- Name: funcionGrupo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "funcionGrupo_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."funcionGrupo_id_seq" OWNER TO postgres;

--
-- Name: funcionGrupo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "funcionGrupo_id_seq" OWNED BY "funcionGrupo".id;


--
-- Name: funcion_idFuncion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "funcion_idFuncion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."funcion_idFuncion_seq" OWNER TO postgres;

--
-- Name: funcion_idFuncion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "funcion_idFuncion_seq" OWNED BY funcion."idFuncion";


--
-- Name: grupo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE grupo (
    "idGrupo" integer NOT NULL,
    "nombreGrupo" character varying(50)
);


ALTER TABLE public.grupo OWNER TO postgres;

--
-- Name: grupo_idGrupo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "grupo_idGrupo_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."grupo_idGrupo_seq" OWNER TO postgres;

--
-- Name: grupo_idGrupo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "grupo_idGrupo_seq" OWNED BY grupo."idGrupo";


--
-- Name: incidencia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE incidencia (
    "idIncidencia" integer NOT NULL,
    "descIncidencia" text,
    "fechaIncidencia" date,
    "estadoIncidencia" character varying(50),
    "idArrendamiento" integer NOT NULL,
    "idEvento" integer NOT NULL,
    "idPersona" integer NOT NULL,
    "idTipoIncidencia" integer NOT NULL,
    "idVisita" integer NOT NULL
);


ALTER TABLE public.incidencia OWNER TO postgres;

--
-- Name: incidencia_idIncidencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "incidencia_idIncidencia_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."incidencia_idIncidencia_seq" OWNER TO postgres;

--
-- Name: incidencia_idIncidencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "incidencia_idIncidencia_seq" OWNED BY incidencia."idIncidencia";


--
-- Name: indicador; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE indicador (
    "idIndicador" integer NOT NULL,
    "nombreIndicador" character varying(50),
    "descIndicador" text,
    "estadoIndicador" character varying(50),
    calculo integer,
    "idTipoindicador" integer NOT NULL,
    "idUnidadMedida" integer NOT NULL
);


ALTER TABLE public.indicador OWNER TO postgres;

--
-- Name: indicador_idIndicador_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "indicador_idIndicador_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."indicador_idIndicador_seq" OWNER TO postgres;

--
-- Name: indicador_idIndicador_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "indicador_idIndicador_seq" OWNED BY indicador."idIndicador";


--
-- Name: morosidad; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE morosidad (
    "idMorosidad" integer NOT NULL,
    "fechaVencimiento" date,
    "fechaPago" date,
    "idPersona" integer NOT NULL
);


ALTER TABLE public.morosidad OWNER TO postgres;

--
-- Name: morosidad_idMorosidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "morosidad_idMorosidad_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."morosidad_idMorosidad_seq" OWNER TO postgres;

--
-- Name: morosidad_idMorosidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "morosidad_idMorosidad_seq" OWNED BY morosidad."idMorosidad";


--
-- Name: noticia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE noticia (
    "idNoticia" integer NOT NULL,
    "rutaImagennoticia" character varying(150),
    "descNoticia" text,
    "idEvento" integer NOT NULL,
    "idTiponoticia" integer NOT NULL
);


ALTER TABLE public.noticia OWNER TO postgres;

--
-- Name: noticia_idNoticia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "noticia_idNoticia_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."noticia_idNoticia_seq" OWNER TO postgres;

--
-- Name: noticia_idNoticia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "noticia_idNoticia_seq" OWNED BY noticia."idNoticia";


--
-- Name: opcionPregunta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "opcionPregunta" (
    "idOpcionpregunta" integer NOT NULL,
    "nombreOpcionpregunta" character varying(50),
    "idPregunta" integer NOT NULL
);


ALTER TABLE public."opcionPregunta" OWNER TO postgres;

--
-- Name: opcionPregunta_idOpcionpregunta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "opcionPregunta_idOpcionpregunta_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."opcionPregunta_idOpcionpregunta_seq" OWNER TO postgres;

--
-- Name: opcionPregunta_idOpcionpregunta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "opcionPregunta_idOpcionpregunta_seq" OWNED BY "opcionPregunta"."idOpcionpregunta";


--
-- Name: opinion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE opinion (
    "idOpinion" integer NOT NULL,
    "descOpinon" text,
    "fechaOpinion" date,
    "estadoOpinion" character varying(50),
    "idPersona" integer NOT NULL,
    "idTipoOpinion" integer NOT NULL,
    "idUsuario" integer NOT NULL
);


ALTER TABLE public.opinion OWNER TO postgres;

--
-- Name: opinion_idOpinion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "opinion_idOpinion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."opinion_idOpinion_seq" OWNER TO postgres;

--
-- Name: opinion_idOpinion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "opinion_idOpinion_seq" OWNED BY opinion."idOpinion";


--
-- Name: persona; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE persona (
    "idPersona" integer NOT NULL,
    nombre character varying(50),
    "Apellido" character varying(50),
    cedula character varying(30),
    direccion text,
    "correoElectronico" character varying(75),
    "fechaNacimiento" date,
    sexo character varying(1),
    "lugarNacimiento" character varying(50),
    nacionalidad character varying(50),
    profesion character varying(50),
    "cargoTrabajo" character varying(50),
    sueldo integer,
    hobby character varying(50),
    "estadoCivil" character varying(50),
    "idPadre" integer,
    rutaimagen character varying(100) NOT NULL,
    "idCargo" integer NOT NULL,
    "idTipoParentesco" integer NOT NULL,
    "idTipoPersona" integer NOT NULL
);


ALTER TABLE public.persona OWNER TO postgres;

--
-- Name: persona_idPersona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "persona_idPersona_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."persona_idPersona_seq" OWNER TO postgres;

--
-- Name: persona_idPersona_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "persona_idPersona_seq" OWNED BY persona."idPersona";


--
-- Name: pregunta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pregunta (
    "idPregunta" integer NOT NULL,
    "nombrePregunta" character varying(50),
    "idCategoria" integer NOT NULL,
    "idTipopregunta" integer NOT NULL
);


ALTER TABLE public.pregunta OWNER TO postgres;

--
-- Name: pregunta_idPregunta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "pregunta_idPregunta_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."pregunta_idPregunta_seq" OWNER TO postgres;

--
-- Name: pregunta_idPregunta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "pregunta_idPregunta_seq" OWNED BY pregunta."idPregunta";


--
-- Name: rechazo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE rechazo (
    "idRechazo" integer NOT NULL,
    "nombreRechazo" character varying(50),
    "descRechazo" text,
    "estadoRechazo" character varying(50),
    "idApelacion" integer NOT NULL,
    "idEvento" integer NOT NULL,
    "idSolicitudAccion" integer NOT NULL,
    "idSolicitudArrendamiento" integer NOT NULL,
    "idTipoRechazo" integer NOT NULL
);


ALTER TABLE public.rechazo OWNER TO postgres;

--
-- Name: rechazo_idRechazo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "rechazo_idRechazo_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."rechazo_idRechazo_seq" OWNER TO postgres;

--
-- Name: rechazo_idRechazo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "rechazo_idRechazo_seq" OWNED BY rechazo."idRechazo";


--
-- Name: recurso; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE recurso (
    "idRecurso" integer NOT NULL,
    "nombreRecurso" character varying(50) NOT NULL,
    "cantidadRecurso" integer,
    "descRecurso" character varying(100),
    "estadoRecurso" character varying(50) NOT NULL
);


ALTER TABLE public.recurso OWNER TO postgres;

--
-- Name: recurso_Arrendamiento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "recurso_Arrendamiento" (
    id integer NOT NULL,
    "cantidadRecurso" integer,
    "idArrendamiento" integer NOT NULL,
    "idRecurso" integer NOT NULL
);


ALTER TABLE public."recurso_Arrendamiento" OWNER TO postgres;

--
-- Name: recurso_Arrendamiento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "recurso_Arrendamiento_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."recurso_Arrendamiento_id_seq" OWNER TO postgres;

--
-- Name: recurso_Arrendamiento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "recurso_Arrendamiento_id_seq" OWNED BY "recurso_Arrendamiento".id;


--
-- Name: recurso_Evento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "recurso_Evento" (
    id integer NOT NULL,
    "cantidadRecurso" integer,
    "idEvento" integer NOT NULL,
    "idRecurso" integer NOT NULL
);


ALTER TABLE public."recurso_Evento" OWNER TO postgres;

--
-- Name: recurso_Evento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "recurso_Evento_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."recurso_Evento_id_seq" OWNER TO postgres;

--
-- Name: recurso_Evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "recurso_Evento_id_seq" OWNED BY "recurso_Evento".id;


--
-- Name: recurso_idRecurso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "recurso_idRecurso_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."recurso_idRecurso_seq" OWNER TO postgres;

--
-- Name: recurso_idRecurso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "recurso_idRecurso_seq" OWNED BY recurso."idRecurso";


--
-- Name: referencia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE referencia (
    "idReferencia" integer NOT NULL,
    "nombreReferencia" character varying(20),
    "apellidoReferencia" character varying(20),
    "nroAccionReferencia" integer,
    "telRefencia" integer,
    "idSolicitudAccion" integer NOT NULL
);


ALTER TABLE public.referencia OWNER TO postgres;

--
-- Name: referencia_idReferencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "referencia_idReferencia_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."referencia_idReferencia_seq" OWNER TO postgres;

--
-- Name: referencia_idReferencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "referencia_idReferencia_seq" OWNED BY referencia."idReferencia";


--
-- Name: respuestaEncuesta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "respuestaEncuesta" (
    "idRespuestaencuesta" integer NOT NULL,
    "fechaRespuestaencuesta" date,
    "idOpcionpregunta" integer NOT NULL,
    "idUsuario" integer NOT NULL
);


ALTER TABLE public."respuestaEncuesta" OWNER TO postgres;

--
-- Name: respuestaEncuesta_idRespuestaencuesta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "respuestaEncuesta_idRespuestaencuesta_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."respuestaEncuesta_idRespuestaencuesta_seq" OWNER TO postgres;

--
-- Name: respuestaEncuesta_idRespuestaencuesta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "respuestaEncuesta_idRespuestaencuesta_seq" OWNED BY "respuestaEncuesta"."idRespuestaencuesta";


--
-- Name: resultado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE resultado (
    "idResultado" integer NOT NULL,
    "nombreResultado" character varying(50),
    "valorEsperado" integer,
    "idIndicador" integer NOT NULL
);


ALTER TABLE public.resultado OWNER TO postgres;

--
-- Name: resultado_Arrendamiento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "resultado_Arrendamiento" (
    id integer NOT NULL,
    "idArrendamiento" integer NOT NULL,
    "idResultado" integer NOT NULL
);


ALTER TABLE public."resultado_Arrendamiento" OWNER TO postgres;

--
-- Name: resultado_Arrendamiento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "resultado_Arrendamiento_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."resultado_Arrendamiento_id_seq" OWNER TO postgres;

--
-- Name: resultado_Arrendamiento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "resultado_Arrendamiento_id_seq" OWNED BY "resultado_Arrendamiento".id;


--
-- Name: resultado_Evento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "resultado_Evento" (
    id integer NOT NULL,
    "idEvento" integer NOT NULL,
    "idResultado" integer NOT NULL
);


ALTER TABLE public."resultado_Evento" OWNER TO postgres;

--
-- Name: resultado_Evento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "resultado_Evento_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."resultado_Evento_id_seq" OWNER TO postgres;

--
-- Name: resultado_Evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "resultado_Evento_id_seq" OWNED BY "resultado_Evento".id;


--
-- Name: resultado_Persona; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "resultado_Persona" (
    id integer NOT NULL,
    "idPersona" integer NOT NULL,
    "idResultado" integer NOT NULL
);


ALTER TABLE public."resultado_Persona" OWNER TO postgres;

--
-- Name: resultado_Persona_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "resultado_Persona_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."resultado_Persona_id_seq" OWNER TO postgres;

--
-- Name: resultado_Persona_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "resultado_Persona_id_seq" OWNED BY "resultado_Persona".id;


--
-- Name: resultado_idResultado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "resultado_idResultado_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."resultado_idResultado_seq" OWNER TO postgres;

--
-- Name: resultado_idResultado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "resultado_idResultado_seq" OWNED BY resultado."idResultado";


--
-- Name: sancion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sancion (
    "idSancion" integer NOT NULL,
    "descSancion" text,
    "fechaSancion" date,
    "estadoSancion" character varying(50),
    "idIncidencia" integer NOT NULL,
    "idPersona" integer NOT NULL,
    "idTipoSancion" integer NOT NULL
);


ALTER TABLE public.sancion OWNER TO postgres;

--
-- Name: sancion_idSancion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "sancion_idSancion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."sancion_idSancion_seq" OWNER TO postgres;

--
-- Name: sancion_idSancion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "sancion_idSancion_seq" OWNED BY sancion."idSancion";


--
-- Name: solicitudAccion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "solicitudAccion" (
    "idSolicitudAccion" integer NOT NULL,
    "fechaSolicitud" date,
    "estadoSolicitudAccion" character varying(50),
    "idPersona" integer NOT NULL
);


ALTER TABLE public."solicitudAccion" OWNER TO postgres;

--
-- Name: solicitudAccion_idSolicitudAccion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "solicitudAccion_idSolicitudAccion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."solicitudAccion_idSolicitudAccion_seq" OWNER TO postgres;

--
-- Name: solicitudAccion_idSolicitudAccion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "solicitudAccion_idSolicitudAccion_seq" OWNED BY "solicitudAccion"."idSolicitudAccion";


--
-- Name: solicitudArrendamiento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "solicitudArrendamiento" (
    "idSolicitudArrendamiento" integer NOT NULL,
    "descripcionArrendamiento" text,
    "fechaArrendamiento" date,
    "horaInicio" timestamp with time zone,
    "horaFin" timestamp with time zone,
    monto integer,
    "idArea" integer NOT NULL,
    "idPersona" integer NOT NULL
);


ALTER TABLE public."solicitudArrendamiento" OWNER TO postgres;

--
-- Name: solicitudArrendamiento_idSolicitudArrendamiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "solicitudArrendamiento_idSolicitudArrendamiento_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."solicitudArrendamiento_idSolicitudArrendamiento_seq" OWNER TO postgres;

--
-- Name: solicitudArrendamiento_idSolicitudArrendamiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "solicitudArrendamiento_idSolicitudArrendamiento_seq" OWNED BY "solicitudArrendamiento"."idSolicitudArrendamiento";


--
-- Name: solicitudEvento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "solicitudEvento" (
    "idSolicitudEvento" integer NOT NULL,
    "nombreEvento" character varying(50),
    "descEvento" text,
    "fechaEvento" date,
    "estadoSolEvento" character varying(50),
    "idPersona" integer NOT NULL,
    "idTipoEvento" integer NOT NULL,
    "idTipoGenero" integer NOT NULL,
    "idTipoPublico" integer NOT NULL
);


ALTER TABLE public."solicitudEvento" OWNER TO postgres;

--
-- Name: solicitudEvento_idSolicitudEvento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "solicitudEvento_idSolicitudEvento_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."solicitudEvento_idSolicitudEvento_seq" OWNER TO postgres;

--
-- Name: solicitudEvento_idSolicitudEvento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "solicitudEvento_idSolicitudEvento_seq" OWNED BY "solicitudEvento"."idSolicitudEvento";


--
-- Name: tarea; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tarea (
    "idTarea" integer NOT NULL,
    "nombreTarea" character varying(50),
    "descTarea" text,
    "fechaTarea" date,
    "responsableTarea" character varying(50),
    "estadoTarea" character varying(50)
);


ALTER TABLE public.tarea OWNER TO postgres;

--
-- Name: tarea_idTarea_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tarea_idTarea_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tarea_idTarea_seq" OWNER TO postgres;

--
-- Name: tarea_idTarea_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tarea_idTarea_seq" OWNED BY tarea."idTarea";


--
-- Name: tipoArea; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoArea" (
    "idTipoArea" integer NOT NULL,
    "nombreTipoArea" character varying(50),
    "descTipoArea" text
);


ALTER TABLE public."tipoArea" OWNER TO postgres;

--
-- Name: tipoArea_idTipoArea_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoArea_idTipoArea_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoArea_idTipoArea_seq" OWNER TO postgres;

--
-- Name: tipoArea_idTipoArea_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoArea_idTipoArea_seq" OWNED BY "tipoArea"."idTipoArea";


--
-- Name: tipoCargo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoCargo" (
    "idTipoCargo" integer NOT NULL,
    "nombreTipoCargo" character varying(50),
    "descTipoCargo" text
);


ALTER TABLE public."tipoCargo" OWNER TO postgres;

--
-- Name: tipoCargo_idTipoCargo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoCargo_idTipoCargo_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoCargo_idTipoCargo_seq" OWNER TO postgres;

--
-- Name: tipoCargo_idTipoCargo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoCargo_idTipoCargo_seq" OWNED BY "tipoCargo"."idTipoCargo";


--
-- Name: tipoComision; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoComision" (
    "idTipoComision" integer NOT NULL,
    "nombreTipoComision" character varying(50),
    "descTipoComision" text
);


ALTER TABLE public."tipoComision" OWNER TO postgres;

--
-- Name: tipoComision_idTipoComision_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoComision_idTipoComision_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoComision_idTipoComision_seq" OWNER TO postgres;

--
-- Name: tipoComision_idTipoComision_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoComision_idTipoComision_seq" OWNED BY "tipoComision"."idTipoComision";


--
-- Name: tipoEvento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoEvento" (
    "idTipoEvento" integer NOT NULL,
    "nombreTipoEvento" character varying(50),
    "descTipoEvento" text
);


ALTER TABLE public."tipoEvento" OWNER TO postgres;

--
-- Name: tipoEvento_idTipoEvento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoEvento_idTipoEvento_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoEvento_idTipoEvento_seq" OWNER TO postgres;

--
-- Name: tipoEvento_idTipoEvento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoEvento_idTipoEvento_seq" OWNED BY "tipoEvento"."idTipoEvento";


--
-- Name: tipoGenero; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoGenero" (
    "idTipoGenero" integer NOT NULL,
    "nombreTipoGenero" character varying(50),
    "descTipoGenero" text
);


ALTER TABLE public."tipoGenero" OWNER TO postgres;

--
-- Name: tipoGenero_idTipoGenero_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoGenero_idTipoGenero_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoGenero_idTipoGenero_seq" OWNER TO postgres;

--
-- Name: tipoGenero_idTipoGenero_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoGenero_idTipoGenero_seq" OWNED BY "tipoGenero"."idTipoGenero";


--
-- Name: tipoIncidencia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoIncidencia" (
    "idTipoIncidencia" integer NOT NULL,
    "nombreTipoIncidencia" character varying(50),
    "descTipoIncidencia" text
);


ALTER TABLE public."tipoIncidencia" OWNER TO postgres;

--
-- Name: tipoIncidencia_idTipoIncidencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoIncidencia_idTipoIncidencia_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoIncidencia_idTipoIncidencia_seq" OWNER TO postgres;

--
-- Name: tipoIncidencia_idTipoIncidencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoIncidencia_idTipoIncidencia_seq" OWNED BY "tipoIncidencia"."idTipoIncidencia";


--
-- Name: tipoIndicador; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoIndicador" (
    "idTipoindicador" integer NOT NULL,
    "nombreTipoindicador" character varying(50),
    "descTipoIndicador" text
);


ALTER TABLE public."tipoIndicador" OWNER TO postgres;

--
-- Name: tipoIndicador_idTipoindicador_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoIndicador_idTipoindicador_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoIndicador_idTipoindicador_seq" OWNER TO postgres;

--
-- Name: tipoIndicador_idTipoindicador_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoIndicador_idTipoindicador_seq" OWNED BY "tipoIndicador"."idTipoindicador";


--
-- Name: tipoNoticia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoNoticia" (
    "idTiponoticia" integer NOT NULL,
    "nombreTiponoticia" character varying(50),
    "descTiponoticia" text
);


ALTER TABLE public."tipoNoticia" OWNER TO postgres;

--
-- Name: tipoNoticia_idTiponoticia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoNoticia_idTiponoticia_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoNoticia_idTiponoticia_seq" OWNER TO postgres;

--
-- Name: tipoNoticia_idTiponoticia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoNoticia_idTiponoticia_seq" OWNED BY "tipoNoticia"."idTiponoticia";


--
-- Name: tipoOpinion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoOpinion" (
    "idTipoOpinion" integer NOT NULL,
    "nombreTipoOpinion" character varying(50),
    "descTipoOpinion" text
);


ALTER TABLE public."tipoOpinion" OWNER TO postgres;

--
-- Name: tipoOpinion_idTipoOpinion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoOpinion_idTipoOpinion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoOpinion_idTipoOpinion_seq" OWNER TO postgres;

--
-- Name: tipoOpinion_idTipoOpinion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoOpinion_idTipoOpinion_seq" OWNED BY "tipoOpinion"."idTipoOpinion";


--
-- Name: tipoParentesco; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoParentesco" (
    "idTipoParentesco" integer NOT NULL,
    "nombreTipoParen" character varying(50),
    "descTipoParentesco" text
);


ALTER TABLE public."tipoParentesco" OWNER TO postgres;

--
-- Name: tipoParentesco_idTipoParentesco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoParentesco_idTipoParentesco_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoParentesco_idTipoParentesco_seq" OWNER TO postgres;

--
-- Name: tipoParentesco_idTipoParentesco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoParentesco_idTipoParentesco_seq" OWNED BY "tipoParentesco"."idTipoParentesco";


--
-- Name: tipoPersona; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoPersona" (
    "idTipoPersona" integer NOT NULL,
    "nombreTipoPer" character varying(50),
    "descTipoPersona" text
);


ALTER TABLE public."tipoPersona" OWNER TO postgres;

--
-- Name: tipoPersona_idTipoPersona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoPersona_idTipoPersona_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoPersona_idTipoPersona_seq" OWNER TO postgres;

--
-- Name: tipoPersona_idTipoPersona_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoPersona_idTipoPersona_seq" OWNED BY "tipoPersona"."idTipoPersona";


--
-- Name: tipoPregunta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoPregunta" (
    "idTipopregunta" integer NOT NULL,
    "nombreTipopregunta" character varying(50),
    "descTipopregunta" text
);


ALTER TABLE public."tipoPregunta" OWNER TO postgres;

--
-- Name: tipoPregunta_idTipopregunta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoPregunta_idTipopregunta_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoPregunta_idTipopregunta_seq" OWNER TO postgres;

--
-- Name: tipoPregunta_idTipopregunta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoPregunta_idTipopregunta_seq" OWNED BY "tipoPregunta"."idTipopregunta";


--
-- Name: tipoPublico; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoPublico" (
    "idTipoPublico" integer NOT NULL,
    "nombreTipoPublico" character varying(50),
    "descTipoPublico" text
);


ALTER TABLE public."tipoPublico" OWNER TO postgres;

--
-- Name: tipoPublico_idTipoPublico_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoPublico_idTipoPublico_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoPublico_idTipoPublico_seq" OWNER TO postgres;

--
-- Name: tipoPublico_idTipoPublico_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoPublico_idTipoPublico_seq" OWNED BY "tipoPublico"."idTipoPublico";


--
-- Name: tipoRechazo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoRechazo" (
    "idTipoRechazo" integer NOT NULL,
    "nombreTipoRechazo" character varying(50),
    "descTipoRechazo" text
);


ALTER TABLE public."tipoRechazo" OWNER TO postgres;

--
-- Name: tipoRechazo_idTipoRechazo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoRechazo_idTipoRechazo_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoRechazo_idTipoRechazo_seq" OWNER TO postgres;

--
-- Name: tipoRechazo_idTipoRechazo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoRechazo_idTipoRechazo_seq" OWNED BY "tipoRechazo"."idTipoRechazo";


--
-- Name: tipoSancion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoSancion" (
    "idTipoSancion" integer NOT NULL,
    "nombreTipoSancion" character varying(50),
    "descTipoSancion" text
);


ALTER TABLE public."tipoSancion" OWNER TO postgres;

--
-- Name: tipoSancion_idTipoSancion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoSancion_idTipoSancion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoSancion_idTipoSancion_seq" OWNER TO postgres;

--
-- Name: tipoSancion_idTipoSancion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoSancion_idTipoSancion_seq" OWNED BY "tipoSancion"."idTipoSancion";


--
-- Name: tipoUnidadMedida; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "tipoUnidadMedida" (
    "idTipoUnidadMedida" integer NOT NULL,
    "nombreTipoUnidadM" character varying(50),
    "descTipoUnidad" text
);


ALTER TABLE public."tipoUnidadMedida" OWNER TO postgres;

--
-- Name: tipoUnidadMedida_idTipoUnidadMedida_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "tipoUnidadMedida_idTipoUnidadMedida_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."tipoUnidadMedida_idTipoUnidadMedida_seq" OWNER TO postgres;

--
-- Name: tipoUnidadMedida_idTipoUnidadMedida_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "tipoUnidadMedida_idTipoUnidadMedida_seq" OWNED BY "tipoUnidadMedida"."idTipoUnidadMedida";


--
-- Name: unidadMedida; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "unidadMedida" (
    "idUnidadMedida" integer NOT NULL,
    "nombreUnidadM" character varying(50),
    "descUnidadM" text,
    "idTipoUnidadMedida" integer NOT NULL
);


ALTER TABLE public."unidadMedida" OWNER TO postgres;

--
-- Name: unidadMedida_idUnidadMedida_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "unidadMedida_idUnidadMedida_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."unidadMedida_idUnidadMedida_seq" OWNER TO postgres;

--
-- Name: unidadMedida_idUnidadMedida_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "unidadMedida_idUnidadMedida_seq" OWNED BY "unidadMedida"."idUnidadMedida";


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    "idUsuario" integer NOT NULL,
    "nombreUsuario" character varying(50),
    contrasenna character varying(50),
    "preguntaSereta" character varying(100),
    "respuestaSecreta" character varying(100),
    "fechaUsuario" date,
    estado character varying(20),
    "idPersona" integer
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- Name: usuario_idUsuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "usuario_idUsuario_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."usuario_idUsuario_seq" OWNER TO postgres;

--
-- Name: usuario_idUsuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "usuario_idUsuario_seq" OWNED BY usuario."idUsuario";


--
-- Name: visita; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE visita (
    "idVisita" integer NOT NULL,
    "fechaVisita" date,
    "horaVisita" timestamp with time zone,
    "idPersona" integer NOT NULL
);


ALTER TABLE public.visita OWNER TO postgres;

--
-- Name: visita_idVisita_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "visita_idVisita_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."visita_idVisita_seq" OWNER TO postgres;

--
-- Name: visita_idVisita_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "visita_idVisita_seq" OWNED BY visita."idVisita";


--
-- Name: nroAccion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accion ALTER COLUMN "nroAccion" SET DEFAULT nextval('"accion_nroAccion_seq"'::regclass);


--
-- Name: idApelacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY apelacion ALTER COLUMN "idApelacion" SET DEFAULT nextval('"apelacion_idApelacion_seq"'::regclass);


--
-- Name: idArea; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area ALTER COLUMN "idArea" SET DEFAULT nextval('"area_idArea_seq"'::regclass);


--
-- Name: idArrendamiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY arrendamiento ALTER COLUMN "idArrendamiento" SET DEFAULT nextval('"arrendamiento_idArrendamiento_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "asistencia_Evento" ALTER COLUMN id SET DEFAULT nextval('"asistencia_Evento_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: idCargo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cargo ALTER COLUMN "idCargo" SET DEFAULT nextval('"cargo_idCargo_seq"'::regclass);


--
-- Name: idCategoria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria ALTER COLUMN "idCategoria" SET DEFAULT nextval('"categoria_idCategoria_seq"'::regclass);


--
-- Name: idClub; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY club ALTER COLUMN "idClub" SET DEFAULT nextval('"club_idClub_seq"'::regclass);


--
-- Name: idComision; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comision ALTER COLUMN "idComision" SET DEFAULT nextval('"comision_idComision_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY corsheaders_corsmodel ALTER COLUMN id SET DEFAULT nextval('corsheaders_corsmodel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: idEncuesta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY encuesta ALTER COLUMN "idEncuesta" SET DEFAULT nextval('"encuesta_idEncuesta_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "encuesta_Pregunta" ALTER COLUMN id SET DEFAULT nextval('"encuesta_Pregunta_id_seq"'::regclass);


--
-- Name: idEvento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento ALTER COLUMN "idEvento" SET DEFAULT nextval('"evento_idEvento_seq"'::regclass);


--
-- Name: idFuncion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY funcion ALTER COLUMN "idFuncion" SET DEFAULT nextval('"funcion_idFuncion_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "funcionGrupo" ALTER COLUMN id SET DEFAULT nextval('"funcionGrupo_id_seq"'::regclass);


--
-- Name: idGrupo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY grupo ALTER COLUMN "idGrupo" SET DEFAULT nextval('"grupo_idGrupo_seq"'::regclass);


--
-- Name: idIncidencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY incidencia ALTER COLUMN "idIncidencia" SET DEFAULT nextval('"incidencia_idIncidencia_seq"'::regclass);


--
-- Name: idIndicador; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicador ALTER COLUMN "idIndicador" SET DEFAULT nextval('"indicador_idIndicador_seq"'::regclass);


--
-- Name: idMorosidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY morosidad ALTER COLUMN "idMorosidad" SET DEFAULT nextval('"morosidad_idMorosidad_seq"'::regclass);


--
-- Name: idNoticia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noticia ALTER COLUMN "idNoticia" SET DEFAULT nextval('"noticia_idNoticia_seq"'::regclass);


--
-- Name: idOpcionpregunta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "opcionPregunta" ALTER COLUMN "idOpcionpregunta" SET DEFAULT nextval('"opcionPregunta_idOpcionpregunta_seq"'::regclass);


--
-- Name: idOpinion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY opinion ALTER COLUMN "idOpinion" SET DEFAULT nextval('"opinion_idOpinion_seq"'::regclass);


--
-- Name: idPersona; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona ALTER COLUMN "idPersona" SET DEFAULT nextval('"persona_idPersona_seq"'::regclass);


--
-- Name: idPregunta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pregunta ALTER COLUMN "idPregunta" SET DEFAULT nextval('"pregunta_idPregunta_seq"'::regclass);


--
-- Name: idRechazo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rechazo ALTER COLUMN "idRechazo" SET DEFAULT nextval('"rechazo_idRechazo_seq"'::regclass);


--
-- Name: idRecurso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recurso ALTER COLUMN "idRecurso" SET DEFAULT nextval('"recurso_idRecurso_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "recurso_Arrendamiento" ALTER COLUMN id SET DEFAULT nextval('"recurso_Arrendamiento_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "recurso_Evento" ALTER COLUMN id SET DEFAULT nextval('"recurso_Evento_id_seq"'::regclass);


--
-- Name: idReferencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY referencia ALTER COLUMN "idReferencia" SET DEFAULT nextval('"referencia_idReferencia_seq"'::regclass);


--
-- Name: idRespuestaencuesta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "respuestaEncuesta" ALTER COLUMN "idRespuestaencuesta" SET DEFAULT nextval('"respuestaEncuesta_idRespuestaencuesta_seq"'::regclass);


--
-- Name: idResultado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultado ALTER COLUMN "idResultado" SET DEFAULT nextval('"resultado_idResultado_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "resultado_Arrendamiento" ALTER COLUMN id SET DEFAULT nextval('"resultado_Arrendamiento_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "resultado_Evento" ALTER COLUMN id SET DEFAULT nextval('"resultado_Evento_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "resultado_Persona" ALTER COLUMN id SET DEFAULT nextval('"resultado_Persona_id_seq"'::regclass);


--
-- Name: idSancion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sancion ALTER COLUMN "idSancion" SET DEFAULT nextval('"sancion_idSancion_seq"'::regclass);


--
-- Name: idSolicitudAccion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "solicitudAccion" ALTER COLUMN "idSolicitudAccion" SET DEFAULT nextval('"solicitudAccion_idSolicitudAccion_seq"'::regclass);


--
-- Name: idSolicitudArrendamiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "solicitudArrendamiento" ALTER COLUMN "idSolicitudArrendamiento" SET DEFAULT nextval('"solicitudArrendamiento_idSolicitudArrendamiento_seq"'::regclass);


--
-- Name: idSolicitudEvento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "solicitudEvento" ALTER COLUMN "idSolicitudEvento" SET DEFAULT nextval('"solicitudEvento_idSolicitudEvento_seq"'::regclass);


--
-- Name: idTarea; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tarea ALTER COLUMN "idTarea" SET DEFAULT nextval('"tarea_idTarea_seq"'::regclass);


--
-- Name: idTipoArea; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoArea" ALTER COLUMN "idTipoArea" SET DEFAULT nextval('"tipoArea_idTipoArea_seq"'::regclass);


--
-- Name: idTipoCargo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoCargo" ALTER COLUMN "idTipoCargo" SET DEFAULT nextval('"tipoCargo_idTipoCargo_seq"'::regclass);


--
-- Name: idTipoComision; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoComision" ALTER COLUMN "idTipoComision" SET DEFAULT nextval('"tipoComision_idTipoComision_seq"'::regclass);


--
-- Name: idTipoEvento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoEvento" ALTER COLUMN "idTipoEvento" SET DEFAULT nextval('"tipoEvento_idTipoEvento_seq"'::regclass);


--
-- Name: idTipoGenero; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoGenero" ALTER COLUMN "idTipoGenero" SET DEFAULT nextval('"tipoGenero_idTipoGenero_seq"'::regclass);


--
-- Name: idTipoIncidencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoIncidencia" ALTER COLUMN "idTipoIncidencia" SET DEFAULT nextval('"tipoIncidencia_idTipoIncidencia_seq"'::regclass);


--
-- Name: idTipoindicador; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoIndicador" ALTER COLUMN "idTipoindicador" SET DEFAULT nextval('"tipoIndicador_idTipoindicador_seq"'::regclass);


--
-- Name: idTiponoticia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoNoticia" ALTER COLUMN "idTiponoticia" SET DEFAULT nextval('"tipoNoticia_idTiponoticia_seq"'::regclass);


--
-- Name: idTipoOpinion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoOpinion" ALTER COLUMN "idTipoOpinion" SET DEFAULT nextval('"tipoOpinion_idTipoOpinion_seq"'::regclass);


--
-- Name: idTipoParentesco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoParentesco" ALTER COLUMN "idTipoParentesco" SET DEFAULT nextval('"tipoParentesco_idTipoParentesco_seq"'::regclass);


--
-- Name: idTipoPersona; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoPersona" ALTER COLUMN "idTipoPersona" SET DEFAULT nextval('"tipoPersona_idTipoPersona_seq"'::regclass);


--
-- Name: idTipopregunta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoPregunta" ALTER COLUMN "idTipopregunta" SET DEFAULT nextval('"tipoPregunta_idTipopregunta_seq"'::regclass);


--
-- Name: idTipoPublico; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoPublico" ALTER COLUMN "idTipoPublico" SET DEFAULT nextval('"tipoPublico_idTipoPublico_seq"'::regclass);


--
-- Name: idTipoRechazo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoRechazo" ALTER COLUMN "idTipoRechazo" SET DEFAULT nextval('"tipoRechazo_idTipoRechazo_seq"'::regclass);


--
-- Name: idTipoSancion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoSancion" ALTER COLUMN "idTipoSancion" SET DEFAULT nextval('"tipoSancion_idTipoSancion_seq"'::regclass);


--
-- Name: idTipoUnidadMedida; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "tipoUnidadMedida" ALTER COLUMN "idTipoUnidadMedida" SET DEFAULT nextval('"tipoUnidadMedida_idTipoUnidadMedida_seq"'::regclass);


--
-- Name: idUnidadMedida; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "unidadMedida" ALTER COLUMN "idUnidadMedida" SET DEFAULT nextval('"unidadMedida_idUnidadMedida_seq"'::regclass);


--
-- Name: idUsuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN "idUsuario" SET DEFAULT nextval('"usuario_idUsuario_seq"'::regclass);


--
-- Name: idVisita; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visita ALTER COLUMN "idVisita" SET DEFAULT nextval('"visita_idVisita_seq"'::regclass);


--
-- Name: accion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accion
    ADD CONSTRAINT accion_pkey PRIMARY KEY ("nroAccion");


--
-- Name: apelacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY apelacion
    ADD CONSTRAINT apelacion_pkey PRIMARY KEY ("idApelacion");


--
-- Name: area_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY area
    ADD CONSTRAINT area_pkey PRIMARY KEY ("idArea");


--
-- Name: arrendamiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY arrendamiento
    ADD CONSTRAINT arrendamiento_pkey PRIMARY KEY ("idArrendamiento");


--
-- Name: asistencia_Evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "asistencia_Evento"
    ADD CONSTRAINT "asistencia_Evento_pkey" PRIMARY KEY (id);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: cargo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cargo
    ADD CONSTRAINT cargo_pkey PRIMARY KEY ("idCargo");


--
-- Name: categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pkey PRIMARY KEY ("idCategoria");


--
-- Name: club_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY club
    ADD CONSTRAINT club_pkey PRIMARY KEY ("idClub");


--
-- Name: comision_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY comision
    ADD CONSTRAINT comision_pkey PRIMARY KEY ("idComision");


--
-- Name: corsheaders_corsmodel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY corsheaders_corsmodel
    ADD CONSTRAINT corsheaders_corsmodel_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_45f3b1d93ec8c61c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_45f3b1d93ec8c61c_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: encuesta_Pregunta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "encuesta_Pregunta"
    ADD CONSTRAINT "encuesta_Pregunta_pkey" PRIMARY KEY (id);


--
-- Name: encuesta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY encuesta
    ADD CONSTRAINT encuesta_pkey PRIMARY KEY ("idEncuesta");


--
-- Name: evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT evento_pkey PRIMARY KEY ("idEvento");


--
-- Name: funcionGrupo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "funcionGrupo"
    ADD CONSTRAINT "funcionGrupo_pkey" PRIMARY KEY (id);


--
-- Name: funcion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY funcion
    ADD CONSTRAINT funcion_pkey PRIMARY KEY ("idFuncion");


--
-- Name: grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY grupo
    ADD CONSTRAINT grupo_pkey PRIMARY KEY ("idGrupo");


--
-- Name: incidencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY incidencia
    ADD CONSTRAINT incidencia_pkey PRIMARY KEY ("idIncidencia");


--
-- Name: indicador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY indicador
    ADD CONSTRAINT indicador_pkey PRIMARY KEY ("idIndicador");


--
-- Name: morosidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY morosidad
    ADD CONSTRAINT morosidad_pkey PRIMARY KEY ("idMorosidad");


--
-- Name: noticia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY noticia
    ADD CONSTRAINT noticia_pkey PRIMARY KEY ("idNoticia");


--
-- Name: opcionPregunta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "opcionPregunta"
    ADD CONSTRAINT "opcionPregunta_pkey" PRIMARY KEY ("idOpcionpregunta");


--
-- Name: opinion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY opinion
    ADD CONSTRAINT opinion_pkey PRIMARY KEY ("idOpinion");


--
-- Name: persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT persona_pkey PRIMARY KEY ("idPersona");


--
-- Name: pregunta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pregunta
    ADD CONSTRAINT pregunta_pkey PRIMARY KEY ("idPregunta");


--
-- Name: rechazo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rechazo
    ADD CONSTRAINT rechazo_pkey PRIMARY KEY ("idRechazo");


--
-- Name: recurso_Arrendamiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "recurso_Arrendamiento"
    ADD CONSTRAINT "recurso_Arrendamiento_pkey" PRIMARY KEY (id);


--
-- Name: recurso_Evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "recurso_Evento"
    ADD CONSTRAINT "recurso_Evento_pkey" PRIMARY KEY (id);


--
-- Name: recurso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY recurso
    ADD CONSTRAINT recurso_pkey PRIMARY KEY ("idRecurso");


--
-- Name: referencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY referencia
    ADD CONSTRAINT referencia_pkey PRIMARY KEY ("idReferencia");


--
-- Name: respuestaEncuesta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "respuestaEncuesta"
    ADD CONSTRAINT "respuestaEncuesta_pkey" PRIMARY KEY ("idRespuestaencuesta");


--
-- Name: resultado_Arrendamiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "resultado_Arrendamiento"
    ADD CONSTRAINT "resultado_Arrendamiento_pkey" PRIMARY KEY (id);


--
-- Name: resultado_Evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "resultado_Evento"
    ADD CONSTRAINT "resultado_Evento_pkey" PRIMARY KEY (id);


--
-- Name: resultado_Persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "resultado_Persona"
    ADD CONSTRAINT "resultado_Persona_pkey" PRIMARY KEY (id);


--
-- Name: resultado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY resultado
    ADD CONSTRAINT resultado_pkey PRIMARY KEY ("idResultado");


--
-- Name: sancion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sancion
    ADD CONSTRAINT sancion_pkey PRIMARY KEY ("idSancion");


--
-- Name: solicitudAccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "solicitudAccion"
    ADD CONSTRAINT "solicitudAccion_pkey" PRIMARY KEY ("idSolicitudAccion");


--
-- Name: solicitudArrendamiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "solicitudArrendamiento"
    ADD CONSTRAINT "solicitudArrendamiento_pkey" PRIMARY KEY ("idSolicitudArrendamiento");


--
-- Name: solicitudEvento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "solicitudEvento"
    ADD CONSTRAINT "solicitudEvento_pkey" PRIMARY KEY ("idSolicitudEvento");


--
-- Name: tarea_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tarea
    ADD CONSTRAINT tarea_pkey PRIMARY KEY ("idTarea");


--
-- Name: tipoArea_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoArea"
    ADD CONSTRAINT "tipoArea_pkey" PRIMARY KEY ("idTipoArea");


--
-- Name: tipoCargo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoCargo"
    ADD CONSTRAINT "tipoCargo_pkey" PRIMARY KEY ("idTipoCargo");


--
-- Name: tipoComision_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoComision"
    ADD CONSTRAINT "tipoComision_pkey" PRIMARY KEY ("idTipoComision");


--
-- Name: tipoEvento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoEvento"
    ADD CONSTRAINT "tipoEvento_pkey" PRIMARY KEY ("idTipoEvento");


--
-- Name: tipoGenero_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoGenero"
    ADD CONSTRAINT "tipoGenero_pkey" PRIMARY KEY ("idTipoGenero");


--
-- Name: tipoIncidencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoIncidencia"
    ADD CONSTRAINT "tipoIncidencia_pkey" PRIMARY KEY ("idTipoIncidencia");


--
-- Name: tipoIndicador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoIndicador"
    ADD CONSTRAINT "tipoIndicador_pkey" PRIMARY KEY ("idTipoindicador");


--
-- Name: tipoNoticia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoNoticia"
    ADD CONSTRAINT "tipoNoticia_pkey" PRIMARY KEY ("idTiponoticia");


--
-- Name: tipoOpinion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoOpinion"
    ADD CONSTRAINT "tipoOpinion_pkey" PRIMARY KEY ("idTipoOpinion");


--
-- Name: tipoParentesco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoParentesco"
    ADD CONSTRAINT "tipoParentesco_pkey" PRIMARY KEY ("idTipoParentesco");


--
-- Name: tipoPersona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoPersona"
    ADD CONSTRAINT "tipoPersona_pkey" PRIMARY KEY ("idTipoPersona");


--
-- Name: tipoPregunta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoPregunta"
    ADD CONSTRAINT "tipoPregunta_pkey" PRIMARY KEY ("idTipopregunta");


--
-- Name: tipoPublico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoPublico"
    ADD CONSTRAINT "tipoPublico_pkey" PRIMARY KEY ("idTipoPublico");


--
-- Name: tipoRechazo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoRechazo"
    ADD CONSTRAINT "tipoRechazo_pkey" PRIMARY KEY ("idTipoRechazo");


--
-- Name: tipoSancion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoSancion"
    ADD CONSTRAINT "tipoSancion_pkey" PRIMARY KEY ("idTipoSancion");


--
-- Name: tipoUnidadMedida_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tipoUnidadMedida"
    ADD CONSTRAINT "tipoUnidadMedida_pkey" PRIMARY KEY ("idTipoUnidadMedida");


--
-- Name: unidadMedida_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "unidadMedida"
    ADD CONSTRAINT "unidadMedida_pkey" PRIMARY KEY ("idUnidadMedida");


--
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY ("idUsuario");


--
-- Name: visita_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT visita_pkey PRIMARY KEY ("idVisita");


--
-- Name: accion_12f762d8; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX accion_12f762d8 ON accion USING btree ("idSolicitudAccion");


--
-- Name: accion_806825b0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX accion_806825b0 ON accion USING btree ("idClub");


--
-- Name: accion_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX accion_f0681536 ON accion USING btree ("idPersona");


--
-- Name: apelacion_67646d8f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX apelacion_67646d8f ON apelacion USING btree ("idSancion");


--
-- Name: apelacion_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX apelacion_f0681536 ON apelacion USING btree ("idPersona");


--
-- Name: area_806825b0; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX area_806825b0 ON area USING btree ("idClub");


--
-- Name: area_c9666bed; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX area_c9666bed ON area USING btree ("idTipoArea");


--
-- Name: arrendamiento_d77b5d12; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX arrendamiento_d77b5d12 ON arrendamiento USING btree ("idSolicitudArrendamiento");


--
-- Name: asistencia_Evento_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "asistencia_Evento_1ed26592" ON "asistencia_Evento" USING btree ("idEvento");


--
-- Name: asistencia_Evento_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "asistencia_Evento_f0681536" ON "asistencia_Evento" USING btree ("idPersona");


--
-- Name: auth_group_name_253ae2a6331666e8_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_name_253ae2a6331666e8_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_51b3b110094b8aae_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX auth_user_username_51b3b110094b8aae_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: cargo_c4ace542; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX cargo_c4ace542 ON cargo USING btree ("idTipoPersona");


--
-- Name: club_3cfd71b3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX club_3cfd71b3 ON club USING btree ("idUsuario");


--
-- Name: comision_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX comision_1ed26592 ON comision USING btree ("idEvento");


--
-- Name: comision_3bb0cd9f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX comision_3bb0cd9f ON comision USING btree ("idTipoComision");


--
-- Name: comision_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX comision_f0681536 ON comision USING btree ("idPersona");


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_461cfeaa630ca218_like; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX django_session_session_key_461cfeaa630ca218_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: encuesta_Pregunta_3a6bebd1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "encuesta_Pregunta_3a6bebd1" ON "encuesta_Pregunta" USING btree ("idPregunta");


--
-- Name: encuesta_Pregunta_f4db176b; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "encuesta_Pregunta_f4db176b" ON "encuesta_Pregunta" USING btree ("idEncuesta");


--
-- Name: evento_fc85bd02; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX evento_fc85bd02 ON evento USING btree ("idSolicitudEvento");


--
-- Name: funcionGrupo_a3c64682; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "funcionGrupo_a3c64682" ON "funcionGrupo" USING btree ("idFuncion");


--
-- Name: funcionGrupo_d5f7a198; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "funcionGrupo_d5f7a198" ON "funcionGrupo" USING btree ("idGrupo");


--
-- Name: incidencia_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX incidencia_1ed26592 ON incidencia USING btree ("idEvento");


--
-- Name: incidencia_3c086f6c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX incidencia_3c086f6c ON incidencia USING btree ("idArrendamiento");


--
-- Name: incidencia_878a66d3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX incidencia_878a66d3 ON incidencia USING btree ("idTipoIncidencia");


--
-- Name: incidencia_bfde36d4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX incidencia_bfde36d4 ON incidencia USING btree ("idVisita");


--
-- Name: incidencia_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX incidencia_f0681536 ON incidencia USING btree ("idPersona");


--
-- Name: indicador_09dd2faf; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX indicador_09dd2faf ON indicador USING btree ("idTipoindicador");


--
-- Name: indicador_81933ff2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX indicador_81933ff2 ON indicador USING btree ("idUnidadMedida");


--
-- Name: morosidad_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX morosidad_f0681536 ON morosidad USING btree ("idPersona");


--
-- Name: noticia_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX noticia_1ed26592 ON noticia USING btree ("idEvento");


--
-- Name: noticia_2c2b8547; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX noticia_2c2b8547 ON noticia USING btree ("idTiponoticia");


--
-- Name: opcionPregunta_3a6bebd1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "opcionPregunta_3a6bebd1" ON "opcionPregunta" USING btree ("idPregunta");


--
-- Name: opinion_3cfd71b3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX opinion_3cfd71b3 ON opinion USING btree ("idUsuario");


--
-- Name: opinion_826503b5; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX opinion_826503b5 ON opinion USING btree ("idTipoOpinion");


--
-- Name: opinion_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX opinion_f0681536 ON opinion USING btree ("idPersona");


--
-- Name: persona_01ec234f; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX persona_01ec234f ON persona USING btree ("idCargo");


--
-- Name: persona_a19b22b1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX persona_a19b22b1 ON persona USING btree ("idTipoParentesco");


--
-- Name: persona_c4ace542; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX persona_c4ace542 ON persona USING btree ("idTipoPersona");


--
-- Name: pregunta_5ea0e3e4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX pregunta_5ea0e3e4 ON pregunta USING btree ("idCategoria");


--
-- Name: pregunta_d1532948; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX pregunta_d1532948 ON pregunta USING btree ("idTipopregunta");


--
-- Name: rechazo_12f762d8; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rechazo_12f762d8 ON rechazo USING btree ("idSolicitudAccion");


--
-- Name: rechazo_18a8e5bd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rechazo_18a8e5bd ON rechazo USING btree ("idApelacion");


--
-- Name: rechazo_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rechazo_1ed26592 ON rechazo USING btree ("idEvento");


--
-- Name: rechazo_c74696e7; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rechazo_c74696e7 ON rechazo USING btree ("idTipoRechazo");


--
-- Name: rechazo_d77b5d12; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rechazo_d77b5d12 ON rechazo USING btree ("idSolicitudArrendamiento");


--
-- Name: recurso_Arrendamiento_296b207b; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "recurso_Arrendamiento_296b207b" ON "recurso_Arrendamiento" USING btree ("idRecurso");


--
-- Name: recurso_Arrendamiento_3c086f6c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "recurso_Arrendamiento_3c086f6c" ON "recurso_Arrendamiento" USING btree ("idArrendamiento");


--
-- Name: recurso_Evento_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "recurso_Evento_1ed26592" ON "recurso_Evento" USING btree ("idEvento");


--
-- Name: recurso_Evento_296b207b; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "recurso_Evento_296b207b" ON "recurso_Evento" USING btree ("idRecurso");


--
-- Name: referencia_12f762d8; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX referencia_12f762d8 ON referencia USING btree ("idSolicitudAccion");


--
-- Name: respuestaEncuesta_3cfd71b3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "respuestaEncuesta_3cfd71b3" ON "respuestaEncuesta" USING btree ("idUsuario");


--
-- Name: respuestaEncuesta_3d08b03b; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "respuestaEncuesta_3d08b03b" ON "respuestaEncuesta" USING btree ("idOpcionpregunta");


--
-- Name: resultado_Arrendamiento_1a13d2cd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "resultado_Arrendamiento_1a13d2cd" ON "resultado_Arrendamiento" USING btree ("idResultado");


--
-- Name: resultado_Arrendamiento_3c086f6c; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "resultado_Arrendamiento_3c086f6c" ON "resultado_Arrendamiento" USING btree ("idArrendamiento");


--
-- Name: resultado_Evento_1a13d2cd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "resultado_Evento_1a13d2cd" ON "resultado_Evento" USING btree ("idResultado");


--
-- Name: resultado_Evento_1ed26592; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "resultado_Evento_1ed26592" ON "resultado_Evento" USING btree ("idEvento");


--
-- Name: resultado_Persona_1a13d2cd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "resultado_Persona_1a13d2cd" ON "resultado_Persona" USING btree ("idResultado");


--
-- Name: resultado_Persona_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "resultado_Persona_f0681536" ON "resultado_Persona" USING btree ("idPersona");


--
-- Name: resultado_c78f0797; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX resultado_c78f0797 ON resultado USING btree ("idIndicador");


--
-- Name: sancion_07dcffe6; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sancion_07dcffe6 ON sancion USING btree ("idTipoSancion");


--
-- Name: sancion_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sancion_f0681536 ON sancion USING btree ("idPersona");


--
-- Name: sancion_f195edd1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sancion_f195edd1 ON sancion USING btree ("idIncidencia");


--
-- Name: solicitudAccion_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "solicitudAccion_f0681536" ON "solicitudAccion" USING btree ("idPersona");


--
-- Name: solicitudArrendamiento_88e3c0d7; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "solicitudArrendamiento_88e3c0d7" ON "solicitudArrendamiento" USING btree ("idArea");


--
-- Name: solicitudArrendamiento_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "solicitudArrendamiento_f0681536" ON "solicitudArrendamiento" USING btree ("idPersona");


--
-- Name: solicitudEvento_11f759d2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "solicitudEvento_11f759d2" ON "solicitudEvento" USING btree ("idTipoGenero");


--
-- Name: solicitudEvento_248d39f2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "solicitudEvento_248d39f2" ON "solicitudEvento" USING btree ("idTipoPublico");


--
-- Name: solicitudEvento_2981c086; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "solicitudEvento_2981c086" ON "solicitudEvento" USING btree ("idTipoEvento");


--
-- Name: solicitudEvento_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "solicitudEvento_f0681536" ON "solicitudEvento" USING btree ("idPersona");


--
-- Name: unidadMedida_1f04dcf3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "unidadMedida_1f04dcf3" ON "unidadMedida" USING btree ("idTipoUnidadMedida");


--
-- Name: usuario_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX usuario_f0681536 ON usuario USING btree ("idPersona");


--
-- Name: visita_f0681536; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visita_f0681536 ON visita USING btree ("idPersona");


--
-- Name: D099fb004808a7b82f8126e34fdc5dfb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "unidadMedida"
    ADD CONSTRAINT "D099fb004808a7b82f8126e34fdc5dfb" FOREIGN KEY ("idTipoUnidadMedida") REFERENCES "tipoUnidadMedida"("idTipoUnidadMedida") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D0b246455a683aeeeacf6a8d2ea22bee; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accion
    ADD CONSTRAINT "D0b246455a683aeeeacf6a8d2ea22bee" FOREIGN KEY ("idSolicitudAccion") REFERENCES "solicitudAccion"("idSolicitudAccion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D0caa7080a1a73114776bdafe8f2661f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rechazo
    ADD CONSTRAINT "D0caa7080a1a73114776bdafe8f2661f" FOREIGN KEY ("idSolicitudAccion") REFERENCES "solicitudAccion"("idSolicitudAccion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D15b4bca0be0d7187a32396ee79a8144; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY referencia
    ADD CONSTRAINT "D15b4bca0be0d7187a32396ee79a8144" FOREIGN KEY ("idSolicitudAccion") REFERENCES "solicitudAccion"("idSolicitudAccion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D56a7fa8f62ec257ff915f116f0ccf16; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "respuestaEncuesta"
    ADD CONSTRAINT "D56a7fa8f62ec257ff915f116f0ccf16" FOREIGN KEY ("idOpcionpregunta") REFERENCES "opcionPregunta"("idOpcionpregunta") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D5cbd42db1ce0eed616603d96825f919; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT "D5cbd42db1ce0eed616603d96825f919" FOREIGN KEY ("idSolicitudEvento") REFERENCES "solicitudEvento"("idSolicitudEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D892476238fb6dd1e0a4d74fb0a41eed; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY incidencia
    ADD CONSTRAINT "D892476238fb6dd1e0a4d74fb0a41eed" FOREIGN KEY ("idTipoIncidencia") REFERENCES "tipoIncidencia"("idTipoIncidencia") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D8cc468f7c0c85289ae67c33968228ea; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicador
    ADD CONSTRAINT "D8cc468f7c0c85289ae67c33968228ea" FOREIGN KEY ("idTipoindicador") REFERENCES "tipoIndicador"("idTipoindicador") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D99d84644ab48ca9b34e683d058352a5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY arrendamiento
    ADD CONSTRAINT "D99d84644ab48ca9b34e683d058352a5" FOREIGN KEY ("idSolicitudArrendamiento") REFERENCES "solicitudArrendamiento"("idSolicitudArrendamiento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: accion_idClub_338f68c8c7cdf8a7_fk_club_idClub; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accion
    ADD CONSTRAINT "accion_idClub_338f68c8c7cdf8a7_fk_club_idClub" FOREIGN KEY ("idClub") REFERENCES club("idClub") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: accion_idPersona_1b5140f94a713866_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accion
    ADD CONSTRAINT "accion_idPersona_1b5140f94a713866_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aebcf063528ce9f78343a393106f5ccb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT aebcf063528ce9f78343a393106f5ccb FOREIGN KEY ("idTipoParentesco") REFERENCES "tipoParentesco"("idTipoParentesco") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: apelacion_idPersona_5f528ad25590b7c8_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY apelacion
    ADD CONSTRAINT "apelacion_idPersona_5f528ad25590b7c8_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: apelacion_idSancion_667245386b53f651_fk_sancion_idSancion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY apelacion
    ADD CONSTRAINT "apelacion_idSancion_667245386b53f651_fk_sancion_idSancion" FOREIGN KEY ("idSancion") REFERENCES sancion("idSancion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: area_idClub_7e8a13e9d26d8c39_fk_club_idClub; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area
    ADD CONSTRAINT "area_idClub_7e8a13e9d26d8c39_fk_club_idClub" FOREIGN KEY ("idClub") REFERENCES club("idClub") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: area_idTipoArea_5e61371363e62720_fk_tipoArea_idTipoArea; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area
    ADD CONSTRAINT "area_idTipoArea_5e61371363e62720_fk_tipoArea_idTipoArea" FOREIGN KEY ("idTipoArea") REFERENCES "tipoArea"("idTipoArea") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: asistencia_Even_idPersona_1c627fcd1ef82938_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "asistencia_Evento"
    ADD CONSTRAINT "asistencia_Even_idPersona_1c627fcd1ef82938_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: asistencia_Evento_idEvento_400d50869991f38_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "asistencia_Evento"
    ADD CONSTRAINT "asistencia_Evento_idEvento_400d50869991f38_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_content_type_id_508cf46651277a81_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_content_type_id_508cf46651277a81_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: b751b26933db855c050dc64a6b9ea98a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rechazo
    ADD CONSTRAINT b751b26933db855c050dc64a6b9ea98a FOREIGN KEY ("idSolicitudArrendamiento") REFERENCES "solicitudArrendamiento"("idSolicitudArrendamiento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: c_idTipoComision_7bb1218a9d31384_fk_tipoComision_idTipoComision; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comision
    ADD CONSTRAINT "c_idTipoComision_7bb1218a9d31384_fk_tipoComision_idTipoComision" FOREIGN KEY ("idTipoComision") REFERENCES "tipoComision"("idTipoComision") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: car_idTipoPersona_3053ee0a98da419c_fk_tipoPersona_idTipoPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cargo
    ADD CONSTRAINT "car_idTipoPersona_3053ee0a98da419c_fk_tipoPersona_idTipoPersona" FOREIGN KEY ("idTipoPersona") REFERENCES "tipoPersona"("idTipoPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: club_idUsuario_31d963289104f451_fk_usuario_idUsuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY club
    ADD CONSTRAINT "club_idUsuario_31d963289104f451_fk_usuario_idUsuario" FOREIGN KEY ("idUsuario") REFERENCES usuario("idUsuario") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comision_idEvento_4ee5f2849ac6f834_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comision
    ADD CONSTRAINT "comision_idEvento_4ee5f2849ac6f834_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: comision_idPersona_6c59e498bbd8cc24_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comision
    ADD CONSTRAINT "comision_idPersona_6c59e498bbd8cc24_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: d3de99c5793313c18352fac3836f0383; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "resultado_Arrendamiento"
    ADD CONSTRAINT d3de99c5793313c18352fac3836f0383 FOREIGN KEY ("idArrendamiento") REFERENCES arrendamiento("idArrendamiento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djan_content_type_id_697914295151027a_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT djan_content_type_id_697914295151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: encuesta_Pre_idPregunta_32aaef25e3636eea_fk_pregunta_idPregunta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "encuesta_Pregunta"
    ADD CONSTRAINT "encuesta_Pre_idPregunta_32aaef25e3636eea_fk_pregunta_idPregunta" FOREIGN KEY ("idPregunta") REFERENCES pregunta("idPregunta") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: encuesta_Preg_idEncuesta_c936faa81ac27c8_fk_encuesta_idEncuesta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "encuesta_Pregunta"
    ADD CONSTRAINT "encuesta_Preg_idEncuesta_c936faa81ac27c8_fk_encuesta_idEncuesta" FOREIGN KEY ("idEncuesta") REFERENCES encuesta("idEncuesta") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: f28d575cee6498efc99262b6b00c26a3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY incidencia
    ADD CONSTRAINT f28d575cee6498efc99262b6b00c26a3 FOREIGN KEY ("idArrendamiento") REFERENCES arrendamiento("idArrendamiento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: f4a20c38684e5cd6cfcd7b2625f51acf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "recurso_Arrendamiento"
    ADD CONSTRAINT f4a20c38684e5cd6cfcd7b2625f51acf FOREIGN KEY ("idArrendamiento") REFERENCES arrendamiento("idArrendamiento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: funcionGrupo_idFuncion_5d06651055d78770_fk_funcion_idFuncion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "funcionGrupo"
    ADD CONSTRAINT "funcionGrupo_idFuncion_5d06651055d78770_fk_funcion_idFuncion" FOREIGN KEY ("idFuncion") REFERENCES funcion("idFuncion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: funcionGrupo_idGrupo_5e5d23e27077de0d_fk_grupo_idGrupo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "funcionGrupo"
    ADD CONSTRAINT "funcionGrupo_idGrupo_5e5d23e27077de0d_fk_grupo_idGrupo" FOREIGN KEY ("idGrupo") REFERENCES grupo("idGrupo") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: idTipopregunta_25a6cb9615f59586_fk_tipoPregunta_idTipopregunta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pregunta
    ADD CONSTRAINT "idTipopregunta_25a6cb9615f59586_fk_tipoPregunta_idTipopregunta" FOREIGN KEY ("idTipopregunta") REFERENCES "tipoPregunta"("idTipopregunta") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: idUnidadMedida_5a929e86b8f3bd45_fk_unidadMedida_idUnidadMedida; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicador
    ADD CONSTRAINT "idUnidadMedida_5a929e86b8f3bd45_fk_unidadMedida_idUnidadMedida" FOREIGN KEY ("idUnidadMedida") REFERENCES "unidadMedida"("idUnidadMedida") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: incidencia_idEvento_4819872e4ed09386_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY incidencia
    ADD CONSTRAINT "incidencia_idEvento_4819872e4ed09386_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: incidencia_idPersona_6e97f13e3306776_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY incidencia
    ADD CONSTRAINT "incidencia_idPersona_6e97f13e3306776_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: incidencia_idVisita_17d9cc040294a4f7_fk_visita_idVisita; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY incidencia
    ADD CONSTRAINT "incidencia_idVisita_17d9cc040294a4f7_fk_visita_idVisita" FOREIGN KEY ("idVisita") REFERENCES visita("idVisita") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: morosidad_idPersona_310bd577d74a613e_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY morosidad
    ADD CONSTRAINT "morosidad_idPersona_310bd577d74a613e_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: not_idTiponoticia_3599035cc2c0972c_fk_tipoNoticia_idTiponoticia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noticia
    ADD CONSTRAINT "not_idTiponoticia_3599035cc2c0972c_fk_tipoNoticia_idTiponoticia" FOREIGN KEY ("idTiponoticia") REFERENCES "tipoNoticia"("idTiponoticia") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: noticia_idEvento_3c44879e8b557625_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noticia
    ADD CONSTRAINT "noticia_idEvento_3c44879e8b557625_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: opcionPregun_idPregunta_29f485b31a3e00f8_fk_pregunta_idPregunta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "opcionPregunta"
    ADD CONSTRAINT "opcionPregun_idPregunta_29f485b31a3e00f8_fk_pregunta_idPregunta" FOREIGN KEY ("idPregunta") REFERENCES pregunta("idPregunta") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: opi_idTipoOpinion_4c56eff427ed4192_fk_tipoOpinion_idTipoOpinion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY opinion
    ADD CONSTRAINT "opi_idTipoOpinion_4c56eff427ed4192_fk_tipoOpinion_idTipoOpinion" FOREIGN KEY ("idTipoOpinion") REFERENCES "tipoOpinion"("idTipoOpinion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: opinion_idPersona_6ac9b8c0220eecfe_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY opinion
    ADD CONSTRAINT "opinion_idPersona_6ac9b8c0220eecfe_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: opinion_idUsuario_ecf8b73bd01efea_fk_usuario_idUsuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY opinion
    ADD CONSTRAINT "opinion_idUsuario_ecf8b73bd01efea_fk_usuario_idUsuario" FOREIGN KEY ("idUsuario") REFERENCES usuario("idUsuario") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: per_idTipoPersona_631c7fb05c2ab3ba_fk_tipoPersona_idTipoPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT "per_idTipoPersona_631c7fb05c2ab3ba_fk_tipoPersona_idTipoPersona" FOREIGN KEY ("idTipoPersona") REFERENCES "tipoPersona"("idTipoPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: persona_idCargo_23f16a104c0d239e_fk_cargo_idCargo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT "persona_idCargo_23f16a104c0d239e_fk_cargo_idCargo" FOREIGN KEY ("idCargo") REFERENCES cargo("idCargo") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pregunta_idCategoria_7cf896c4a859c7e8_fk_categoria_idCategoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pregunta
    ADD CONSTRAINT "pregunta_idCategoria_7cf896c4a859c7e8_fk_categoria_idCategoria" FOREIGN KEY ("idCategoria") REFERENCES categoria("idCategoria") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rec_idTipoRechazo_235dc57ee6f78f52_fk_tipoRechazo_idTipoRechazo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rechazo
    ADD CONSTRAINT "rec_idTipoRechazo_235dc57ee6f78f52_fk_tipoRechazo_idTipoRechazo" FOREIGN KEY ("idTipoRechazo") REFERENCES "tipoRechazo"("idTipoRechazo") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rechazo_idApelacion_3ba192eb965ed1fa_fk_apelacion_idApelacion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rechazo
    ADD CONSTRAINT "rechazo_idApelacion_3ba192eb965ed1fa_fk_apelacion_idApelacion" FOREIGN KEY ("idApelacion") REFERENCES apelacion("idApelacion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rechazo_idEvento_51fdef1ed318fac6_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rechazo
    ADD CONSTRAINT "rechazo_idEvento_51fdef1ed318fac6_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: recurso_Arrenda_idRecurso_65be109477ecaadc_fk_recurso_idRecurso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "recurso_Arrendamiento"
    ADD CONSTRAINT "recurso_Arrenda_idRecurso_65be109477ecaadc_fk_recurso_idRecurso" FOREIGN KEY ("idRecurso") REFERENCES recurso("idRecurso") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: recurso_Evento_idEvento_11f0c7fa5d0ca0de_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "recurso_Evento"
    ADD CONSTRAINT "recurso_Evento_idEvento_11f0c7fa5d0ca0de_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: recurso_Evento_idRecurso_603390891c1e17ab_fk_recurso_idRecurso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "recurso_Evento"
    ADD CONSTRAINT "recurso_Evento_idRecurso_603390891c1e17ab_fk_recurso_idRecurso" FOREIGN KEY ("idRecurso") REFERENCES recurso("idRecurso") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: respuestaEncues_idUsuario_1fd52516650094cc_fk_usuario_idUsuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "respuestaEncuesta"
    ADD CONSTRAINT "respuestaEncues_idUsuario_1fd52516650094cc_fk_usuario_idUsuario" FOREIGN KEY ("idUsuario") REFERENCES usuario("idUsuario") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: resultado_Evento_idEvento_15a5058e77d17ce2_fk_evento_idEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "resultado_Evento"
    ADD CONSTRAINT "resultado_Evento_idEvento_15a5058e77d17ce2_fk_evento_idEvento" FOREIGN KEY ("idEvento") REFERENCES evento("idEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: resultado_Perso_idPersona_2b5ba72f54952b84_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "resultado_Persona"
    ADD CONSTRAINT "resultado_Perso_idPersona_2b5ba72f54952b84_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: resultado_idIndicador_79964e27159506a6_fk_indicador_idIndicador; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY resultado
    ADD CONSTRAINT "resultado_idIndicador_79964e27159506a6_fk_indicador_idIndicador" FOREIGN KEY ("idIndicador") REFERENCES indicador("idIndicador") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: resultado_idResultado_416afb3e005a5a61_fk_resultado_idResultado; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "resultado_Evento"
    ADD CONSTRAINT "resultado_idResultado_416afb3e005a5a61_fk_resultado_idResultado" FOREIGN KEY ("idResultado") REFERENCES resultado("idResultado") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: resultado_idResultado_53795661a995b51c_fk_resultado_idResultado; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "resultado_Arrendamiento"
    ADD CONSTRAINT "resultado_idResultado_53795661a995b51c_fk_resultado_idResultado" FOREIGN KEY ("idResultado") REFERENCES resultado("idResultado") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: resultado_idResultado_66a98c13565722c5_fk_resultado_idResultado; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "resultado_Persona"
    ADD CONSTRAINT "resultado_idResultado_66a98c13565722c5_fk_resultado_idResultado" FOREIGN KEY ("idResultado") REFERENCES resultado("idResultado") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: san_idTipoSancion_1e8b92967bad2588_fk_tipoSancion_idTipoSancion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sancion
    ADD CONSTRAINT "san_idTipoSancion_1e8b92967bad2588_fk_tipoSancion_idTipoSancion" FOREIGN KEY ("idTipoSancion") REFERENCES "tipoSancion"("idTipoSancion") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sancio_idIncidencia_5b006785a4f891a7_fk_incidencia_idIncidencia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sancion
    ADD CONSTRAINT "sancio_idIncidencia_5b006785a4f891a7_fk_incidencia_idIncidencia" FOREIGN KEY ("idIncidencia") REFERENCES incidencia("idIncidencia") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sancion_idPersona_55ec8b6af851517_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sancion
    ADD CONSTRAINT "sancion_idPersona_55ec8b6af851517_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sol_idTipoPublico_4761d717bf05c1ab_fk_tipoPublico_idTipoPublico; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "solicitudEvento"
    ADD CONSTRAINT "sol_idTipoPublico_4761d717bf05c1ab_fk_tipoPublico_idTipoPublico" FOREIGN KEY ("idTipoPublico") REFERENCES "tipoPublico"("idTipoPublico") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: solici_idTipoEvento_2f05c59ab25c131f_fk_tipoEvento_idTipoEvento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "solicitudEvento"
    ADD CONSTRAINT "solici_idTipoEvento_2f05c59ab25c131f_fk_tipoEvento_idTipoEvento" FOREIGN KEY ("idTipoEvento") REFERENCES "tipoEvento"("idTipoEvento") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: solici_idTipoGenero_7cf56dfe0207f5e8_fk_tipoGenero_idTipoGenero; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "solicitudEvento"
    ADD CONSTRAINT "solici_idTipoGenero_7cf56dfe0207f5e8_fk_tipoGenero_idTipoGenero" FOREIGN KEY ("idTipoGenero") REFERENCES "tipoGenero"("idTipoGenero") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: solicitudAccion_idPersona_612348d1d18690cb_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "solicitudAccion"
    ADD CONSTRAINT "solicitudAccion_idPersona_612348d1d18690cb_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: solicitudArrend_idPersona_49f5c08f004a0d96_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "solicitudArrendamiento"
    ADD CONSTRAINT "solicitudArrend_idPersona_49f5c08f004a0d96_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: solicitudArrendamiento_idArea_8a46cecf1631f20_fk_area_idArea; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "solicitudArrendamiento"
    ADD CONSTRAINT "solicitudArrendamiento_idArea_8a46cecf1631f20_fk_area_idArea" FOREIGN KEY ("idArea") REFERENCES area("idArea") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: solicitudEvento_idPersona_4b16c3654b5d0a75_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "solicitudEvento"
    ADD CONSTRAINT "solicitudEvento_idPersona_4b16c3654b5d0a75_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuario_idPersona_24d60ce8e14c02ec_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT "usuario_idPersona_24d60ce8e14c02ec_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: visita_idPersona_67be1c087d4870e7_fk_persona_idPersona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visita
    ADD CONSTRAINT "visita_idPersona_67be1c087d4870e7_fk_persona_idPersona" FOREIGN KEY ("idPersona") REFERENCES persona("idPersona") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

