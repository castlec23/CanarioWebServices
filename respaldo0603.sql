﻿--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.11
-- Dumped by pg_dump version 9.3.11
-- Started on 2016-03-07 04:17:22 VET


--
-- TOC entry 2438 (class 0 OID 21099)
-- Dependencies: 183
-- Data for Name: cargo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO cargo (idcargo, desccargo, editingstatus, estadocargo, nombrecargo) VALUES (1, 'Implica el liderazgo máximo dentro de la Junta Directiva del Club Hogar Canario Larense', false, 'A', 'Presidente');
INSERT INTO cargo (idcargo, desccargo, editingstatus, estadocargo, nombrecargo) VALUES (2, 'Presta su servicio en la Junta Directiva y lograr la realizacion de los proyectos especiales que asigne el presidente. Ocupará y desempeñara las funciones del p', false, 'A', 'Vicepresidente');
INSERT INTO cargo (idcargo, desccargo, editingstatus, estadocargo, nombrecargo) VALUES (3, 'Garantizar la salud financiera del Club Hogar Canario Larense, mediante una buena gestion financiera', false, 'A', 'Secretario de Finanzas');
INSERT INTO cargo (idcargo, desccargo, editingstatus, estadocargo, nombrecargo) VALUES (4, 'Planificar y ejecutar estrategias para la promocion, rescate, preservacion, estimulo, acrecentamiento y difusión en el ambito cultural Canario-Venezolano', false, 'A', 'Secretario de Cultura');
INSERT INTO cargo (idcargo, desccargo, editingstatus, estadocargo, nombrecargo) VALUES (5, 'Controlar la ejecución de las actividades de mantenimiento y reparaciones de la institucion, distribuyendo, coordinando y supervisando los trabajos del personal', false, 'A', 'Director de Mantenimiento');
INSERT INTO cargo (idcargo, desccargo, editingstatus, estadocargo, nombrecargo) VALUES (6, 'Persona encargada de conceptualizar, crear, producir y ejecutar los eventos que los miembros demanden en el Club Hogar Canario Larense, garantizando las mejores', false, 'A', 'Director de Planificación');
INSERT INTO cargo (idcargo, desccargo, editingstatus, estadocargo, nombrecargo) VALUES (8, 'Persona responsable de promover el deporte y la actividad fisic aen el Club Hogar Canario Larense, asi como el buen funcionamiento, conservacion y mantenimiento', false, 'A', 'Director de Deporte');
INSERT INTO cargo (idcargo, desccargo, editingstatus, estadocargo, nombrecargo) VALUES (7, 'Persona encargada de organizar y hacer la celebracion de las fiestas que se realizan anualmente en el Club Hogar Canario Larense', false, 'A', 'Director de Fiestas');


--
-- TOC entry 2479 (class 0 OID 21227)
-- Dependencies: 225
-- Data for Name: tipopersona; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipopersona (idtipopersona, desctipopersona, editingstatus, estadotipopersona, nombretipopersona) VALUES (1, 'Persona natural que aceptada por la junta directiva del club está inscrito en el libro de accionistas.', false, 'MP', 'Miembro Propietario');
INSERT INTO tipopersona (idtipopersona, desctipopersona, editingstatus, estadotipopersona, nombretipopersona) VALUES (2, 'Persona natural que aceptada por la junta directiva del club está inscrito en el libro de accionistas, y además no cancelan una cuota mensual', false, 'MH', 'Miembro Honorario');
INSERT INTO tipopersona (idtipopersona, desctipopersona, editingstatus, estadotipopersona, nombretipopersona) VALUES (3, 'Persona que es hijo varón de un miembro propietario, soltero y mayor de 21 años.', false, 'MA', 'Miembro Afiliado');
INSERT INTO tipopersona (idtipopersona, desctipopersona, editingstatus, estadotipopersona, nombretipopersona) VALUES (4, 'Persona que arrienda espacios del Club o solicita postulación}', false, 'CLI', 'Cliente');


--
-- TOC entry 2458 (class 0 OID 21164)
-- Dependencies: 204
-- Data for Name: persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO persona (idpersona, apellidopersona, cargopersona, cedpersona, correopersona, direcpersona, directrabpersona, editingstatus, estadocivilpersona, estadopersona, fechanacpersona, imagenpersona, lugarnacpersona, nombrepersona, profesionpersona, sexopersona, sueldopersona, tlfcasapersona, tlfcelpersona, tlftrabpersona, cargoid, tipopersonaid) VALUES (3, 'Colmenares', 'Ingeniero', '19849170', 'colmenareskarem@gmail.com', 'Cubiro', 'UCLA', false, 'Soltero', 'A', '1989-09-11', 'foto3.jpg', 'Cabudare', 'Karem', 'Ing. Informática', 'F', 35000, '02514431401', '04264863725', '02514759687', NULL, 4);
INSERT INTO persona (idpersona, apellidopersona, cargopersona, cedpersona, correopersona, direcpersona, directrabpersona, editingstatus, estadocivilpersona, estadopersona, fechanacpersona, imagenpersona, lugarnacpersona, nombrepersona, profesionpersona, sexopersona, sueldopersona, tlfcasapersona, tlfcelpersona, tlftrabpersona, cargoid, tipopersonaid) VALUES (2, 'Llarenas', 'Ingeniero', '20350202', 'fharanaz91@gmail.com', 'Av. Lara', 'Hogar Canario Larense', false, 'Soltero', 'A', '1991-11-07', 'foto2.jpg', 'Barquisimeto', 'Fharanaz', 'Ing. Informática', 'F', 50000, '02514856652', '04165458978', '02514447789', 1, 1);
INSERT INTO persona (idpersona, apellidopersona, cargopersona, cedpersona, correopersona, direcpersona, directrabpersona, editingstatus, estadocivilpersona, estadopersona, fechanacpersona, imagenpersona, lugarnacpersona, nombrepersona, profesionpersona, sexopersona, sueldopersona, tlfcasapersona, tlfcelpersona, tlftrabpersona, cargoid, tipopersonaid) VALUES (1, 'Herrera', 'Ingeniero', '19300953', 'tjhgca@gmail.com', 'Av. Madrid. Urb Parque Barquisimeto', 'UCLA', false, 'soltero', 'A', '1989-01-23', 'foto1.jpg', 'Carora', 'Teodoro', 'Ing. Informática', 'M', 30000, '02524448903', '04126775910', '02514445698', NULL, 1);
INSERT INTO persona (idpersona, apellidopersona, cargopersona, cedpersona, correopersona, direcpersona, directrabpersona, editingstatus, estadocivilpersona, estadopersona, fechanacpersona, imagenpersona, lugarnacpersona, nombrepersona, profesionpersona, sexopersona, sueldopersona, tlfcasapersona, tlfcelpersona, tlftrabpersona, cargoid, tipopersonaid) VALUES (4, 'Molina', 'Medico', '18861919', 'raulmolina440@gmail.com', 'Cabudare', 'Plaza Madrid', false, 'Casado', 'A', '1988-08-18', 'foto4.jpg', 'Barquisimeto', 'Raúl', 'Ginecobstetra', 'M', 60000, '02514789652', '04124578965', '025145789654', NULL, 2);
INSERT INTO persona (idpersona, apellidopersona, cargopersona, cedpersona, correopersona, direcpersona, directrabpersona, editingstatus, estadocivilpersona, estadopersona, fechanacpersona, imagenpersona, lugarnacpersona, nombrepersona, profesionpersona, sexopersona, sueldopersona, tlfcasapersona, tlfcelpersona, tlftrabpersona, cargoid, tipopersonaid) VALUES (5, 'Manzanilla', 'Admistrador', '19669350', 'auramanzanilla@gmail.com', 'Carrera 15 entre 55 y 54', 'Hogar Canario Larense', false, 'Casada', 'A', '1991-06-09', 'foto5.jpg', 'Barquisimeto', 'Aura', 'Administrador', 'F', 40000, '02514547844', '04164547845', '025142487546', 3, 1);


--
-- TOC entry 2426 (class 0 OID 21063)
-- Dependencies: 171
-- Data for Name: accion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO accion (idaccion, editingstatus, estadoaccion, fechaaccion, personaiid) VALUES (1, false, 'A', '2014-02-05', 1);
INSERT INTO accion (idaccion, editingstatus, estadoaccion, fechaaccion, personaiid) VALUES (2, false, 'A', '2001-08-03', 2);
INSERT INTO accion (idaccion, editingstatus, estadoaccion, fechaaccion, personaiid) VALUES (3, false, 'A', '1995-08-03', 4);
INSERT INTO accion (idaccion, editingstatus, estadoaccion, fechaaccion, personaiid) VALUES (4, false, 'A', '2000-08-02', 5);


--
-- TOC entry 2427 (class 0 OID 21066)
-- Dependencies: 172
-- Data for Name: acompanante; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO acompanante (idacompanante, apellidoacompanante, cedacompanante, editingstatus, estadoacompanante, nombreacompanante) VALUES (1, 'Suarez', '16278545', false, 'A', 'Meibelin');
INSERT INTO acompanante (idacompanante, apellidoacompanante, cedacompanante, editingstatus, estadoacompanante, nombreacompanante) VALUES (3, 'Vera', '18525744', false, 'A', 'Mariel');
INSERT INTO acompanante (idacompanante, apellidoacompanante, cedacompanante, editingstatus, estadoacompanante, nombreacompanante) VALUES (2, 'Salazar', '21064751', false, 'A', 'Fernando');
INSERT INTO acompanante (idacompanante, apellidoacompanante, cedacompanante, editingstatus, estadoacompanante, nombreacompanante) VALUES (4, 'Yepez', '19114746', false, 'A', 'Maria');


--
-- TOC entry 2429 (class 0 OID 21072)
-- Dependencies: 174
-- Data for Name: actividad; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO actividad (idactividad, descactividad, editingstatus, estadoactividad, nombreactividad) VALUES (2, 'Encargada de mantener las areas limpias y en buen estado las areas destinadas al evento', false, 'A', 'Limpieza');
INSERT INTO actividad (idactividad, descactividad, editingstatus, estadoactividad, nombreactividad) VALUES (1, 'Encargada de coordinar las actividades necesarias para la realizacion del evento', false, 'A', 'Logistica');


--
-- TOC entry 2441 (class 0 OID 21111)
-- Dependencies: 186
-- Data for Name: comision; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO comision (idcomision, desccomision, editingstatus, estadocomision, nombrecomision) VALUES (1, 'Comision encargada del evento San Antonio', false, 'A', 'San Antonio');


--
-- TOC entry 2428 (class 0 OID 21069)
-- Dependencies: 173
-- Data for Name: act_comision; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO act_comision (idact_comision, actividadid, comisionid) VALUES (1, 1, 1);


--
-- TOC entry 2471 (class 0 OID 21203)
-- Dependencies: 217
-- Data for Name: tipoarea; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipoarea (idtipoarea, desctipoarea, editingstatus, estadotipoarea, nombretipoarea) VALUES (1, 'Area destinada a actividades recreativas', false, 'A', 'Recreativa');
INSERT INTO tipoarea (idtipoarea, desctipoarea, editingstatus, estadotipoarea, nombretipoarea) VALUES (2, 'Area destinada a actividades culturales', false, 'A', 'Cultural');
INSERT INTO tipoarea (idtipoarea, desctipoarea, editingstatus, estadotipoarea, nombretipoarea) VALUES (3, 'Area destinada a actividades sociales', false, 'A', 'Social');
INSERT INTO tipoarea (idtipoarea, desctipoarea, editingstatus, estadotipoarea, nombretipoarea) VALUES (4, 'Area destinada a actividades deportivas', false, 'A', 'Deportiva');


--
-- TOC entry 2430 (class 0 OID 21075)
-- Dependencies: 175
-- Data for Name: area; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO area (idarea, capacidadarea, descarea, dimensionarea, editingstatus, estadoarea, nombrearea, tipoareaid) VALUES (1, 250, 'Salon para eventos', '100m2', false, 'D', 'Salón Principal', 3);
INSERT INTO area (idarea, capacidadarea, descarea, dimensionarea, editingstatus, estadoarea, nombrearea, tipoareaid) VALUES (2, 100, 'Salon para fiestas', '50m2', false, 'D', 'Salón de los espejos', 1);
INSERT INTO area (idarea, capacidadarea, descarea, dimensionarea, editingstatus, estadoarea, nombrearea, tipoareaid) VALUES (3, 100, 'Salón para fiestas para niños', '50m2', false, 'D', 'Tamarindo', 2);
INSERT INTO area (idarea, capacidadarea, descarea, dimensionarea, editingstatus, estadoarea, nombrearea, tipoareaid) VALUES (4, 75, 'Caneyes ', '50m2', false, 'D', 'Bosquecito', 2);


--
-- TOC entry 2472 (class 0 OID 21206)
-- Dependencies: 218
-- Data for Name: tipoarrendamiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipoarrendamiento (idtipoarrend, descarrend, editingstatus, estadotipoarrend, nombrearrend) VALUES (1, 'Boda civil', false, 'A', 'Boda');
INSERT INTO tipoarrendamiento (idtipoarrend, descarrend, editingstatus, estadotipoarrend, nombrearrend) VALUES (2, 'Quince años', false, 'A', 'Quince años');
INSERT INTO tipoarrendamiento (idtipoarrend, descarrend, editingstatus, estadotipoarrend, nombrearrend) VALUES (4, 'Despedida de soltera', false, 'A', 'Despedida de Soltera');
INSERT INTO tipoarrendamiento (idtipoarrend, descarrend, editingstatus, estadotipoarrend, nombrearrend) VALUES (3, 'Graduación', false, 'A', 'Graduación');


--
-- TOC entry 2434 (class 0 OID 21087)
-- Dependencies: 179
-- Data for Name: arrendamiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO arrendamiento (idarrendamiento, descarrend, editingstatus, estadoarrend, fechaarrend, fechaemisionarrend, horaarrend, personaid, tipoarrendamientoid) VALUES (1, 'Boda Civil de Aura', false, 'A', '2016-03-04', '2016-02-25', '06:00:00', 5, 1);
INSERT INTO arrendamiento (idarrendamiento, descarrend, editingstatus, estadoarrend, fechaarrend, fechaemisionarrend, horaarrend, personaid, tipoarrendamientoid) VALUES (3, 'Quince años de Karem', false, 'A', '2016-04-04', '2016-03-01', '08:00:00', 3, 2);
INSERT INTO arrendamiento (idarrendamiento, descarrend, editingstatus, estadoarrend, fechaarrend, fechaemisionarrend, horaarrend, personaid, tipoarrendamientoid) VALUES (2, 'Graduación de Teodoro', false, 'A', '2016-06-19', '2016-02-28', '07:00:00', 1, 3);


--
-- TOC entry 2431 (class 0 OID 21078)
-- Dependencies: 176
-- Data for Name: area_arrend; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO area_arrend (idarea_arrend, areaid, arrendamientoid) VALUES (1, 1, 1);


--
-- TOC entry 2473 (class 0 OID 21209)
-- Dependencies: 219
-- Data for Name: tipoevento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipoevento (idtipoevento, desctipoevento, editingstatus, estadotipoevento, nombretipoevento) VALUES (1, 'Evento que se realiza constantemente en el club', false, 'A', 'Permanente');
INSERT INTO tipoevento (idtipoevento, desctipoevento, editingstatus, estadotipoevento, nombretipoevento) VALUES (2, 'Evento que se realiza eventualmente', false, 'A', 'Temporal');


--
-- TOC entry 2480 (class 0 OID 21230)
-- Dependencies: 226
-- Data for Name: tipopublico; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipopublico (idtipopubico, desctipopublico, editingstatus, estadotipopublico, nombretipopublico) VALUES (1, 'Beneficiarios menores a 13 años de edad', false, 'A', 'Niños');


--
-- TOC entry 2444 (class 0 OID 21120)
-- Dependencies: 189
-- Data for Name: evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO evento (idevento, descevento, editingstatus, estadoevento, fechaevento, nombreevento, tipoeventoid, tipopublicoid) VALUES (1, 'Evento destinado a la celebracion mariana de la Virgen de la Candelaria', false, 'A', '1989-12-12', 'Candelaria', 1, 1);


--
-- TOC entry 2432 (class 0 OID 21081)
-- Dependencies: 177
-- Data for Name: area_evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO area_evento (idarea_evento, areaid, eventoid) VALUES (1, 1, 1);


--
-- TOC entry 2433 (class 0 OID 21084)
-- Dependencies: 178
-- Data for Name: area_recurso; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO area_recurso (idarea_recurso, areaid, recursoid) VALUES (1, 1, 1);


--
-- TOC entry 2435 (class 0 OID 21090)
-- Dependencies: 180
-- Data for Name: asistencia_evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO asistencia_evento (idasistencia_evento, eventoid, personaid) VALUES (1, 1, 1);


--
-- TOC entry 2457 (class 0 OID 21161)
-- Dependencies: 203
-- Data for Name: parentesco; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO parentesco (idparentesco, descparentesco, editingstatus, estadoparentesco, nombreparentesco) VALUES (1, NULL, false, 'A', 'Cónyugue');
INSERT INTO parentesco (idparentesco, descparentesco, editingstatus, estadoparentesco, nombreparentesco) VALUES (2, NULL, false, 'A', 'Hijo');
INSERT INTO parentesco (idparentesco, descparentesco, editingstatus, estadoparentesco, nombreparentesco) VALUES (3, NULL, false, 'A', 'Padre');
INSERT INTO parentesco (idparentesco, descparentesco, editingstatus, estadoparentesco, nombreparentesco) VALUES (4, NULL, false, 'A', 'Madre');
INSERT INTO parentesco (idparentesco, descparentesco, editingstatus, estadoparentesco, nombreparentesco) VALUES (5, NULL, false, 'A', 'Suegro');
INSERT INTO parentesco (idparentesco, descparentesco, editingstatus, estadoparentesco, nombreparentesco) VALUES (6, NULL, false, 'A', 'Suegra');


--
-- TOC entry 2436 (class 0 OID 21093)
-- Dependencies: 181
-- Data for Name: beneficiario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO beneficiario (idbeneficiario, apellidobene, cedbene, correobene, editingstatus, estadobene, fechanacbene, nombrebene, parentescoid, personaid) VALUES (2, 'Velasquez', '19669350', 'auramanzanilla@gmail.com', false, 'A', '2016-02-23', 'Miguelauro', 2, 5);
INSERT INTO beneficiario (idbeneficiario, apellidobene, cedbene, correobene, editingstatus, estadobene, fechanacbene, nombrebene, parentescoid, personaid) VALUES (1, 'Herrera', '19300953', 'teodoro_h19@hotmail.com', false, 'A', '2010-08-04', 'Teo', 2, 1);


--
-- TOC entry 2437 (class 0 OID 21096)
-- Dependencies: 182
-- Data for Name: calendario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO calendario (idcalendario, estadocalend, fechafincalend, fechainiciocalend) VALUES ('1', 'A', '2016-12-10', '2016-12-15');


--
-- TOC entry 2439 (class 0 OID 21102)
-- Dependencies: 184
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO categoria (idcategoria, desccategoria, editingstatus, estadocategoria, nombrecategoria) VALUES (1, 'Actividad destinada a la integracion social de los miembros del CLub Hogar Canario Larense', false, 'A', 'Social');
INSERT INTO categoria (idcategoria, desccategoria, editingstatus, estadocategoria, nombrecategoria) VALUES (2, 'Actividad destinada a la ejecucion de actividades que fomenten la cultura Canario-Venezolana', false, 'A', 'Cultural');
INSERT INTO categoria (idcategoria, desccategoria, editingstatus, estadocategoria, nombrecategoria) VALUES (3, 'Actividad destinada a la ejecucion de eventos que fomenten las actividades deportivas', false, 'A', 'Deportivo');


--
-- TOC entry 2440 (class 0 OID 21105)
-- Dependencies: 185
-- Data for Name: club; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO club (idclub, correoclub, direccionclub, editingstatus, estadoclub, logoclub, misionclub, nombreclub, rifclub, tlfclub, visionclub) VALUES (1, 'clubcanario@gmail.com', 'Av. La Salle', false, 'A', 'logo.jpg', 'El club hogar canario larense', 'Hogar Canario Larense', 'Rif45585255', '02514456856', 'El club Hogar canario Larense');


--
-- TOC entry 2442 (class 0 OID 21114)
-- Dependencies: 187
-- Data for Name: documento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO documento (iddocumento, archivodocumento, editingstatus, estadodocumento, nombredocumento, clubid) VALUES (1, 'archivo.pdf', false, 'A', 'Constancia', 1);
INSERT INTO documento (iddocumento, archivodocumento, editingstatus, estadodocumento, nombredocumento, clubid) VALUES (2, 'archivo2.pdf', false, 'A', 'Planilla de solicitud de membr', 1);
INSERT INTO documento (iddocumento, archivodocumento, editingstatus, estadodocumento, nombredocumento, clubid) VALUES (3, 'archivo3.pdf', false, 'A', 'Formato de apelacion de sancio', 1);


--
-- TOC entry 2443 (class 0 OID 21117)
-- Dependencies: 188
-- Data for Name: encuesta; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO encuesta (idencuesta, descencuesta, editingstatus, estadoencuesta, fechaencuesta, nombreencuesta) VALUES (1, 'Conjunto de preguntas orientadas a conocer la opinion de los miembros acerca del evento San Antonio', false, 'A', '2016-12-12', 'Encuesta de satisfacción Event');


--
-- TOC entry 2445 (class 0 OID 21123)
-- Dependencies: 190
-- Data for Name: evento_comision; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO evento_comision (idevento_comision, responsable, comisionid, eventoid) VALUES (1, 'Fharanaz', 1, 1);


--
-- TOC entry 2475 (class 0 OID 21215)
-- Dependencies: 221
-- Data for Name: tipoindicador; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipoindicador (idtipoindicador, desctipoind, editingstatus, estadotipoind, nombretipoind) VALUES (1, 'Indicador que se hace uso en los miembros', false, 'A', 'Miembro');
INSERT INTO tipoindicador (idtipoindicador, desctipoind, editingstatus, estadotipoind, nombretipoind) VALUES (2, 'Indicador que se hace uso en los arrendamiento', false, 'A', 'Arrendamiento');
INSERT INTO tipoindicador (idtipoindicador, desctipoind, editingstatus, estadotipoind, nombretipoind) VALUES (3, 'Indicador que se hace uso en los Eventos', false, 'A', 'Evento');


--
-- TOC entry 2483 (class 0 OID 21239)
-- Dependencies: 229
-- Data for Name: unidadmedida; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO unidadmedida (idunidadmedida, descunidadmedida, editingstatus, estadounidadmedida, nombreunidadmedida) VALUES (1, 'Unidad expresada en porcentajes', false, 'A', 'Porcentaje');


--
-- TOC entry 2452 (class 0 OID 21146)
-- Dependencies: 198
-- Data for Name: indicador; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO indicador (idindicador, descindicador, editingstatus, estadoindicador, nombreindicador, tipoindicadorid, unidadmedidaid) VALUES (1, 'Asistencia de cada uno de los miembros del club a cada uno de los eventos', false, 'A', 'Asistencia', 1, 1);


--
-- TOC entry 2446 (class 0 OID 21126)
-- Dependencies: 191
-- Data for Name: evento_indicador; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO evento_indicador (idevento_ind, valorespevento_ind, eventoid, indicadorid) VALUES (1, 1, 1, 1);


--
-- TOC entry 2447 (class 0 OID 21129)
-- Dependencies: 192
-- Data for Name: funcion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (1, 'A', 'No Funcion', 'z-icon-cogs', 0, 'Configuración del Sistema', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (2, 'A', 'No Funcion', 'icon-angle-double-right', 1, 'Miembros', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (3, 'A', 'Funcion', 'z-icon-angle-double-right', 2, 'Tipos de Persona', 'vista/tipoPersona.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (4, 'A', 'Funcion', 'z-icon-angle-double-right', 2, 'Cargos', 'vista/cargo.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (5, 'A', 'Funcion', 'z-icon-angle-double-right', 2, 'Tipos de Sanción', 'vista/tipoSancion.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (6, 'A', 'Funcion', 'z-icon-angle-double-right', 2, 'Tipos d Incidencia', 'vista/tipoIncidencia.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (7, 'A', 'Funcion', 'z-icon-angle-double-right', 2, 'Parentesco', 'vista/parentesco.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (8, 'A', 'No Funcion', 'z-icon-angle-double-right', 1, 'Eventos', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (9, 'A', 'Funcion', 'z-icon-angle-double-right', 8, 'Tipos de Evento', 'vista/tipoEvento.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (10, 'A', 'Funcion', 'z-icon-angle-double-right', 8, 'Tipos de Público', 'vista/tipoPublico.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (11, 'A', 'Funcion', 'z-icon-angle-double-right', 8, 'Actividades', 'vista/actividad.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (12, 'A', 'No Funcion', 'z-icon-angle-double-right', 1, 'Arrendamiento', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (13, 'A', 'Funcion', 'z-icon-angle-double-right', 12, 'Tipos de Area', 'vista/tipoArea.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (14, 'A', 'No Funcion', 'z-icon-angle-double-right', 1, 'Club', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (16, 'A', 'Funcion', 'z-icon-angle-double-right', 14, 'Tipos de Noticias', 'vista/tipoNoticia.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (17, 'A', 'Funcion', 'z-icon-angle-double-right', 14, 'Recursos', 'vista/recurso.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (18, 'A', 'Funcion', 'z-icon-angle-double-right', 14, 'Unidad de Medida', 'vista/unidadMedida.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (20, 'A', 'Funcion', 'z-icon-angle-double-right', 14, 'Categorías', 'vista/categoria.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (21, 'A', 'No Funcion', 'z-icon-angle-double-right', 1, 'Configuración', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (22, 'A', 'Funcion', 'z-icon-angle-double-right', 21, 'Áreas', 'vista/area.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (23, 'A', 'Funcion', 'z-icon-angle-double-right', 21, 'Comisiones', 'vista/comisiones.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (24, 'A', 'Funcion', 'z-icon-angle-double-right', 21, 'Indicadores', 'vista/indicador.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (25, 'A', 'Funcion', 'z-icon-angle-double-right', 21, 'Preferencias', 'vista/preferencia.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (26, 'A', 'No Funcion', 'z-icon-group', 0, 'Gestión de Miembros', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (28, 'A', 'Funcion', 'z-icon-angle-double-right', 26, 'Asignar Acción', 'vista/asignarAccion.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (29, 'A', 'Funcion', 'z-icon-angle-double-right', 26, 'Registra Beneficiarios', 'vista/registrarBeneficiario.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (30, 'A', 'Funcion', 'z-icon-angle-double-right', 26, 'Registrar Visitas', 'vista/registarVisita.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (31, 'A', 'Funcion', 'z-icon-angle-double-right', 26, 'Registrar Incidencia', 'vista/registrarIncidenciaMiembro.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (32, 'A', 'Funcion', 'z-icon-angle-double-right', 26, 'Sancionar', 'vista/sancionar.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (33, 'A', 'Funcion', 'z-icon-angle-double-right', 26, 'Reactivar', 'vista/reactivarMiembro.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (34, 'A', 'Funcion', 'z-icon-angle-double-right', 26, 'Cese de Acción', 'vista/ceseAccion.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (35, 'A', 'No Funcion', 'z-icon-calendar', 0, 'Gestión de Evento', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (37, 'A', 'Funcion', 'z-icon-angle-double-right', 35, 'Planificar Evento', 'vista/configuracionEventos.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (38, 'A', 'Funcion', 'z-icon-angle-double-right', 35, 'Ejecución de Eventos', 'vista/ejecucionEventos.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (39, 'A', 'Funcion', 'z-icon-angle-double-right', 35, 'Asistencia', 'vista/registrarAsistenciaEvento.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (41, 'A', 'No Funcion', 'z-icon-bell', 0, 'Gestión de Arrendamientos', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (42, 'A', 'Funcion', 'z-icon-angle-double-right', 41, 'Registrar Solicitud', 'vista/solicitudMiembroArrendamiento.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (43, 'A', 'Funcion', 'z-icon-angle-double-right', 41, 'Atender Solicitud', 'vista/atenderSolicitudesArrendamiento.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (46, 'A', 'No Funcion', 'z-icon-wrench', 0, 'Administración del Sistema', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (47, 'A', 'No Funcion', 'z-icon-angle-double-right', 46, 'Seguridad Funcional', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (48, 'A', 'Funcion', 'z-icon-angle-double-right', 47, 'Crear Grupo', 'vista/crearGrupo.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (49, 'A', 'Funcion', 'z-icon-angle-double-right', 47, 'Crear Usuario', 'vista/crearUsuario.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (50, 'A', 'No Funcion', 'z-icon-angle-double-right', 46, 'Configuración', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (51, 'A', 'Funcion', 'z-icon-angle-double-right', 50, 'Noticias', 'vista/registrarNoticias.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (52, 'A', 'Funcion', 'z-icon-angle-double-right', 50, 'Documentos', 'vista/documento.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (56, 'A', 'No Funcion', 'z-icon-file-text', 0, 'Reportes', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (57, 'A', 'Funcion', 'z-icon-angle-double-right', 12, 'Tipos de Arrendamiento', 'vista/tipoArrendamiento.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (15, 'A', 'Funcion', 'z-icon-angle-double-right', 14, 'Tipos de Rechazo', 'vista/tipoRechazo.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (19, 'A', 'Funcion', 'z-icon-angle-double-right', 14, 'Tipos de Indicador', 'vista/tipoIndicador.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (44, 'A', 'Funcion', 'z-icon-angle-double-right', 41, 'Cancelar Arrendamiento', 'vista/arrendamientos.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (45, 'A', 'Funcion', 'z-icon-angle-double-right', 41, 'Registrar Incidencia', 'vista/registrarIncidenciaArrendamiento.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (53, 'A', 'Funcion', 'z-icon-angle-double-right', 50, 'Galería', 'vista/configuracionGaleria.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (61, 'A', 'Funcion', 'z-icon-angle-double-right', 58, 'Arrendamientos', 'vista/reporteEstructuradoArrendamiento.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (59, 'A', 'Funcion', 'z-icon-angle-double-right', 58, 'Miembros', 'vista/reporteEstructuradoMiembro.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (58, 'A', 'No Funcion', 'z-icon-angle-double-right', 56, 'Estructurado', '""');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (27, 'A', 'Funcion', 'z-icon-angle-double-right', 26, 'Atender Solicitud', 'vista/atenderPostulacionMembresia.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (55, 'A', 'Funcion', 'z-icon-angle-double-right', 50, 'Sugerencias', 'vista/sugerencia.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (60, 'A', 'Funcion', 'z-icon-angle-double-right', 58, 'Eventos', 'vista/reporteEstructuradoEvento.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (54, 'A', 'Funcion', 'z-icon-angle-double-right', 50, 'Club', 'vista/perfilClub.zul');
INSERT INTO funcion (idfuncion, estadofuncion, extrafuncion, iconurifuncion, idpadrefuncion, nombrefuncion, paginafuncion) VALUES (36, 'A', 'Funcion', 'z-icon-angle-double-right', 35, 'Registrar Evento', 'vista/registrarEvento.zul');


--
-- TOC entry 2450 (class 0 OID 21138)
-- Dependencies: 195
-- Data for Name: grupo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO grupo (idgrupo, descgrupo, editingstatus, estadogrupo, nombregrupo) VALUES (1, 'El grupo de administración', false, 'A', 'Administración');


--
-- TOC entry 2448 (class 0 OID 21132)
-- Dependencies: 193
-- Data for Name: funcion_grupo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO funcion_grupo (idfuncion_grupo, funcionid, grupoid) VALUES (1, 1, 1);


--
-- TOC entry 2449 (class 0 OID 21135)
-- Dependencies: 194
-- Data for Name: galeria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO galeria (idgaleria, editingstatus, estadogaleria, imagengaleria, nombregaleria, clubid) VALUES (1, false, 'A', 'imagen.jpg', 'Piscina', 1);
INSERT INTO galeria (idgaleria, editingstatus, estadogaleria, imagengaleria, nombregaleria, clubid) VALUES (2, false, 'A', 'imgaen2.jpg', 'Cancha de Bolas Criollas', 1);


--
-- TOC entry 2474 (class 0 OID 21212)
-- Dependencies: 220
-- Data for Name: tipoincidencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipoincidencia (idtipoincid, desctipoincid, editingstatus, estadotipoincid, nombretipoincid) VALUES (1, 'Actos Violentos dentro de las instalaciones del club', false, 'A', 'Violencia');
INSERT INTO tipoincidencia (idtipoincid, desctipoincid, editingstatus, estadotipoincid, nombretipoincid) VALUES (2, 'Daños materiales en las instalaciones del club', false, 'A', 'Daños Materiales');


--
-- TOC entry 2451 (class 0 OID 21143)
-- Dependencies: 197
-- Data for Name: incidencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO incidencia (idincidencia, descincidencia, editingstatus, estadoincidencia, fechaincidencia, horaincidencia, arrendamientoid, personaid, tipoincidenciaid) VALUES (1, 'Se reporto una mesa de madera dañada durante el arrendamiento del miembro', false, 'A', '2016-12-01', '06:00:00', 1, 1, 1);


--
-- TOC entry 2476 (class 0 OID 21218)
-- Dependencies: 222
-- Data for Name: tiponoticia; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tiponoticia (idtiponoticia, desctiponoticia, editingstatus, estadotiponoticia, nombretiponoticia) VALUES (1, 'El dia sábado se realizara un torneo de bolas criollas para todos los miembros, inscripciones abiertas.', false, 'A', 'Deportiva');


--
-- TOC entry 2453 (class 0 OID 21149)
-- Dependencies: 199
-- Data for Name: noticia; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO noticia (idnoticia, descnoticia, difusionnoticia, editingstatus, estadonoticia, fechaexpnoticia, fechaininoticia, imagennoticia, titulonoticia, clubid, tiponoticiaid) VALUES (1, 'Se dictaran clases de natacion para todas las edades durante el mes de agosto para todas las edades', 'PUBLICA', false, 'A', '2016-12-12', '2016-12-12', 'IMAGEN.JPG', 'Clases de verano de natacion', 1, 1);


--
-- TOC entry 2477 (class 0 OID 21221)
-- Dependencies: 223
-- Data for Name: tiponotificacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tiponotificacion (idtiponotif, desctiponotif, editingstatus, estadotiponotif, nombretiponotif) VALUES (1, 'Solicitud de pago por daños a las instalaciones', false, 'A', 'Pago');


--
-- TOC entry 2454 (class 0 OID 21152)
-- Dependencies: 200
-- Data for Name: notificacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO notificacion (idnotificacion, asuntonotificacion, editingstatus, estadonotificacion, personaid, tiponotificacionid) VALUES (1, 'Expulsion temporal', false, 'a', 1, 1);


--
-- TOC entry 2455 (class 0 OID 21155)
-- Dependencies: 201
-- Data for Name: opcionpregunta; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO opcionpregunta (idopcionpregunta, editingstatus, estadoopcionpreg, nombreopcion) VALUES (1, false, 'a', 'opcion');


--
-- TOC entry 2478 (class 0 OID 21224)
-- Dependencies: 224
-- Data for Name: tipoopinion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipoopinion (idtipoopinion, desctipoopinion, editingstatus, estadotipoopinion, nombretipoopinion) VALUES (1, NULL, false, 'A', 'Area');


--
-- TOC entry 2484 (class 0 OID 21242)
-- Dependencies: 230
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO usuario (idusuario, claveusuario, editingstatus, estadousuario, fechausuario, nombreusuario, grupoid, personaid) VALUES (1, '123', false, 'A', '2016-02-12', 'fhara', 1, 2);


--
-- TOC entry 2456 (class 0 OID 21158)
-- Dependencies: 202
-- Data for Name: opinion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO opinion (idopinion, asuntoopinion, descopinion, editingstatus, estadoopinion, tipoopinionid, usuarioid) VALUES (1, 'Descontento por fall', 'No estoy de acuerdo con las empresas de recreacion que contrataron para el evento del dia del niño', false, 'a', 1, 1);


--
-- TOC entry 2459 (class 0 OID 21167)
-- Dependencies: 205
-- Data for Name: postulacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO postulacion (idpostulacion, editingstatus, estadopostulacion, fechapostulacion, personaid) VALUES (1, false, 'A', '1999-08-03', 1);
INSERT INTO postulacion (idpostulacion, editingstatus, estadopostulacion, fechapostulacion, personaid) VALUES (2, false, 'A', '1999-08-03', 2);
INSERT INTO postulacion (idpostulacion, editingstatus, estadopostulacion, fechapostulacion, personaid) VALUES (3, false, 'A', '1999-08-03', 4);
INSERT INTO postulacion (idpostulacion, editingstatus, estadopostulacion, fechapostulacion, personaid) VALUES (4, false, 'A', '1999-08-03', 5);


--
-- TOC entry 2467 (class 0 OID 21191)
-- Dependencies: 213
-- Data for Name: referencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO referencia (idreferencia, apellidoreferencia, cedulareferencia, editingstatus, estadoreferencia, nombrereferencia, tlfreferencia) VALUES (1, 'Herrera', '19300953', false, 'A', 'Teodoro', '04126775910');


--
-- TOC entry 2460 (class 0 OID 21170)
-- Dependencies: 206
-- Data for Name: postulacion_ref; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO postulacion_ref (idpost_ref, postulacionid, referenciaid) VALUES (1, 1, 1);


--
-- TOC entry 2461 (class 0 OID 21173)
-- Dependencies: 207
-- Data for Name: preferencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO preferencia (idpreferencia, descpreferencia, estadopreferencia, nombrepreferencia, categoriaid) VALUES (1, 'Toda actividad que este relacionada al futbol', 'A', 'Fútbol', 3);


--
-- TOC entry 2462 (class 0 OID 21176)
-- Dependencies: 208
-- Data for Name: preferencia_evento; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2463 (class 0 OID 21179)
-- Dependencies: 209
-- Data for Name: preferencia_persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO preferencia_persona (idpreferencia_per, personaid, preferenciaid) VALUES (1, 1, 1);


--
-- TOC entry 2464 (class 0 OID 21182)
-- Dependencies: 210
-- Data for Name: pregunta; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO pregunta (idpregunta, editingstatus, estadopregunta, nombrepregunta, categoriaid, encuestaid, opcionpreguntaid) VALUES (1, false, 'A', '¿Cómo le pareció el conjunto c', 1, 1, 1);


--
-- TOC entry 2481 (class 0 OID 21233)
-- Dependencies: 227
-- Data for Name: tiporechazo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tiporechazo (idtiporechazo, desctiporechazo, editingstatus, estadotiporechazo, nombretiporechazo) VALUES (1, 'Se rechaza la solicitud por no completar la documentacion solicitada', false, 'A', 'Documentación incompleta');


--
-- TOC entry 2465 (class 0 OID 21185)
-- Dependencies: 211
-- Data for Name: rechazo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rechazo (idrechazo, descrechazo, editingstatus, estadorechazo, arrendamientoid, postulacionid, tiporechazoid) VALUES (1, 'Su solicitud a sido rechazada por no completar los requisitos solicitados', false, 'a', 1, 1, 1);


--
-- TOC entry 2466 (class 0 OID 21188)
-- Dependencies: 212
-- Data for Name: recurso; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO recurso (idrecurso, cantidadrecurso, descrecurso, editingstatus, estadorecurso, nombrerecurso) VALUES (1, 525, 'Sillas plasticas', false, 'D', 'Sillas');
INSERT INTO recurso (idrecurso, cantidadrecurso, descrecurso, editingstatus, estadorecurso, nombrerecurso) VALUES (2, 35, 'Mesas de madera', false, 'D', 'Mesas');
INSERT INTO recurso (idrecurso, cantidadrecurso, descrecurso, editingstatus, estadorecurso, nombrerecurso) VALUES (3, 3, 'Mesas Rectangulares de madera', false, 'D', 'Mesas rectangulares');


--
-- TOC entry 2468 (class 0 OID 21194)
-- Dependencies: 214
-- Data for Name: respuestaencuesta; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO respuestaencuesta (idrespuestaenc, editingstatus, estadorespuestaenc, fecharespuestaenc, opcionpreguntaid, preguntaid, usuarioid) VALUES (1, false, 'A', '2016-12-12', 1, 1, 1);


--
-- TOC entry 2469 (class 0 OID 21197)
-- Dependencies: 215
-- Data for Name: resultado; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO resultado (idresultado, calculoresultado, descresultado, editingstatus, estadoresultado, valorrealresultado, evento_indicadorid) VALUES (1, 60, 'Se cumplio con lo esperado en un % muy alto', false, 'A', 54, 1);


--
-- TOC entry 2482 (class 0 OID 21236)
-- Dependencies: 228
-- Data for Name: tiposancion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tiposancion (idtiposancion, desctiposancion, editingstatus, estadotiposancion, nombretiposancion) VALUES (1, 'Expulsion parcial o total de un miembro o beneficiario', false, 'A', 'Expulsión');


--
-- TOC entry 2470 (class 0 OID 21200)
-- Dependencies: 216
-- Data for Name: sancion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sancion (idsancion, descsancion, editingstatus, estadosancion, fechasancion, personaid, tiposancionid) VALUES (1, 'Debe cancelar la cantidad correspondiente a la reparacion del inmueble dañado durante el arrendamiento', false, 'a', '2016-12-12', 1, 1);


--
-- TOC entry 2485 (class 0 OID 21245)
-- Dependencies: 231
-- Data for Name: visita; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO visita (idvisita, editingstatus, fechavisita, horavisita, personaid) VALUES (1, false, '0216-12-12', '06:00:00', 1);


--
-- TOC entry 2486 (class 0 OID 21248)
-- Dependencies: 232
-- Data for Name: visita_acomp; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO visita_acomp (idvisita_acomp, acompananteid, visitaid) VALUES (1, 1, 1);


-- Completed on 2016-03-07 04:17:24 VET

--
-- PostgreSQL database dump complete
--

